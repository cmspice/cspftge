﻿// C# example.
using System;
using UnityEditor;
using UnityEngine;
using System.Diagnostics;
using System.IO;

public class FGEUnityBuilderMain 
{
	[MenuItem("Build/OSX")]
	public static void BuildGameOSX ()
	{
		// Get filename.
		//string path = EditorUtility.SaveFolderPanel("Choose Location of Built Game", "", "");
		string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/unitybuilds";
		string[] levels = new string[] { "Assets/TESTEG.unity" };
		string buildName = "FGEtest";

		// Build player.
		//TODO add date
		BuildPipeline.BuildPlayer(levels, path+"/"+buildName, BuildTarget.StandaloneOSXIntel, BuildOptions.None);

		// Copy a file from the project folder to the build folder, alongside the built game.
		//FileUtil.CopyFileOrDirectory(Application.dataPath + "/EditorOutput", path + "/" + buildName + "/Contents");
		DirectoryCopy(Application.dataPath + "/EditorOutput", path + "/" + buildName + ".app/Contents/EditorOutput",true);
		DirectoryCopy(Application.dataPath + "/StreamingAssets", path + "/" + buildName + ".app/Contents/StreamingAssets",true);



		// Run the game (Process class from System.Diagnostics).
		Process proc = new Process();
		proc.StartInfo.FileName = path + "/" + buildName + ".app/Contents/MacOS/" + buildName;
		proc.Start();
	}
	[UnityEditor.Callbacks.PostProcessBuildAttribute(1)]
	public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {
		UnityEngine.Debug.Log( "finished building to " + pathToBuiltProject );
	}

	[MenuItem("Build/Run OSX")]
	public static void RunGame ()
	{
		string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/unitybuilds";
		string buildName = "FGEtest";
		Process proc = new Process();
		proc.StartInfo.FileName = path + "/" + buildName + ".app/Contents/MacOS/" + buildName;
		proc.Start();
	}

	private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
	{
		// Get the subdirectories for the specified directory.
		DirectoryInfo dir = new DirectoryInfo(sourceDirName);

		if (!dir.Exists)
		{
			throw new DirectoryNotFoundException(
				"Source directory does not exist or could not be found: "
				+ sourceDirName);
		}

		DirectoryInfo[] dirs = dir.GetDirectories();
		// If the destination directory doesn't exist, create it.
		if (!Directory.Exists(destDirName))
		{
			Directory.CreateDirectory(destDirName);
		}

		// Get the files in the directory and copy them to the new location.
		FileInfo[] files = dir.GetFiles();
		foreach (FileInfo file in files)
		{
			string temppath = Path.Combine(destDirName, file.Name);
			file.CopyTo(temppath, false);
		}

		// If copying subdirectories, copy them and their contents to new location.
		if (copySubDirs)
		{
			foreach (DirectoryInfo subdir in dirs)
			{
				string temppath = Path.Combine(destDirName, subdir.Name);
				DirectoryCopy(subdir.FullName, temppath, copySubDirs);
			}
		}
	}
}