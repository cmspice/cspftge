﻿using UnityEditor;
using UnityEngine;
using UnityEditor.AnimatedValues;

public class HitboxEditor : EditorWindow
{
	string myString = "Hello World";
	bool groupEnabled;
	bool myBool = true;
	float myFloat = 1.23f;



	AnimBool m_ShowExtraFields;
	string m_String;
	Color m_Color = Color.white;
	int m_Number = 0;



	// Add menu item named "My Window" to the Window menu
	[MenuItem("Window/My Window")]
	public static void ShowWindow()
	{
		//Show existing window instance. If one doesn't exist, make one.
		EditorWindow.GetWindow(typeof(HitboxEditor));
	}

	void OnEnable(){
		m_ShowExtraFields = new AnimBool(true);
		m_ShowExtraFields.valueChanged.AddListener(Repaint);
	}

	void OnGUI()
	{
		//Rect r1 = (Rect)EditorGUILayout.BeginVertical("button",GUILayout.ExpandHeight(true));
		m_ShowExtraFields.target = EditorGUILayout.ToggleLeft("Show extra fields", m_ShowExtraFields.target);

		//Extra block that can be toggled on and off.
		if (EditorGUILayout.BeginFadeGroup(m_ShowExtraFields.faded))
		{
			EditorGUI.indentLevel++;
			EditorGUILayout.PrefixLabel("Color");
			m_Color = EditorGUILayout.ColorField(m_Color);
			EditorGUILayout.PrefixLabel("Text");
			m_String = EditorGUILayout.TextField(m_String);
			EditorGUILayout.PrefixLabel("Number");
			m_Number = EditorGUILayout.IntSlider(m_Number,0,10);
			EditorGUI.indentLevel--;
		}
		EditorGUILayout.EndFadeGroup();
		//EditorGUILayout.EndVertical();


		EditorGUILayout.BeginVertical("button",GUILayout.ExpandHeight(true));


		GUILayout.Label ("Base Settings", EditorStyles.boldLabel);
		myString = EditorGUILayout.TextField ("Text Field", myString);
		groupEnabled = EditorGUILayout.BeginToggleGroup ("Optional Settings", groupEnabled);
		myBool = EditorGUILayout.Toggle ("Toggle", myBool);
		myFloat = EditorGUILayout.Slider ("Slider", myFloat, -3, 3);
		EditorGUILayout.EndToggleGroup ();

		EditorGUILayout.EndVertical();


		//EditorGUI.DrawPreviewTexture

	}

	//OnDestroy	OnDestroy is called when the EditorWindow is closed.
	//OnFocus	Called when the window gets keyboard focus.
	//OnGUI	Implement your own editor GUI here.
	//OnHierarchyChange	Called whenever the scene hierarchy has changed.
	//OnInspectorUpdate	OnInspectorUpdate is called at 10 frames per second to give the inspector a chance to update.
	//OnLostFocus	Called when the window loses keyboard focus.
	//OnProjectChange	Called whenever the project has changed.
	//OnSelectionChange	Called whenever the selection has changed.
	//Update	Called multiple times per second on all visible windows.
}