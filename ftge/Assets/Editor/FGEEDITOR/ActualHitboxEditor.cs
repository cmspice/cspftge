﻿using UnityEditor;
using UnityEngine;
using UnityEditor.AnimatedValues;
using System.Collections.Generic;
using System;
using System.Linq;
using System.IO;
using FGE;


public class ActualHitboxEditor : EditorWindow
{
	[MenuItem("Window/ActualHitboxEditor")]
	public static void ShowWindow()
	{
		EditorWindow.GetWindow<ActualHitboxEditor>();
	}

	//UI styles
	GUIStyle listUnselected, listSelected;

	FrameSequenceSerializer helper = new FrameSequenceSerializer ();

	void OnEnable(){
		Debug.Log ("LAUNCHING HITBOX EDITOR");
		animEntities = new AnimBool (false);
		animAnimations = new AnimBool(false);	
		animFrames = new AnimBool(false);	
		animHitboxes = new AnimBool(false);	
		animTextures = new AnimBool (false);
		animEntities.valueChanged.AddListener (Repaint);
		animAnimations.valueChanged.AddListener (Repaint);
		animFrames.valueChanged.AddListener (Repaint);
		animHitboxes.valueChanged.AddListener (Repaint);
		animTextures.valueChanged.AddListener (Repaint);
		helper.Refresh ();

		listUnselected = new GUIStyle ();
		listUnselected.onHover.background = EditorGUIUtility.whiteTexture;
		listSelected = new GUIStyle ();
		listSelected.normal.background = EditorGUIUtility.whiteTexture;

		Init ();
	}

	void OnProjectChange(){
		//TODO regenerate file lists
	}

	public void Init() {
		foreach (var e in AssetDatabase.GetSubFolders("Assets/EditorOutput/Resources")) {
			Debug.Log ("LOADING ENTITY " + e);
			var entity = EditorEntity.CreateEntity (e);
			helper.entities.Add(entity);

			/* CAN DELETE, just hacks for bulk editing, don't mind me
			foreach (var f in entity.frameSequences) {
				foreach (var g in f.frames) {
					if (g.frame.hitbox.Count > 0) {
						var nhb = new Hitbox ();
						nhb.name = "body";
						nhb.mask = HitboxMask.BODY | HitboxMask.CANHIT | HitboxMask.CANGRAB;
						nhb.box = new Rectangle (new Point (50, 30), new Point (125, 200));
						g.frame.hitbox [0] = nhb;
					}
				}
			}*/

			/* CAN DELETE
			foreach (var f in entity.frameSequences) {
				foreach (var g in f.frames) {
					var temp = g.frame.sprite.Value;
					temp.texture = AssetDatabase.GetAssetPath (g.texture);
					g.frame.sprite = temp;
					Debug.Log (g.frame.sprite.Value.texture);
				}
			}*/
		}

		/*
		var newEntity = EditorEntity.CreateEntity ();
		//TODO make sure this name does not repeat
		newEntity.name = "New Entity " + helper.entities.Count;
		helper.entities.Add (newEntity);
		helper.currentEntity = 0;

		var fs = helper.CurrentEntity.frameSequences;
		var newfs = EditorFrameSequence.CreateNew ();
		newfs.sequence.name = "New Frame Sequence " + fs.Count;
		fs.Add (newfs);
		helper.CurrentEntity.currentFrameSequence = 0;

		var frames = helper.CurrentSequence.frames;
		var newFrame = new EditorFrame ();
		newFrame.frame.name = "Frame " + frames.Count;
		frames.Add (newFrame);
		helper.CurrentSequence.currentFrame = 0;

		var frame = helper.CurrentFrame;
		var newHitbox = new Hitbox ();
		newHitbox.name = "hitbox " + frame.frame.hitbox.Count;
		frame.frame.hitbox.Add (newHitbox);
		helper.CurrentFrame.currentHitbox = 0;*/

	}

	/* currently not worth making this generic, syntax way too cumbersome. Just copypasta
	void ShowAddableList<T>(ref int currentSelection, ref Vector2 scroll, IEnumerable<T> list, Func<T,string> getNameFunc, Action addNewFunc)
	{
		//TODO make this generic
		for(int i = 0; i < list.Count(); i++){
			if (i == currentSelection) {
				GUILayout.Button (getNameFunc(list [i]), listSelected);
			} else if (GUILayout.Button (helper.entities [i].name, listUnselected)) {
				currentSelection = i;
			}
		}
		if (GUILayout.Button ("+", listUnselected)) {
			addNewFunc ();
			helper.currentEntity = helper.entities.Count - 1;
			var newScroll = scroll;
			newScroll.y += 100; //auto scroll to bottom when adding
			scroll = newScroll;
		}
	}*/

	void ShowEntities()
	{
		EditorGUI.indentLevel++;

		for(int i = 0; i < helper.entities.Count; i++){
			if (i == helper.currentEntity) {
				GUILayout.Button (helper.entities [i].name, listSelected);
			} else if (GUILayout.Button (helper.entities [i].name, listUnselected)) {
				helper.currentEntity = i;
			}
		}
		if (GUILayout.Button ("+", listUnselected)) {
			var newEntity = EditorEntity.CreateEntity ();
			//TODO make sure this name does not repeat
			newEntity.name = "New Entity " + helper.entities.Count;
			helper.entities.Add (newEntity);
			helper.currentEntity = helper.entities.Count - 1;
			var scroll = scrollEntities;
			scroll.y += 100; //auto scroll to bottom when adding
			scrollEntities = scroll;
		}

		EditorGUI.indentLevel--;
	}

	void ShowAnimations()
	{
		if (helper.CurrentEntity == null)
			return;
		EditorGUI.indentLevel++;
		var fs = helper.CurrentEntity.frameSequences;
		for(int i = 0; i < fs.Count; i++){
			if (i == helper.CurrentEntity.currentFrameSequence) {
				GUILayout.Button (fs [i].sequence.name, listSelected);
			} else if (GUILayout.Button (fs [i].sequence.name, listUnselected)) {
				helper.CurrentEntity.currentFrameSequence = i;
			}
		}
		if (GUILayout.Button ("+", listUnselected)) {
			var newfs = EditorFrameSequence.CreateNew ();
			newfs.sequence.name = "New Frame Sequence " + fs.Count;
			fs.Add (newfs);
			helper.CurrentEntity.currentFrameSequence = fs.Count - 1;
			var scroll = scrollAnimations;
			scroll.y += 100; //auto scroll to bottom when adding
			scrollAnimations = scroll;
		}

		EditorGUI.indentLevel--;
	}

	void ShowFrames()
	{
		//TODO multiselect
		if (helper.CurrentSequence == null)
			return;
		var frames = helper.CurrentSequence.frames;
		
		EditorGUI.indentLevel++;
		for(int i = 0; i < frames.Count; i++){
			EditorGUILayout.BeginHorizontal ();
			if (i == helper.CurrentSequence.currentFrame) {
				GUILayout.Button (frames [i].frame.name, listSelected);
			} else if (GUILayout.Button (frames [i].frame.name, listUnselected)) {
				helper.CurrentSequence.currentFrame = i;
			}
			if (GUILayout.Button ("[X]",GUIStyle.none,GUILayout.Width(15))) {
				helper.CurrentSequence.currentFrame -= 1;
				frames.RemoveAt (i);
				//TODO go through frame parameters and adjust indices
			}
			EditorGUILayout.EndHorizontal ();
		}
		if (GUILayout.Button ("+", listUnselected)) {
			var newFrame = new EditorFrame ();
			newFrame.frame.name = "Frame " + frames.Count;
			frames.Add (newFrame);
			helper.CurrentSequence.currentFrame = frames.Count - 1;
			var scroll = scrollFrames;
			scroll.y += 100; //auto scroll to bottom when adding
			scrollFrames = scroll;
		}

		EditorGUI.indentLevel--;
	}

	void ShowHitboxes()
	{
		if (helper.CurrentSequence == null)
			return;
		var frame = helper.CurrentSequence.CurrentFrame;
		if (frame == null)
			return;
		
		EditorGUI.indentLevel++;
		bool recenter = false;
		for(int i = 0; i < frame.frame.hitboxGroup.hitboxes.Count; i++){
			EditorGUILayout.BeginHorizontal ();
			if (i == frame.currentHitbox) {
				GUILayout.Button (frame.frame.hitboxGroup.hitboxes[i].name, listSelected);
			} else if (GUILayout.Button (frame.frame.hitboxGroup.hitboxes[i].name, listUnselected)) {
				frame.currentHitbox = i;
				recenter = true;
			}
			if (GUILayout.Button ("[X]",GUIStyle.none,GUILayout.Width(15))) {
				frame.currentHitbox -= 1;
				frame.frame.hitboxGroup.hitboxes.RemoveAt (i);
			}
			EditorGUILayout.EndHorizontal ();
		}
		if (GUILayout.Button ("+", listUnselected)) {
			var newHitbox = new Hitbox ();
			//TODO unique name
			newHitbox.name = "hitbox " + frame.frame.hitboxGroup.hitboxes.Count;

			//just temp stuff for robot
			//TODO delete
			newHitbox.mask = HitboxMask.BODY | HitboxMask.CANHIT | HitboxMask.CANGRAB;
			newHitbox.box = new Rectangle (new Point (50, 30), new Point (125, 200));

			frame.frame.hitboxGroup.hitboxes.Add (newHitbox);
			frame.currentHitbox = frame.frame.hitboxGroup.hitboxes.Count - 1;
			var scroll = scrollHitboxes;
			scroll.y += 100; //auto scroll to bottom when adding
			scrollHitboxes = scroll;
		}

		if (recenter) { 
			//TODO recenter  camera to hitbox
		}

		EditorGUI.indentLevel--;	
	}

	void TextEntryField(string label, ref string value, bool isNull)
	{
		EditorGUILayout.BeginHorizontal (GUILayout.ExpandWidth (true));
		EditorGUILayout.LabelField (label+":",GUILayout.Width(50));
		if (isNull) {
			EditorGUILayout.LabelField ("<none>");
		} else {
			
			var temp = EditorGUILayout.TextField (value);
			if (temp != value) {
				value = temp;
			}
		}
		EditorGUILayout.EndHorizontal ();
	}

	void ShowInfo()
	{
		string dummy = "";

		if (helper.CurrentEntity == null) {
			TextEntryField ("Entity", ref dummy, true);
		} else {
			TextEntryField ("Entity", ref helper.CurrentEntity.name, false);
			var e = helper.CurrentEntity;
			int objCount = EditorGUILayout.IntField("objects: ", e.objects.Length);
			Array.Resize (ref e.objects, objCount);
			//animIndexedFrameParameterCancels.target = EditorGUILayout.Foldout(animIndexedFrameParameterCancels.target,"show");
			showIndexedFrameParameterCancels = EditorGUILayout.Foldout(showIndexedFrameParameterCancels,"list");
			if (showIndexedFrameParameterCancels) {
				for (int i = 0; i < e.objects.Length; i++) {
					var tc = e.objects[i];
					e.objects[i] = EditorGUILayout.ObjectField (tc, typeof(UnityEngine.Object), false);
				}
			}
		}


		var cs = helper.CurrentSequence;
		if (cs != null) {
			GUILayout.Box("",GUI.skin.horizontalSlider);

			//EditorGUILayout.LabelField ("Sequence: " + cs.sequence.name);

			//name
			TextEntryField("Sequence",ref cs.sequence.name,false);
			TextEntryField("Default transition",ref cs.sequence.defaultTransition, false);
			TextEntryField("Input Combo", ref cs.sequence.inputComboString, false);

			//tags
			string tags = (cs.sequence.tags == null || cs.sequence.tags.Length == 0) ? "" : cs.sequence.tags.Aggregate ((a, b) => a + ", " + b);
			string newTags = tags;
			TextEntryField ("Tags", ref newTags, false);
			if (newTags != tags) {
				cs.sequence.tags = newTags.Split (",".ToCharArray (), StringSplitOptions.RemoveEmptyEntries).Select (e => e.Trim ()).ToArray ();
			}
			EditorGUILayout.LabelField ("# frames: " + cs.frames.Count);
				
		} else {
			EditorGUILayout.LabelField ("No FrameSequences selected");
		}

		if (helper.CurrentFrame != null) {
			var frame = helper.CurrentSequence.CurrentFrame;
			GUILayout.Box ("", GUI.skin.horizontalSlider);
			TextEntryField ("Frame", ref frame.frame.name, false);
			
			var newSprite = (Sprite)EditorGUILayout.ObjectField (new GUIContent ("Sprite: ", "setting as sprite will use crop settings from sprite"), frame.sprite, typeof(Sprite), false);

			if (newSprite != frame.sprite) {
				frame.sprite = newSprite;
				frame.texture = frame.sprite.texture;

				//TODO get unique identifier for texture
				var sprite = frame.frame.sprite.Value;
				sprite.texture = AssetDatabase.GetAssetPath (frame.sprite.texture);
				sprite.crop = frame.sprite.rect.ToFGERectangle ();
				frame.frame.sprite = sprite;
			}
				
			var newTex = (Texture2D)EditorGUILayout.ObjectField (new GUIContent ("Texture: ", "setting this will override sprite"), frame.texture, typeof(Texture2D), false);

			if (newTex != frame.texture) {
				frame.texture = newTex;
				frame.sprite = null;
				var sprite = frame.frame.sprite.Value;
				//TODO should refer to a unique identifier for the texture
				sprite.texture = AssetDatabase.GetAssetPath(frame.texture);
				frame.frame.sprite = sprite;
			}

			var rect = frame.frame.sprite.Value.crop.ToUnityRect();
			var newRect = EditorGUILayout.RectField (new GUIContent ("Bounds:"), rect);
			if (rect != newRect) {
				var temp = frame.frame.sprite.Value;
				temp.crop = newRect.ToFGERectangle ();
				frame.frame.sprite = temp;
			}

			var offset = new Vector2 (frame.frame.sprite.Value.offset.x, frame.frame.sprite.Value.offset.y);
			var newOffset = EditorGUILayout.Vector2Field (new GUIContent ("Offset"), offset);
			if (offset != newOffset) {
				var temp = frame.frame.sprite.Value;
				temp.offset = new Point(newOffset.x,newOffset.y);
				frame.frame.sprite = temp;
			}

		} else {
			EditorGUILayout.LabelField ("No frame selected");
		}

		if (helper.CurrentFrame != null && helper.CurrentFrame.CurrentHitbox != null) {
			Hitbox hitbox = helper.CurrentFrame.CurrentHitbox.Value;

			GUILayout.Box ("", GUI.skin.horizontalSlider);
			TextEntryField ("Hitbox", ref hitbox.name, false);
			Rect rect = hitbox.box.ToUnityRect ();
			Rect newRect = EditorGUILayout.RectField (new GUIContent ("Bounds:"), rect);
			if (rect != newRect) {
				hitbox.box = newRect.ToFGERectangle ();
			}

			//checkbox version
			/*EditorGUILayout.LabelField ("Layer mask:");
			int layer = (int)hitbox.mask;
			for (int i = 0; i < 10; i++) {
				EditorGUILayout.BeginHorizontal (GUILayout.ExpandWidth (true));
				bool tog = EditorGUILayout.Toggle ((layer & (1 << i)) != 0,GUILayout.Width(25));
				string label = ((HitboxMask)(1 << i)).ToString ();
				if (System.Text.RegularExpressions.Regex.IsMatch (label, @"^\d+$")) {
					label = i.ToString ();
				}
				EditorGUILayout.LabelField (label);
				EditorGUILayout.EndHorizontal ();
				layer = (layer & (~(1 << i))) | ((tog ? 1 : 0) << i);
			}
			hitbox.mask = (HitboxMask)layer;*/
			hitbox.mask = (HitboxMask)EditorGUILayout.EnumMaskField ("hitbox mask: ", hitbox.mask);
			//hitbox.mask = (HitboxMask)EditorGUILayout.EnumMaskPopup (new GUIContent("hitbox mask: "), hitbox.mask);

			helper.CurrentFrame.CurrentHitbox = hitbox;
		} else {
			EditorGUILayout.LabelField ("No hitbox selected");
		}

		GUILayout.Space (10);

		//TODO this is just hack way to edit frame parameters for now
		if (helper.CurrentSequence != null) {
			EditorGUILayout.BeginVertical (GUILayout.ExpandWidth (true));
			if (helper.CurrentSequence.sequence.serializedFrameParameters.Count > 0) {
				ShowIndexedFrameParameterOptions (helper.CurrentSequence.sequence.serializedFrameParameters [0]);
			}
			EditorGUILayout.EndVertical ();
		}


		GUILayout.Space (10);

		if (helper.CurrentEntity != null) {
			EditorGUILayout.BeginHorizontal (GUILayout.ExpandWidth(true));
			if (GUILayout.Button ("SAVE")) {
				helper.CurrentEntity.SAVE ();
			}
			if (GUILayout.Button ("EXPORT")) {
				helper.CurrentEntity.SAVE (true);
			}
			EditorGUILayout.EndHorizontal ();
		}

		GUILayout.Space (20);
	}

	void MakeVerticalGroup(string name, AnimBool animator, ref Vector2 scroll, Action func)
	{
		EditorGUILayout.BeginVertical("button");
		animator.target = EditorGUILayout.Foldout(animator.target,name);
		if (EditorGUILayout.BeginFadeGroup(animator.faded)){
			scroll = EditorGUILayout.BeginScrollView (scroll, false, false, GUIStyle.none, GUI.skin.verticalScrollbar, GUIStyle.none, GUILayout.Height (100));
			func ();
			EditorGUILayout.EndScrollView ();
		}
		EditorGUILayout.EndFadeGroup();
		EditorGUILayout.EndVertical();
	}


	//UI vars
	AnimBool animEntities, animAnimations, animFrames, animHitboxes, animTextures;
	Vector2 scrollEntities, scrollAnimations, scrollFrames, scrollHitboxes, scrollTextures;
	Rect rectLeft, rectRight;
	Vector2 leftScroll;
	Vector2 rightScroll;
	float zoom = 1;
	int onionForward = 0;
	int onionBack = 0;
	bool addPos = false;
	bool addVel = false;


	void DrawOnionSkin(Vector2 center, int lookback, int lookforward)
	{
		int cf = helper.CurrentSequence.currentFrame;
		for (int i = cf - lookback; i < cf + lookforward + 1; i++) {
			if (i == cf)
				continue;
			helper.CurrentSequence.currentFrame = i;
			if (helper.CurrentFrame != null && helper.CurrentFrame.texture != null) {
				Rect textureScaledRect = new Rect ();
				textureScaledRect.x = textureScaledRect.y = 0;
				textureScaledRect.size = new Vector2(helper.CurrentFrame.texture.width,helper.CurrentFrame.texture.height) * zoom;
				Rect textureOrigRect = new Rect (0, 0, helper.CurrentFrame.texture.width, helper.CurrentFrame.texture.height);
				Rectangle spriteOrigCrop = helper.CurrentFrame.frame.sprite.Value.crop;
				Rectangle spriteOrigCenter = new Rectangle (spriteOrigCrop.corner + helper.CurrentFrame.frame.sprite.Value.offset, Point.zero);
				Rect spriteRightScrollCropRect = FrameRectToRightScrollRect(spriteOrigCrop,textureOrigRect.ToFGERectangle(),textureOrigRect);
				Vector2 offset = FrameRectToRightScrollRect(spriteOrigCenter,textureOrigRect.ToFGERectangle(),textureOrigRect).position;
				Vector2 drawPosition = center - offset;

				//TODO integrate dp + dv*t from frame parameters and add to offset

				var offsetSpriteRightScrollCropRect = spriteRightScrollCropRect;
				offsetSpriteRightScrollCropRect.center += drawPosition;
				GUI.BeginGroup (offsetSpriteRightScrollCropRect);
				GUI.DrawTexture(new Rect (-spriteOrigCrop.corner.x*zoom, //this rect is relative to rect passed into GUI.BeginGroup
					-spriteOrigCrop.corner.y*zoom,
					textureScaledRect.width,
					textureScaledRect.height
				),helper.CurrentFrame.texture,ScaleMode.StretchToFill);
				GUI.EndGroup();


				//TODO draw hitboxes
			}
		}
		helper.CurrentSequence.currentFrame = cf;
	}

	void OnGUI()
	{
		EditorGUILayout.BeginHorizontal (GUILayout.ExpandWidth(true)); //everythnig

		leftScroll = EditorGUILayout.BeginScrollView(leftScroll,GUILayout.Width (270)); //left
		MakeVerticalGroup ("Entities", animEntities, ref scrollEntities, ShowEntities);
		MakeVerticalGroup ("Animations", animAnimations,ref scrollAnimations,ShowAnimations);
		MakeVerticalGroup ("Frames", animFrames, ref scrollFrames, ShowFrames);
		MakeVerticalGroup ("Hitboxes", animHitboxes, ref scrollHitboxes, ShowHitboxes);
		EditorGUILayout.BeginVertical(GUILayout.ExpandHeight(true));
		ShowInfo ();
		EditorGUILayout.EndVertical ();
		EditorGUILayout.EndScrollView(); //left


		EditorGUILayout.BeginVertical (GUILayout.ExpandWidth (true), GUILayout.ExpandHeight (true)); //right
		rightScroll = EditorGUILayout.BeginScrollView (rightScroll, true, true,GUI.skin.horizontalScrollbar,GUI.skin.verticalScrollbar,GUI.skin.textArea);

		//TODO checkerobard background for rightscroll (use guiskin)

		//hitbox/sprite editor here
		if(helper.CurrentFrame != null && helper.CurrentFrame.texture != null){

			//Compute the area of the Texture that is our sprite
			Rect textureScaledRect = new Rect ();
			textureScaledRect.x = textureScaledRect.y = 0;
			textureScaledRect.size = new Vector2(helper.CurrentFrame.texture.width,helper.CurrentFrame.texture.height) * zoom;
			Rect textureOrigRect = new Rect (0, 0, helper.CurrentFrame.texture.width, helper.CurrentFrame.texture.height);
			Rectangle spriteOrigCrop = helper.CurrentFrame.frame.sprite.Value.crop;
			Rectangle spriteOrigCenter = new Rectangle (spriteOrigCrop.corner + helper.CurrentFrame.frame.sprite.Value.offset, Point.zero);
			Rect spriteRightScrollCropRect = FrameRectToRightScrollRect(spriteOrigCrop,textureOrigRect.ToFGERectangle(),textureOrigRect);
			Vector2 spriteRightScrollCenter = FrameRectToRightScrollRect(spriteOrigCenter,textureOrigRect.ToFGERectangle(),textureOrigRect).position;

			//Draw previous onion skins
			DrawOnionSkin(spriteRightScrollCenter,onionBack,0);

			//Draw the texture
			GUI.DrawTexture(textureScaledRect,helper.CurrentFrame.texture);
			//GUILayout.Box (helper.CurrentFrame.texture);
			//EditorGUI.DrawPreviewTexture(texRect, helper.CurrentFrame.texture);
			//force scrollview to expand
			GUILayout.Space (textureScaledRect.height);
			GUILayout.BeginHorizontal();
			GUILayout.Space (textureScaledRect.width);
			GUILayout.EndHorizontal();

			//Draw future onion skins
			DrawOnionSkin(spriteRightScrollCenter,0,onionForward);



			//Draw the crop rect
			Rect newSpriteCropRect = spriteRightScrollCropRect;
			//TODO needs to decide if our focus is on frame or on hitbox and do box drawing for only that
			//HandleBoxDrawing (ref newRect);
			if (spriteRightScrollCropRect != newSpriteCropRect) {
				var temp = helper.CurrentFrame.frame.sprite.Value;
				temp.crop = RightScrollRectToFrameRect(newSpriteCropRect,textureOrigRect.ToFGERectangle(),textureOrigRect);
				helper.CurrentFrame.frame.sprite = temp;
				//TODO shift hitboxes after moving sprite crop frame (maybe)
			}
			//TODO if we are in the middle of editing, this should draw in a darker color
			//otherwise, draw in a lighter color
			EditorGUI.DrawRect (newSpriteCropRect, new Color (1f, 0f, 0f, 0.5f));

			//TODO draw a point for the center

			//Draw all hitboxes
			foreach (var e in helper.CurrentFrame.frame.hitboxGroup.hitboxes) {
				//TODO draw with different color
				EditorGUI.DrawRect(FrameRectToRightScrollRect(e.box,spriteOrigCrop,textureOrigRect),new Color(.5f,.5f,.5f,.5f));
			}

			//Draw/edit current hitobx
			if (helper.CurrentFrame.CurrentHitbox != null) {
				Hitbox hitbox = helper.CurrentFrame.CurrentHitbox.Value;
				Rect hitboxRect = FrameRectToRightScrollRect(hitbox.box,spriteOrigCrop,textureOrigRect);
				Rect newHitboxRect = hitboxRect;
				HandleBoxDrawing (ref newHitboxRect);
				if (hitboxRect != newHitboxRect) {
					hitbox.box = RightScrollRectToFrameRect(newHitboxRect,spriteOrigCrop,textureOrigRect);
					helper.CurrentFrame.CurrentHitbox = hitbox;
				}

				EditorGUI.DrawRect (FrameRectToRightScrollRect(hitbox.box,spriteOrigCrop,textureOrigRect), Color.cyan);
			}
		}
		EditorGUILayout.EndScrollView (); //right scroll
		rectRight = GUILayoutUtility.GetLastRect ();

		EditorGUILayout.BeginHorizontal (); //bottom nav



		EditorGUILayout.LabelField ("Zoom: ",GUILayout.Width(50));
		zoom = EditorGUILayout.Slider(zoom,0.2f,5);

		//TODO vertical divider
		GUILayout.Space (20);

		EditorGUILayout.LabelField ("Onion: back|forward",GUILayout.Width(115));
		onionBack = Mathf.Clamp(EditorGUILayout.IntField (onionBack,GUILayout.Width(30)),0,5);
		onionForward = Mathf.Clamp(EditorGUILayout.IntField (onionForward,GUILayout.Width(30)),0,5);

		EditorGUILayout.LabelField ("add pos|vel",GUILayout.Width(65));
		addPos = EditorGUILayout.Toggle (addPos,GUILayout.Width(15));
		addVel = EditorGUILayout.Toggle (addVel,GUILayout.Width(15));

		//TODO keep image centered when zooming
		//var newZoom = EditorGUILayout.Slider("zoom:",zoom,1,10);
		//rightScroll += rectRight.size * (newZoom-zoom)/newZoom;
		//zoom = newZoom;

		EditorGUILayout.EndHorizontal (); //bottom nav

		EditorGUILayout.EndVertical (); //right

		EditorGUILayout.EndHorizontal (); //everything


	}


	//mouse vars
	bool rightMouseDown = false;
	Vector2 rightMouseDownPosition;
	Rect origRect;
	public void HandleBoxDrawing(ref Rect box)
	{
		
		//TODO abstract this away because we need it for drawing sprite boundaries as well
		if ((Event.current.type == EventType.MouseDown) && (Event.current.button == 0)) {
			origRect = box;
			rightMouseDownPosition = Event.current.mousePosition;
			rightMouseDown = true;

		//TODO did we click on previous rect, if so initiate dragging
		} else if (rightMouseDown && (Event.current.type == EventType.MouseDrag) && (Event.current.button == 0)) {

			//TODO only if mouse has moved beyond a threshold
			var rect = RectExtensions.FromCorners (rightMouseDownPosition, Event.current.mousePosition);
			//TODO offset rect
			box = rect;
			Repaint ();
		} else if (rightMouseDown && (Event.current.type == EventType.MouseUp) && (Event.current.button == 0)) {
			rightMouseDown = false;	
		}

		if ((Event.current.type == EventType.KeyDown) && (Event.current.keyCode == KeyCode.Escape)) {
			box = origRect;
			rightMouseDown = false;
		}

	}

	Rect RectToRightScrollRect(Rect rect){
		rect.x = rect.x * zoom;
		rect.y = rect.y * zoom;
		rect.size *= zoom;
		return rect;
	}

	Rect RightScrollRectToRect(Rect rect){
		rect.x = rect.x / zoom;
		rect.y = rect.y / zoom;
		rect.size /= zoom;
		return rect;
	}


	//GUI is from upper left, but we store everything from lower left relative to the cropping rectangle
	//pass in (crop = total) if there is no sprite crop box
	//total should be size of the texture that crop is relative to (no zooming)
	Rect FrameRectToRightScrollRect(Rectangle rect, Rectangle crop, Rect total){
		Point scrollCropCorner = new Point (crop.corner.x, total.height - (crop.corner.y + crop.size.y));
		Point ULRelCorner = new Point (rect.corner.x, -rect.corner.y + crop.size.y - rect.size.y);
		Rectangle absolute = new Rectangle (scrollCropCorner + ULRelCorner, rect.size);
		return RectToRightScrollRect(absolute.ToUnityRect ());
	}
	Rectangle RightScrollRectToFrameRect(Rect rect, Rectangle crop, Rect total){
		Rectangle absolute = RightScrollRectToRect (rect).ToFGERectangle ();
		Point DLCorner = new Point (absolute.corner.x, total.size.y - (absolute.corner.y + absolute.size.y));
		Rectangle relative = new Rectangle (DLCorner - crop.corner, absolute.size);
		return relative;
	}



	void NullableIntField(string label, ref int? value)
	{

		EditorGUILayout.BeginHorizontal (GUILayout.ExpandWidth (true));
		EditorGUILayout.LabelField(label,GUILayout.MaxWidth(100));
		bool hasValue = EditorGUILayout.Toggle (value.HasValue);
		if (!hasValue)
			value = null;
		if (hasValue) {
			if (value == null)
				value = 0;
			value = EditorGUILayout.IntField (value.Value, GUILayout.ExpandWidth (true));
		}
		EditorGUILayout.EndHorizontal ();
	}

	void NullableFloatField(string label, ref float? value)
	{

		EditorGUILayout.BeginHorizontal (GUILayout.ExpandWidth (true));
		EditorGUILayout.LabelField(label,GUILayout.MaxWidth(100));
		bool hasValue = EditorGUILayout.Toggle (value.HasValue);
		if (!hasValue)
			value = null;
		if (hasValue) {
			if (value == null)
				value = 0;
			value = EditorGUILayout.FloatField (value.Value, GUILayout.ExpandWidth (true));
		}
		EditorGUILayout.EndHorizontal ();
	}

	void NullableStringField(string label, ref string value)
	{

		EditorGUILayout.BeginHorizontal (GUILayout.ExpandWidth (true));
		EditorGUILayout.LabelField(label,GUILayout.MaxWidth(100));
		bool hasValue = EditorGUILayout.Toggle (value != null);
		if (!hasValue)
			value = null;
		if (hasValue) {
			if (value == null)
				value = "";
			value = EditorGUILayout.TextField (value);
		}
		EditorGUILayout.EndHorizontal ();
	}


	//AnimBool animIndexedFrameParameterCancels = new AnimBool ();
	bool showIndexedFrameParameterCancels = true;
	void ShowIndexedFrameParameterOptions(IndexedFrameParameters parameters)
	{
		EditorGUILayout.BeginHorizontal (GUILayout.ExpandWidth (true)); 
		EditorGUILayout.LabelField ("(start,count,end):",GUILayout.MaxWidth(100));
		parameters.startFrame = EditorGUILayout.IntField (parameters.startFrame,GUILayout.Width(20));
		parameters.count = EditorGUILayout.IntField (parameters.count,GUILayout.Width(20));
		EditorGUILayout.LabelField ((parameters.startFrame + parameters.count - 1).ToString(),GUILayout.Width(20));
		EditorGUILayout.EndHorizontal ();

		FrameParameters temp = parameters.parameters;
		NullableIntField ("duration: ", ref temp.duration);
		NullableIntField ("pause: ", ref temp.pause);

		NullableFloatField ("dx: ", ref temp.dx);
		NullableFloatField ("dy: ", ref temp.dy);
		NullableFloatField ("dvx: ", ref temp.dvx);
		NullableFloatField ("dvy: ", ref temp.dvy);
		NullableFloatField ("reset vx: ", ref temp.resetvx);
		NullableFloatField ("reset vy: ", ref temp.resetvy);

		NullableIntField ("damage: ", ref temp.damage);
		NullableIntField ("block stun: ", ref temp.blockStun);
		NullableIntField ("hit stun: ", ref temp.hitStun);
		NullableIntField ("priority: ", ref temp.priority);
		NullableStringField ("type: ", ref temp.hitType);

		if(temp.cancels == null)
			temp.cancels = new ConditionalCancel[0];
		int cancelCount = EditorGUILayout.IntField("Cancels: ", temp.cancels.Length);
		Array.Resize (ref temp.cancels, cancelCount);
		//animIndexedFrameParameterCancels.target = EditorGUILayout.Foldout(animIndexedFrameParameterCancels.target,"show");
		showIndexedFrameParameterCancels = EditorGUILayout.Foldout(showIndexedFrameParameterCancels,"list");
		if (showIndexedFrameParameterCancels) {
			for (int i = 0; i < temp.cancels.Length; i++) {
				var tc = temp.cancels [i];
				if (tc.cancel == null) tc.cancel = ""; //"" and null are same, and there's no need to use NullableStringField
				TextEntryField("string: ",ref tc.cancel, false);
				NullableStringField ("condition: ", ref tc.condition);
				NullableIntField ("override priority: ", ref tc.overridePriority);
				NullableStringField ("override combo: ", ref tc.overrideComboString);
				temp.cancels [i] = tc;
			}
		}
		if (temp.cancels.Length == 0)
			temp.cancels = null;
		parameters.parameters = temp;
	}








	//OnDestroy	OnDestroy is called when the EditorWindow is closed.
	//OnFocus	Called when the window gets keyboard focus.
	//OnGUI	Implement your own editor GUI here.
	//OnHierarchyChange	Called whenever the scene hierarchy has changed.
	//OnInspectorUpdate	OnInspectorUpdate is called at 10 frames per second to give the inspector a chance to update.
	//OnLostFocus	Called when the window loses keyboard focus.
	//OnProjectChange	Called whenever the project has changed.
	//OnSelectionChange	Called whenever the selection has changed.
	//Update	Called multiple times per second on all visible windows.
}