﻿using Newtonsoft.Json;
using FGE;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.IO;



public class EditorFrame
{
	public int currentHitbox = -1;
	public Frame frame;

	//other variables used while editting go here
	public Sprite sprite = null;
	public Texture2D texture = null;



	public EditorFrame(Frame _frame = null){
		frame = _frame;
		if (frame == null) {
			frame = new Frame ();
			frame.sprite = new SpriteInfo ();
		}
	}

	public Hitbox? CurrentHitbox{
		get{
			if (currentHitbox == -1 || currentHitbox >= frame.hitboxGroup.hitboxes.Count)
				return null;
			return frame.hitboxGroup.hitboxes [currentHitbox];
		}
		set{
			if(value.HasValue)
				frame.hitboxGroup.hitboxes [currentHitbox] = value.Value;
		}
	}
}

public class EditorFrameSequence
{
	public int currentFrame = -1; //TODO eventually we want to do multiselect here
	public FrameSequence sequence;
	public List<EditorFrame> frames;

	public static EditorFrameSequence CreateNew(){
		EditorFrameSequence r = new EditorFrameSequence ();
		r.sequence = new FrameSequence ();
		r.frames = new List<EditorFrame> ();
		return r;
	}

	public static EditorFrameSequence CreateFromeFile(TextAsset sequenceFile, TextAsset frameFile, Dictionary<string,string> assetMap){

		//Debug.Log("deserializing " + sequenceFile.name + " and " + frameFile.name);
		EditorFrameSequence r = new EditorFrameSequence ();
		r.sequence = JsonConvert.DeserializeObject<FrameSequence> (sequenceFile.text);
		if (frameFile != null) { //this should never happen, but it's okay if the frames are null
			r.frames = JsonConvert.DeserializeObject<Frame[]> (frameFile.text).Select (e => new EditorFrame (e)).ToList ();
		} else {
			r.frames = new List<EditorFrame> ();
			Debug.LogWarning ("No frames found for " + r.sequence.name);
		}
		foreach (var e in r.frames) {

			//TODO add support for prefab and other non sprite types...

			if (e.frame.sprite == null)
				continue;
				//e.frame.sprite = new SpriteInfo ();
			
			string id = "";
			//var key = r.sequence.name + "/" + e.frame.name + "/" + e.frame.sprite.texture;
			var key = e.frame.sprite.Value.texture; 
			if (key == null)
				continue;
			assetMap.TryGetValue (key, out id);
			var asset = AssetDatabase.LoadAssetAtPath<Object>(AssetDatabase.GUIDToAssetPath (id));
			if (asset is Sprite) {
				e.sprite = asset as Sprite;
				e.texture = e.sprite.texture;
			} else if (asset is Texture2D) {
				e.texture = asset as Texture2D;
			} else {
				Debug.LogWarning ("Could not find sprite or texture " + e.frame.sprite.Value.texture + " for " + r.sequence.name + "/" + e.frame.name + " id: " + id);
			}
		}
		return r;
	}

	public void WriteToFile(string folder){
		List<string> r = new List<string>();
		var jsonSettings = new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore };
		jsonSettings.Formatting = Formatting.Indented;
		var seqText = JsonConvert.SerializeObject (sequence,jsonSettings);
		seqText = seqText.Replace("\r\n", "\n");
		var frameText = JsonConvert.SerializeObject (frames.Select(e=>e.frame).ToArray(),jsonSettings);
		frameText = frameText.Replace("\r\n", "\n");
		System.IO.Directory.CreateDirectory (Application.dataPath + "/" + folder);
		string sequencefn = Application.dataPath + "/" + folder + "/" + sequence.name + ".sequence.txt";
		string framefn = Application.dataPath + "/" + folder + "/" + sequence.name + ".frames.txt";
		System.IO.File.WriteAllText (sequencefn, seqText);
		System.IO.File.WriteAllText (framefn, frameText);
		AssetDatabase.Refresh ();
		Debug.Log ("Wrote files for " + sequence.name);
	}

	public EditorFrame CurrentFrame{
		get{
			if (currentFrame <= -1 || currentFrame >= frames.Count)
				return null;
			return frames [currentFrame];
		}
	}
}

public class EditorEntity
{
	public string name = "<!IMPORTANT! give me a unique name>";
	public List<EditorFrameSequence> frameSequences = new List<EditorFrameSequence>();
	public int currentFrameSequence = -1;
	public EntityMetadata entityMetadata;
	public UnityEngine.Object[] objects = new Object[0];

	public EditorFrameSequence CurrentFrameSequence {
		get{
			if (currentFrameSequence <= -1 || currentFrameSequence >= frameSequences.Count)
				return null;
			return frameSequences [currentFrameSequence];
		}
	}

	public static EditorEntity CreateEntity()
	{
		EditorEntity r = new EditorEntity ();
		return r;
	}

	public static EditorEntity CreateEntity(string folder)
	{
		EditorEntity r = new EditorEntity ();
		r.name = Path.GetFileName(folder); 


		TextAsset assetMapAsset = AssetDatabase.LoadAssetAtPath<TextAsset> (folder + "/EditorAssetMap.txt");

		TextAsset entityMetadataAsset = AssetDatabase.LoadAssetAtPath<TextAsset> (folder + "/EntityMetadata.txt"); 
		if (entityMetadataAsset != null) {
			r.entityMetadata = JsonConvert.DeserializeObject<EntityMetadata> (entityMetadataAsset.text);
		}

		Dictionary<string,string> assetMap = new Dictionary<string, string>();
		if (assetMapAsset == null)
			//assetMap = new Dictionary<string, int> ();
			throw new UnityException ("Could not load asset map for " + r.name);
		else
			assetMap = JsonConvert.DeserializeObject<Dictionary<string,string> > (assetMapAsset.text);

		var objectMap = assetMap.Where (e => e.Key.StartsWith ("...OBJECT..."));
		r.objects = objectMap.Select (e => AssetDatabase.LoadAssetAtPath<Object> (AssetDatabase.GUIDToAssetPath (e.Value))).ToArray ();

		foreach (var e in Directory.GetFiles(folder)) {
			if (e.EndsWith (".sequence.txt")) {
				string frameFilename = e.Substring (0, e.Length - ".sequence.txt".Length) + ".frames.txt";
				TextAsset sequence = AssetDatabase.LoadAssetAtPath<TextAsset> (e);
				TextAsset frame = AssetDatabase.LoadAssetAtPath<TextAsset> (frameFilename);
				if (sequence == null || frame == null) {
					throw new UnityException ("Could not load sequence and/or frames for " + e);
				}
				r.frameSequences.Add (EditorFrameSequence.CreateFromeFile (sequence, frame, assetMap));
			}
		}
		return r;
	}


	public void CHECKERRORS()
	{
		//TODO error checking on input combos, cancels, etc
	}
	public void SAVE(bool writeBundle = false)
	{
		CHECKERRORS ();

		//build local assets map
		Dictionary<string,string> assetMap = new Dictionary<string, string>();
		foreach (var e in frameSequences) {
			foreach (var f in e.frames) {
				if (f.sprite != null) {
					//e.sequence.name + "/" + f.frame.name + "/" + 
					assetMap [f.frame.sprite.Value.texture] = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(f.sprite));
					//Debug.Log (AssetDatabase.GUIDToAssetPath (assetMap [f.frame.sprite.texture]));
				} else if (f.texture != null) {
					assetMap [f.frame.sprite.Value.texture] = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(f.texture));
					//Debug.Log (AssetDatabase.GUIDToAssetPath (assetMap [f.frame.sprite.texture]));
				}

				//TODO reference audio
			}
		}

		foreach (var e in objects) {
			if(e != null)
				assetMap["...OBJECT..." + AssetDatabase.GetAssetPath(e)] = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(e));
		}

		string text = JsonConvert.SerializeObject (assetMap);
		System.IO.File.WriteAllText (Application.dataPath + "/EditorOutput/Resources/" + name + "/EditorAssetMap.txt", text);

		List<string> files = new List<string> ();


		entityMetadata.name = name.ToLower ();
		var jsonSettings = new JsonSerializerSettings{ Formatting = Formatting.Indented };
		string metadata = JsonConvert.SerializeObject (entityMetadata,jsonSettings);
		metadata = metadata.Replace("\r\n", "\n");
		System.IO.File.WriteAllText (Application.dataPath + "/EditorOutput/Resources/" + name + "/EntityMetadata.txt", metadata);
		files.Add ("Assets/EditorOutput/Resources/" + name + "/EntityMetadata.txt");

		foreach (var e in frameSequences) {
			e.WriteToFile ("EditorOutput/Resources/" + name);
			files.Add ("Assets/EditorOutput/Resources/" + name + "/" + e.sequence.name + ".frames.txt");
			files.Add ("Assets/EditorOutput/Resources/" + name + "/" + e.sequence.name + ".sequence.txt");
		}

		if (writeBundle) {
			//we save the asset bundle last in case something goes horrible wrong we still managed to save the files
			AssetBundleBuild bundle = new AssetBundleBuild ();
			bundle.assetBundleName = name + ".bundle";
			bundle.assetNames = assetMap.Select (e => AssetDatabase.GUIDToAssetPath (e.Value)).Concat (files).ToArray ();

			//this makes two files for some reason. Ask on unity forums or w/e
			//TODO build for all platforms
			BuildPipeline.BuildAssetBundles (
				"Assets/EditorOutput/Resources/" + name,
				new AssetBundleBuild[]{ bundle },
				BuildAssetBundleOptions.None,
				EditorUserBuildSettings.activeBuildTarget
			);
		}
	}
}

public class FrameSequenceSerializer
{
	public int currentEntity = -1;
	public List<EditorEntity> entities = new List<EditorEntity>();
	public EditorEntity CurrentEntity { 
		get{
			if (currentEntity <= -1 || currentEntity >= entities.Count)
				return null;
			return entities [currentEntity];
		}
	}
	public EditorFrameSequence CurrentSequence {
		get {
			if (CurrentEntity != null) 
				return CurrentEntity.CurrentFrameSequence;
			return null;
		}
	}
	public EditorFrame CurrentFrame{
		get{
			if (CurrentSequence != null)
				return CurrentSequence.CurrentFrame;
			return null;
		}
	}

	public void Refresh()
	{
		//TODO read from data
	}
}