﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using FGE;

using Newtonsoft.Json;
using System.IO;

public class EEDataTotal
{
	List<EEEntity> entities = new List<EEEntity>();
	public int selectedEntity = -1;

	//------------
	//Getters
	//------------

}

public class MultiIndexSelection
{
	//TODO
}

public class EEEntity : ICloneable
{
	List<EEFrameSequence> sequences = new List<EEFrameSequence>();

	//TODO Entity meta data + serialization
	EntityMetadata entityMetaData;

	public object Clone()
	{
		EEEntity r = new EEEntity ();
		r.sequences = sequences.Select (e => (EEFrameSequence)e.Clone ()).ToList();
		r.entityMetaData = entityMetaData;
		return r;
	}
}

public class EEFrameSequence : ICloneable
{
	//serialized info
	public string name;
	public string defaultTransition;
	public int priority;
	public string inputComboString;
	public string[] tags;
	public List<IndexedFrameParameters> serializedFrameParameters = new List<IndexedFrameParameters> (); 
	public List<EEFrame> frames = new List<EEFrame>();

	//selection info
	int selectedFp = -1;

	public object Clone()
	{
		EEFrameSequence r = new EEFrameSequence();
		r.name = name;
		r.defaultTransition = defaultTransition;
		r.priority = priority;
		r.inputComboString = inputComboString;
		r.tags = (string[])tags.Clone();
		r.serializedFrameParameters = serializedFrameParameters.ToList ();
		r.frames = frames.Select (e => (EEFrame)e.Clone ()).ToList();
		return r;
	}

	//---------------------
	//serialization routines
	//---------------------
	public static EEFrameSequence CreateFromeFile(TextAsset sequenceFile, TextAsset frameFile, Dictionary<string,string> assetMap){
		FrameSequence fgeSequence = new FrameSequence ();
		fgeSequence = JsonConvert.DeserializeObject<FrameSequence> (sequenceFile.text);
		Frame[] fgeFrames;
		if (frameFile != null) { //this should never happen, but it's okay if the frames are null
			fgeFrames = JsonConvert.DeserializeObject<Frame[]> (frameFile.text);
		} else {
			fgeFrames = new Frame[0];
			Debug.LogWarning ("No frames found for " + fgeSequence.name);
		}

		EEFrameSequence r = new EEFrameSequence ();
		r.name = fgeSequence.name;
		r.defaultTransition = fgeSequence.defaultTransition;
		r.inputComboString = fgeSequence.inputComboString;
		r.priority = fgeSequence.priority;
		r.serializedFrameParameters = fgeSequence.serializedFrameParameters;

		foreach (var e in fgeFrames) {
			EEFrame frame = new EEFrame ();
			frame.hitboxes = e.hitboxGroup.hitboxes.ToList ();
			frame.name = e.name;

			if (e.sprite.HasValue) {
				string id;
				var key = e.sprite.Value.texture; 
				assetMap.TryGetValue (key, out id);
				var asset = AssetDatabase.LoadAssetAtPath<UnityEngine.Object> (AssetDatabase.GUIDToAssetPath (id));
				//TODO this is not how Sprite assets works. 
				if (asset is Sprite) { 
					frame.sprite = asset as Sprite;
					frame.texture = frame.sprite.texture;
				} else if (asset is Texture2D) {
					frame.texture = asset as Texture2D;
				} else {
					Debug.LogWarning ("Could not find sprite or texture " + e.sprite.Value.texture + " for " + fgeSequence.name + "/" + e.name + " id: " + id);
				}
			}

			r.frames.Add (frame);
		}
		return r;
	}

	public void WriteToFile(string folder){

		FrameSequence write = new FrameSequence ();
		write.name = name;
		write.defaultTransition = defaultTransition;
		write.inputComboString = inputComboString;
		write.priority = priority;
		write.serializedFrameParameters = serializedFrameParameters;

		List<Frame> writeFrames = new List<Frame> ();
		foreach (var e in frames) {
			Frame f = new Frame ();
			f.hitboxGroup.hitboxes = e.hitboxes.ToList ();
			f.name = e.name;
			//f.sprite =  TODO
			writeFrames.Add(f);
		}

		List<string> r = new List<string>();
		var jsonSettings = new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore };
		jsonSettings.Formatting = Formatting.Indented;
		var seqText = JsonConvert.SerializeObject (write,jsonSettings);
		seqText = seqText.Replace("\r\n", "\n");
		var frameText = JsonConvert.SerializeObject (writeFrames.ToArray(),jsonSettings);
		frameText = frameText.Replace("\r\n", "\n");
		System.IO.Directory.CreateDirectory (Application.dataPath + "/" + folder);
		string sequencefn = Application.dataPath + "/" + folder + "/" + name + ".sequence.txt";
		string framefn = Application.dataPath + "/" + folder + "/" + name + ".frames.txt";
		System.IO.File.WriteAllText (sequencefn, seqText);
		System.IO.File.WriteAllText (framefn, frameText);
		AssetDatabase.Refresh ();
		Debug.Log ("Wrote files for " + name);
	}

}

public class EEFrame : ICloneable
{
	//serialized info
	public string name;
	public List<Hitbox> hitboxes;
	//TODO crop, offset
	public Sprite sprite = null;
	public Texture2D texture = null;

	public object Clone()
	{
		EEFrame r = new EEFrame ();
		r.name = name;
		r.hitboxes = hitboxes.ToList ();
		r.sprite = sprite;
		r.texture = texture;
		return r;
	}
}
