﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using FGE;

public class EETimeline
{


	List<List<IndexedFrameParameters> > __rename_me_frameparameters(List<IndexedFrameParameters> parameters)
	{
		List<List<IndexedFrameParameters> > split = new List<List<IndexedFrameParameters>> ();
		foreach (var e in parameters) {
			bool needNew = true;
			for (int i = 0; i < split.Count; i++) {
				//number of intersecting ranges is 0 means we are ok!
				if (split [i].Where (x => !(x.startFrame > (e.startFrame + e.count) || (x.startFrame + x.count) < e.startFrame)).Count () == 0) {
					needNew = false;
					continue;
				}
			}

			if (needNew) {
				split.Add (new List<IndexedFrameParameters> ());
			}

			for (int i = 0; i < split.Count; i++) {
				if (split [i].Where (x => !(x.startFrame > (e.startFrame + e.count) || (x.startFrame + x.count) < e.startFrame)).Count () == 0) {
					split [i].Add (e);
				}
			}
		}

		return split;
	}
}

public class FrameParameterTimelineGroup
{
	
}