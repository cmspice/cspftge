﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class EEClipboard
{
	//things that can be copied
	//Entity (only one at a time)
	//FrameSequences
	//Frames
	//hitboxes 
	//IndexedFrameProperties

	//stuff that can't be copied
	//text entry boxes (these use normal clipboard)



	//-----------------------

	//TODO make sure this is how you want to do it...
	List<EEEntity> entities;
	List<EEFrameSequence> frameSequences;
	List<EEFrame> frames;
	List<FGE.Hitbox> hitboxes;

	//TODO when pasting, make sure to update names
}