﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class EEHistory
{
	public Stack<EEOperation> history = new Stack<EEOperation>();
	public Stack<EEOperation> redo = new Stack<EEOperation>();

	public void Apply(EEOperation operation, EEDataTotal data)
	{
		operation.Do (data);
		history.Push (operation);
		redo.Clear ();
		if (!operation.CanUndo)
			history.Clear ();
	}

	public void Undo(EEDataTotal data)
	{
		var op = history.Pop ();
		op.Undo (data);
		redo.Push (op);
	}

	public void Redo(EEDataTotal data)
	{
		if (redo.Count == 0)
			return;
		var op = redo.Pop ();
		op.Do (data);
		history.Push (op);
		if (!op.CanUndo) //this will actually never happen 
			history.Clear ();
	}
}

public interface EEOperation
{
	void Do (EEDataTotal data);
	void Undo(EEDataTotal data);
	bool CanUndo {get;}
}

public class EEMultiOperation : EEOperation
{
	List<EEOperation> operations = new List<EEOperation>();

	public EEMultiOperation(List<EEOperation> _operations)
	{
		operations = _operations;
	}
	public void Do(EEDataTotal data)
	{
		for (int i = 0; i < operations.Count; i++)
			operations [i].Do (data);
	}
	public void Undo(EEDataTotal data)
	{
		for (int i = operations.Count-1; i >= 0; i--)
			operations [i].Undo(data);
	}
	public bool CanUndo { get { return operations.Where (e => e.CanUndo == false).Count() == 0; } }
}





