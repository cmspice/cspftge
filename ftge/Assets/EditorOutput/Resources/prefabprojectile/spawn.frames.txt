[
  {
    "name": "Frame 0",
    "hitboxGroup": {
      "hitboxes": [
        {
          "name": "hitbox 0",
          "mask": 16,
          "box": {
            "corner": {
              "x": -20.0,
              "y": -20.0
            },
            "size": {
              "x": 40.0,
              "y": 40.0
            }
          }
        }
      ]
    },
    "prefab": {
      "prefabName": "sparklypulsatingsphere",
      "oneShot": false,
      "scriptName": "PulsatingColorBehaviour"
    }
  }
]