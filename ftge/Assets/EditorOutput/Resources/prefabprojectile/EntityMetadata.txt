{
  "name": "prefabprojectile",
  "startingSequence": {
    "sequence": "spawn",
    "frame": 0
  },
  "totalHealth": 0.0,
  "entityType": null,
  "tags": null,
  "gravity": null,
  "airFriction": null,
  "groundFriction": null,
  "_OnFrame": "<projectile:default>",
  "_OnInput": null,
  "_OnHitEntity": "<projectile:default>",
  "_OnHitEnvironment": "<projectile:default>",
  "_OnFrameSequenceEnd": null,
  "_OnFrameSequenceBegin": null,
  "_OnCreate": null,
  "_OnDestroy": null
}