{
  "name": "crouching",
  "defaultTransition": "crouching",
  "priority": 0,
  "inputComboString": "",
  "tags": [
    "GROUND"
  ],
  "ignorePause": false,
  "serializedFrameParameters": [
    {
      "startFrame": 0,
      "count": 1,
      "parameters": {
        "cancels": [
          {
            "cancel": "crouch_to_stand",
            "overrideComboString": "[held ~d]"
          },
          {
            "cancel": "NORMAL,SPECIAL"
          }
        ],
        "allowFaceChange": true
      }
    }
  ]
}