{
  "name": "normal_punch",
  "defaultTransition": "standing",
  "priority": 0,
  "inputComboString": "[1]A",
  "tags": [
    "NORMAL",
    "GROUND"
  ],
  "ignorePause": false,
  "serializedFrameParameters": [
    {
      "startFrame": 0,
      "count": 3,
      "parameters": {
        "id": 1,
        "damage": 100,
        "hitType": "normal",
        "hitStun": 10,
        "blockStun": 10,
        "hitPause": 8,
        "knockback": {
          "x": -8.0,
          "y": 0.0
        }
      }
    }
  ]
}