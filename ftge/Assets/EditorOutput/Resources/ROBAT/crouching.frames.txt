[
  {
    "name": "Frame 0",
    "hitboxGroup": {
      "hitboxes": [
        {
          "name": "body",
          "mask": 38,
          "box": {
            "corner": {
              "x": 50.0,
              "y": 30.0
            },
            "size": {
              "x": 125.0,
              "y": 170.0
            }
          }
        }
      ]
    },
    "sprite": {
      "texture": "Assets/EntityAssets/ROBAT/adultrobot-02.png",
      "offset": {
        "x": 0.0,
        "y": 0.0
      },
      "crop": {
        "corner": {
          "x": 1750.0,
          "y": 0.0
        },
        "size": {
          "x": 250.071426,
          "y": 288.0
        }
      }
    }
  }
]