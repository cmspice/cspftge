{
  "name": "stand_hit",
  "defaultTransition": "standing",
  "priority": 0,
  "tags": [
    "HIT",
    "GROUND"
  ],
  "ignorePause": false,
  "serializedFrameParameters": [
    {
      "startFrame": 1,
      "count": 1,
      "parameters": {
        "duration": 3
      }
    },
    {
      "startFrame": 0,
      "count": 1,
      "parameters": {
        "duration": 1
      }
    }
  ]
}