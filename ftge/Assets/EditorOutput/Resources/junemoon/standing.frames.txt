[
  {
    "name": "Frame 0",
    "hitboxGroup": {
      "hitboxes": [
        {
          "name": "hitbox 0",
          "mask": 38,
          "box": {
            "corner": {
              "x": 0.0,
              "y": 0.0
            },
            "size": {
              "x": 100.0,
              "y": 134.0
            }
          }
        }
      ]
    },
    "sprite": {
      "texture": "Assets/EntityAssets/JuneMoon/junemoon.png",
      "offset": {
        "x": 0.0,
        "y": 0.0
      },
      "crop": {
        "corner": {
          "x": 0.0,
          "y": 1800.0
        },
        "size": {
          "x": 100.0,
          "y": 134.0
        }
      },
      "shader": "SpriteShaderMagentaKey"
    }
  },
  {
    "name": "Frame 1",
    "hitboxGroup": {
      "hitboxes": [
        {
          "name": "hitbox 0",
          "mask": 38,
          "box": {
            "corner": {
              "x": 0.0,
              "y": 0.0
            },
            "size": {
              "x": 100.0,
              "y": 134.0
            }
          }
        }
      ]
    },
    "sprite": {
      "texture": "Assets/EntityAssets/JuneMoon/junemoon.png",
      "offset": {
        "x": 0.0,
        "y": 0.0
      },
      "crop": {
        "corner": {
          "x": 100.0,
          "y": 1800.0
        },
        "size": {
          "x": 100.0,
          "y": 134.0
        }
      },
      "shader": "SpriteShaderMagentaKey"
    }
  },
  {
    "name": "Frame 2",
    "hitboxGroup": {
      "hitboxes": [
        {
          "name": "hitbox 0",
          "mask": 38,
          "box": {
            "corner": {
              "x": 0.0,
              "y": 0.0
            },
            "size": {
              "x": 100.0,
              "y": 134.0
            }
          }
        }
      ]
    },
    "sprite": {
      "texture": "Assets/EntityAssets/JuneMoon/junemoon.png",
      "offset": {
        "x": 0.0,
        "y": 0.0
      },
      "crop": {
        "corner": {
          "x": 200.0,
          "y": 1800.0
        },
        "size": {
          "x": 100.0,
          "y": 134.0
        }
      },
      "shader": "SpriteShaderMagentaKey"
    }
  },
  {
    "name": "Frame 3",
    "hitboxGroup": {
      "hitboxes": [
        {
          "name": "hitbox 0",
          "mask": 38,
          "box": {
            "corner": {
              "x": 0.0,
              "y": 0.0
            },
            "size": {
              "x": 100.0,
              "y": 134.0
            }
          }
        }
      ]
    },
    "sprite": {
      "texture": "Assets/EntityAssets/JuneMoon/junemoon.png",
      "offset": {
        "x": 0.0,
        "y": 0.0
      },
      "crop": {
        "corner": {
          "x": 300.0,
          "y": 1800.0
        },
        "size": {
          "x": 100.0,
          "y": 134.0
        }
      },
      "shader": "SpriteShaderMagentaKey"
    }
  }
]