﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Linq;
namespace FGE
{

	//this represents one group of assets which could be a character or stage, and is stored as a scene or asset bundle and eventually as data files in the Resources directory
	//TODO finish asset bundle loading
	//TODO should split this up into multiple derived classes, one for each asset source
	public class AssetGroup
	{
		WWW assetBundleRequest = null;
		AssetBundle Bundle { get { return assetBundleRequest.assetBundle; } }
		public string FullFilePath {get; private set;}
		public string BundleName{ get { return System.IO.Path.GetFileName(FullFilePath); } }
		public string LocalFilePath {get; private set;}

		public bool IsDone { 
			get {
				if (assetBundleRequest == null)
					return false;
				return assetBundleRequest.isDone;
			} 
		}

		public Texture2D GetTexture(string name){
			return Bundle.LoadAsset<Texture2D> (name);
		}
		public GameObject GetPrefab(string name){
			return Bundle.LoadAsset<GameObject> (name);
		}
			
		public T Get<T>(string name) where T : UnityEngine.Object{
			return Bundle.LoadAsset<T> (name);
		}

		//TODO character bundle helper functions for getting character bundle meta data
		//for now you can just use filenames w/e...

		public TextAsset GetEntityMetadata(){
			return Bundle.LoadAsset<TextAsset> ("EntityMetadata");
		}

		public GameObject[] GetAllPrefabs(){
			return Bundle.LoadAllAssets<GameObject> ();
		}

		public SequenceFramesResourcePair[] GetAllSequences(){
			List<SequenceFramesResourcePair> r = new List<SequenceFramesResourcePair> ();
			var assets = Bundle.LoadAllAssets<TextAsset> ();
			foreach (var e in assets) {
				//Debug.Log ("found text asset " + e.name);
				if (e.name.EndsWith (".sequence")) {
					string frameFilename = e.name.Substring (0, e.name.Length - ".sequence".Length) + ".frames";
					TextAsset frame = assets.FirstOrDefault (f => f.name == frameFilename);
					r.Add (new SequenceFramesResourcePair (e, frame));
				}
			}

			//now override based on files in EditorOutput
			foreach (var e in r) {
				string folder = BundleName.Substring (0, BundleName.Length - 7) + "/";
				//Debug.Log (folder + e.sequence.name + ".txt");
				var seq = Resources.Load<TextAsset> (folder + e.sequence.name);
				var frame = Resources.Load<TextAsset> (folder + e.frames.name);
				if (seq != null && frame != null) {
					//Debug.Log ("overriding for " + seq.name);
					e.sequence = seq;
					e.frames = frame;
				}
			}
			return r.ToArray ();
		}

		public void LoadFromLocalAssetBundle(string bundle)
		{
			LocalFilePath = bundle;
			string filename = Application.dataPath + "/" + bundle;
			if (!System.IO.File.Exists (filename))
				throw new UnityException ("bundle at " + filename + " does not exist");
			filename = "file://" + filename;
			LoadFromAssetBundle (filename);
		}
		public void LoadFromAssetBundle(string URL)
		{
			FullFilePath = URL;
			//TODO switch to 
			//AssetBundle.LoadFromFileAsync()
			assetBundleRequest = new WWW (URL);

		}

		public void Destroy()
		{
			if (Bundle != null) {
				Bundle.Unload (true);
			}
			assetBundleRequest = null;
		}
	}

	public class SequenceFramesResourcePair
	{
		public TextAsset sequence;
		public TextAsset frames;
		public SequenceFramesResourcePair(TextAsset _sequence, TextAsset _frames)
		{
			sequence = _sequence;
			frames = _frames;	
		}
	}
}