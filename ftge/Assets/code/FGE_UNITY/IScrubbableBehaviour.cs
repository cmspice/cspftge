﻿using UnityEngine;
using System.Collections;

using FGE;
public class IScrubbableBehaviour : MonoBehaviour {

	public virtual void Scrub(PlayState state, Entity entity, int frameSinceStart){
		
	}
}
