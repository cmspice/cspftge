﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
namespace FGE
{
	public class ResourceManager
	{
		Dictionary<string,AssetGroup> assets = new Dictionary<string, AssetGroup>();
		Dictionary<string,UnityEngine.Object> looseAssets = new Dictionary<string, UnityEngine.Object>();

		public bool IsLoaded{ get { return assets.Where (e => !e.Value.IsDone).Count () == 0; } }

		public AssetGroup GetAssetGroup(string name){
			return assets [name];
		}

		public void RemoveAssetGroup(string name){
			assets [name].Destroy ();
			assets.Remove (name);
		}

		public AssetGroup LoadAsset(string filename)
		{
			//check if asset is already loaded
			var r = assets.Select(e=>e.Value).FirstOrDefault(e=>e.LocalFilePath == filename);
			if (r != null){
				return r;
			}

			var a = new AssetGroup();
			a.LoadFromLocalAssetBundle (filename);
			if (assets.ContainsKey (a.FullFilePath))
				throw new System.Exception ("Asset already loaded " + a.FullFilePath);
			assets [a.FullFilePath] = a;
			return a;
		}

		public void LoadResourceFolderAsset(string filename){
			UnityEngine.Object resource = Resources.Load (filename);
			if (resource == null)
				throw new Exception ("Resource " + filename + " does not exist");
			if(looseAssets.ContainsKey(resource.name))
				throw new Exception("Asset with name " + filename + " already exists");
			looseAssets [resource.name] = resource;
		}

		public void LoadResourceFolderAssetByFolder(string folder){
			foreach (var e in Resources.LoadAll (folder)) {
				if (!looseAssets.ContainsKey (e.name)) {
					looseAssets [e.name] = e;
				} else {
					//TODO warning
				}
			}
		}

	}
}
