﻿using UnityEngine;
using FGE;

public static class FGEUnityExtensionMethods 
{
	public static Rectangle ToFGERectangle(this UnityEngine.Rect rect)
	{
		Rectangle r = new Rectangle();
		r.corner = new Point(rect.x,rect.y);
		r.size = new Point (rect.width, rect.height);
		return r;
	}
	public static UnityEngine.Rect ToUnityRect(this Rectangle rect)
	{
		UnityEngine.Rect r = new UnityEngine.Rect ();
		r.x = rect.corner.x;
		r.y = rect.corner.y;
		r.width = rect.size.x;
		r.height = rect.size.y;
		return r;
	}

	public static UnityEngine.Vector2 ToUnityVector2(this Point pt)
	{
		return new Vector2 (pt.x, pt.y);
	}
	public static UnityEngine.Vector3 ToUnityVector3(this Point pt, float z = 0)
	{
		return new Vector3 (pt.x, pt.y,z);
	}

}
