﻿using System.Collections.Generic;
using System;
using System.Linq;

using System.IO;
using UnityEngine;
using Newtonsoft.Json;

namespace FGE
{

	public struct CharacterResourceInfo
	{
		public string name;
		public string bundleName;
		public string previewBundleName;
		public string previewBundleSequence;
		public string type; //"character"
		public string dependencies;

		public static CharacterResourceInfo[] LoadInfo(string path)
		{
			StreamReader file = new StreamReader (Application.streamingAssetsPath + "/" + path);
			string info = file.ReadToEnd ();
			var r = JsonConvert.DeserializeObject<CharacterResourceInfo[]> (info);
			return r;
		}

		public static CharacterResourceInfo[] GenerateFromFolders(string rootPath)
		{
			
			List<CharacterResourceInfo> r = new List<CharacterResourceInfo>();

			//TODO go through each folder and try and pull out matching filenames

			return r.ToArray ();
		}
			
	}
}