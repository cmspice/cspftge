﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using CSP;

namespace FGE
{
	public class RenderObject{
		public string name;
		public GameObject unityGameObject;
		public ImageGameObjectUtility imageUtility;
		public float lastUsed = -1;
		public void Destroy()
		{
			GameObject.Destroy (unityGameObject);
		}

		public void SetActive(bool active)
		{
			unityGameObject.SetActive (active);
		}
	}

	public class EntityRenderManager{
		//stored from oldest to newest
		Dictionary<string,RenderObject> renderObjectCache = new Dictionary<string, RenderObject>();
		Stack<RenderObject> hitboxCache = new Stack<RenderObject>();
		List<RenderObject> rendered = new List<RenderObject>();
		ResourceManager resourceManager;
		Shader defaultShader;
		public Shader debugShader;

		//TODO allow this to be set or change dynamically based on viewport size
		float pixelsperunit = 100;

		public EntityRenderManager(Shader _defaultMaterial, ResourceManager _resourceManager){
			defaultShader = _defaultMaterial;
			debugShader = defaultShader;
			resourceManager = _resourceManager;
		}

		void Prune(int maxCount)
		{
			//TODO take into account last time used 
			renderObjectCache = renderObjectCache.Take(maxCount).ToDictionary(entry => entry.Key, 
					entry => entry.Value);
		}

		//call for clean unloading
		void Clean()
		{
			renderObjectCache.Clear ();
		}

		RenderObject Claim(string name)
		{
			RenderObject r = null; 
			if (name == "__debug_box__") {
				if (hitboxCache.Count > 0) {
					r = hitboxCache.Pop ();
				} else {
					r = new RenderObject ();
					r.name = "__debug_box__";
					//TODO need to use different material
					r.imageUtility = new ImageGameObjectUtility (null, pixelsperunit, debugShader);
					r.unityGameObject = r.imageUtility.ParentObject;
				}
			} else if(renderObjectCache.TryGetValue(name,out r)) {
				renderObjectCache.Remove (name);
				r.SetActive (true);
			}
			return r;
		}

		void ReturnOrDestroy(RenderObject ro)
		{
			ro.SetActive (false);
			if (ro.name == "__debug_box__") {
				hitboxCache.Push (ro);
			} else {
				if (renderObjectCache.ContainsKey (ro.name))
					ro.Destroy ();
				else
					renderObjectCache [ro.name] = ro;
			}
		}

		public void Render(IEnumerable<Entity> entities){
			foreach (var e in rendered)
				ReturnOrDestroy (e);
			rendered.Clear ();

			float time = Time.time;

			//TODO rendering for non FrameSequence Entities
			foreach (var e in entities.OrderBy(e=>e.Depth)) {
				var sequence = e.GetCurrentSequenceReadOnly ();
				var frame = e.GetCurrentFrameReadonly ();
				var name = e.Name + "/" + sequence.name + "/" + frame.name;
				//Debug.Log (frame.GetFrameType () + " " + name);

				if (frame.GetFrameType () == FrameType.SPRITE) {
					RenderObject ro = Claim (name);
					if (ro == null) {
						ro = new RenderObject ();
						ro.name = name;
						var assets = resourceManager.GetAssetGroup (e.EntityMetadata.assetPath);
						var texture = assets.GetTexture (frame.sprite.Value.texture);
						var shader = frame.sprite.Value.shader == null ? defaultShader : assets.Get<Shader> (frame.sprite.Value.shader);
						ImageGameObjectUtility util = new ImageGameObjectUtility (texture, pixelsperunit, shader);
						util.PlaneMaterial.renderQueue = e.Depth;
						util.Crop = frame.sprite.Value.crop.ToUnityRect ();
						util.PixelDimension = util.Crop.size;
						util.ParentObject.name = ro.name;
						ro.imageUtility = util;
						ro.unityGameObject = util.ParentObject;

						//TODO crop the image to just the crop area
						//shoulsd be part of ImageGameObjectUtility funcitonality
					}

					//TODO scale rotation
					Point positionFromCenter;
					positionFromCenter = frame.sprite.Value.crop.size * 0.5f - frame.sprite.Value.offset;
					if (e.Transform.backwards)
						positionFromCenter.x = -positionFromCenter.x;
					ro.unityGameObject.transform.position = (e.Transform.pos + positionFromCenter).ToUnityVector3 () / pixelsperunit;
					ro.unityGameObject.transform.localScale = e.Transform.backwards ? new Vector3 (-1, 1, 1) : new Vector3 (1, 1, 1);

					ro.lastUsed = time;
					rendered.Add (ro);
				
				} else if (frame.GetFrameType () == FrameType.PREFAB || frame.GetFrameType () == FrameType.PREFAB_ONESHOT) {
					RenderObject ro = Claim (name);
					if (ro == null || frame.GetFrameType () == FrameType.PREFAB_ONESHOT) {
						ro = new RenderObject ();
						ro.name = name;
						var prefab = resourceManager.GetAssetGroup (e.EntityMetadata.assetPath).GetPrefab (frame.prefab.Value.prefabName);
						var inst = GameObject.Instantiate<GameObject> (prefab);
						if (frame.prefab.Value.scriptName != null)
							inst.AddComponent (System.Type.GetType (frame.prefab.Value.scriptName));
						inst.name = name;
						ro.unityGameObject = inst;
					}

					//TODO scale rotation
					ro.unityGameObject.transform.position = e.Transform.pos.ToUnityVector3 ()/pixelsperunit;

					ro.lastUsed = time;
					if (frame.GetFrameType () == FrameType.PREFAB) {
						//ro.unityGameObject.GetComponent<IScrubbableBehaviour>().Scrub
						rendered.Add (ro);
					}
				} 
			}
			if (FGEConstants.DEBUG_DRAW_HITBOXES) {
				foreach (var e in entities) {
					//TODO copy transformations from above
					//or just ignore this because no one cares..
					//RenderObject spriteBoundary = Claim ("__debug_box__");
					//spriteBoundary.SetActive (true);
					//rendered.Add (spriteBoundary);

					RenderObject center = Claim ("__debug_box__");
					center.SetActive (true);
					center.unityGameObject.transform.position = e.Transform.pos.ToUnityVector3 () / pixelsperunit;
					center.imageUtility.PixelDimension = new Vector2 (10, 10);
					center.imageUtility.PlaneMaterial.SetColor ("_TintColor", new Color32 (0, 0, 255, 255));
					rendered.Add (center);

					foreach (var f in e.Hitboxes.hitboxes) {
						RenderObject ro = Claim ("__debug_box__");
						ro.SetActive (true);
						ro.imageUtility.PixelDimension = f.box.size.ToUnityVector2 ();
						//TODO colors based on hitbox mask
						ro.imageUtility.PlaneMaterial.SetColor ("_TintColor", new Color32 (255, 100, 100, 30));
						ro.unityGameObject.transform.position = f.box.Center.ToUnityVector3 () / pixelsperunit;

						rendered.Add (ro);
					}
				}
			}
		}

	}
}