﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

namespace CSP{

	public enum NetworkPlayerType
	{
		NONE,
		PLAYER,
		SPECTATOR
	}

	public class NetworkPlayer
	{
		public Guid Id { get; private set; }
		public NetworkPlayerType Type {get; private set;} //spectator or player
		public NetworkInputSource InputSource {get; private set;}

		public NetworkPlayer(Guid _id, NetworkPlayerType _type)
		{
			Id = _id;
			Type = _type;
			if (_type == NetworkPlayerType.PLAYER) {
				InputSource = new NetworkInputSource (_id);
			}
		}

		//confirmed here means networkplayer has sent confirmation that a frame is received
		//all elements older than the first element of this list are confirmed
		List<int> confirmedFrames = new List<int>(){-1};

		//returns all unconfirmed frames up to final inclusive
		public List<int> UnconfirmedFrames(int final){
			int firstFrame = confirmedFrames.First ();
			List<int> r = new List<int> ();
			int c = 0;
			for (int i = firstFrame; i < final; i++) {
				if (confirmedFrames.Count >= c || confirmedFrames [c] != i) {
					if(i != -1)
						r.Add (i);
					continue;
				}
				c++;

			}
			return r;
		}

		public int OldestUnconfirmedFrame(){
			int firstFrame = confirmedFrames.First ();
			int c = 0;
			for (int i = firstFrame; c != confirmedFrames.Count; i++) {
				if (confirmedFrames [c] == i) {
					c++;
					continue;
				}
				return i;
			}
			return confirmedFrames [c-1] + 1;
		}

		public bool IsFrameConfirmed(int frame){
			if (frame < confirmedFrames.First ()) {
				return true;
			}
			return confirmedFrames.Contains (frame);
		}
			
		public void AddConfirmedFrames(int frame)
		{ 
			confirmedFrames.AddSorted (frame);
			PruneConfirmedFrames ();
		}

		void PruneConfirmedFrames()
		{
			while (confirmedFrames.Count > 1 && 
				(confirmedFrames [0] == confirmedFrames [1] || confirmedFrames[0] + 1 == confirmedFrames[1]))
			{
				confirmedFrames.RemoveAt (0);
			}	
		}


	}

	//TODO do we just initialize with reference to network manager?
	public class NetworkPlayerManager
	{
		NetworkManager network;
		public NetworkPlayerManager(NetworkManager _nm)
		{
			network = _nm;
			network = _nm;
			if (network != null) {
				network.OnReceiveId += ReceiveId;
				network.OnReceiveConfirmation += ReceiveConfirmation;
				network.OnReceiveData += delegate(Guid id, byte[] data) {
					var inputs = InputGroup.Deserialize (data);
					ReceiveInputs (id, inputs);
				};
			}
		}

		List<NetworkPlayer> players = new List<NetworkPlayer>();
		NetworkPlayer GetPlayer(Guid playerId){
			return players.Find (e => e.Id == playerId);
		}
		NetworkPlayer GetPlayerWithInputSource(){
			return players.Find (e => e.InputSource != null);
		}

		public NetworkInputSource AddNetworkPlayer(Guid _id, NetworkPlayerType _type)
		{
			NetworkPlayer p;
			p = GetPlayer (_id); //this will be not null in reconnect case
			if (p == null) {
				
				if (_type == NetworkPlayerType.PLAYER && GetPlayerWithInputSource () != null) {
					//throw new Exception ("We already have a player!!");
					//return null;
				}

				p = new NetworkPlayer (_id, _type);
				players.Add (p);
			}
			return p.InputSource;
		}

		public void RemoveNetworkPlayer(Guid _id)
		{
			players.RemoveAll (e => e.Id == _id);
		}

		public NetworkPlayer Opponent {
			get{
				foreach (var e in players) {
					if (e.Type == NetworkPlayerType.PLAYER) {
						return e;
					}
				}
				return null;
			}
		}

		public void SynchronizeOpponent()
		{
			//TODO
			//start
			//receive confirmation
			//send ack
			//figure out time
			//triggre callback
		}

		//TODO check for disconnect routine (or do we just do this in GameRunner???

		//TODO some way to reset opponent without reconnecting




		//TODO pass in InputHistory as well, so we can send to spectators
		public void SendInputs(InputSource source, InputHistory all)
		{
			foreach (var e in players) {
				if (e.Type == NetworkPlayerType.SPECTATOR) {
					foreach (var f in e.UnconfirmedFrames(source.LastProcessedFrame)) {
						var send = all.GetInputsOnFrame (f);
						//TODO CAN DELETE error checking
						if (send == null)
							throw new System.Exception ("inputs sare null, this should not happen");
						network.Send (e.Id, InputGroup.Serialize (send.Value), NetworkMessageType.INPUT);
					}
				} else if (source != null && e.Type == NetworkPlayerType.PLAYER) {
					//DebugConsole.Log ("actually sending inputs " + source.LastProcessedFrame + " " + e.UnconfirmedFrames(source.LastProcessedFrame).Count());
					foreach(var f in e.UnconfirmedFrames(source.LastProcessedFrame)){
						var send = source.GetInputsOnFrame (f); //this should never be null;
						//TODO CAN DELETE error checking
						if (send == null)
							throw new System.Exception ("inputs sare null, this should not happen");	
						
						//if (send.Value.inputs.Length > 0)
							//DebugConsole.Log ("sending inputs " + send.Value.frame + " : " + send.Value.inputs.Length);

						network.Send (e.Id, InputGroup.Serialize (send.Value), NetworkMessageType.INPUT);
					}
				}
			}
		}

		//callback from NetworkManager
		public void ReceiveConfirmation(Guid playerId, int frame)
		{
			var player = players.Find (e => e.Id == playerId);
			player.AddConfirmedFrames (frame);
		}

		//callback from NetworkManager
		//frame information is redundant as all inputs should have the same frame timestamp
		public void ReceiveInputs(Guid playerId, InputGroup inputs)
		{
			var player = players.Find (e => e.Id == playerId);

			//TODO fail gracefully here, this means a spectator is trying to send us input
			//or it could also mean a disconnected player is trying to send us inputs
			if (player != Opponent)
				throw new System.Exception ("Receiving inputs from non-opponent player");

			network.Send (player.Id,System.BitConverter.GetBytes(inputs.frame), NetworkMessageType.CONFIRMATION);

			//because confirmation takes time, we can receive the same set of frames twice
			if(player.InputSource.HasReceivedInput(inputs.frame))
				return;

			//if(inputs.inputs.Length > 0)
				//DebugConsole.Log ("adding inputs for " + inputs.frame + " : " + inputs.inputs.Length);
			
			player.InputSource.AddInput(inputs);
		}



		public void ReceiveId(int connectionId, Guid playerId)
		{
			
		}
	}
}