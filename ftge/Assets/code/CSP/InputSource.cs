﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace CSP{
	//maybe we don't want a class here and instead have our input sources directly send input to MultisourceGameController
	public abstract class InputSource {

		//this is which frame we are currently on, effectively how many times we've called UpdateAndReturnNewInputs-1
		int lastProcessedFrame = -1;
		public int LastProcessedFrame { get{ return lastProcessedFrame; } private set{ lastProcessedFrame = value; } }

		//this is the last internal frame represented (i.e. we may not have all inputs up until present from the network)
		int lastInternalFrame = -1;
		public int LastInternalFrame{ get{ return lastInternalFrame; } private set{ lastInternalFrame = value; } }

		//all frames up until and including LastReturnedFrame are properly represented
		public int LastReturnedFrame{ get { return Math.Min (LastProcessedFrame, LastInternalFrame); } }


		InputHistory history = new InputHistory();

		//return null if no inputs exist
		public InputGroup? GetInputsOnFrame(int frame)
		{
			return history.GetInputsOnFrame (frame);
		}

		//NOTE, this may return inputs that are older than frame, in which means rollback time!
		//this will never return inputs older than an input that it has already returned
		//TODO should probbaly rewrite this to return InputGroup but it's fine because this is protected
		protected abstract InputHistory GetNewInputs ();

		public InputHistory UpdateAndReturnInputs()
		{
			LastProcessedFrame++;
			var r = GetNewInputs ();
			if (r.LastFrame != -1) {
				history.CombineHistoryOverride (r);
				LastInternalFrame = r.LastFrame;
			}
			return r;
		}
	}

	//this class is Unity dependent
	public class LocalInputSource : InputSource
	{
		//TODO this should keep a history of inputs for each frame going into the future so we can queue up multiple frames in case we want to do input delay for handling lag
		List<Input> queuedInputs = new List<Input>();
		public void AddInput(Input input) //real time info instead of just calling in order???
		{
			queuedInputs.Add (input);
		}

		protected override InputHistory GetNewInputs ()
		{
			
			var inputs = queuedInputs.Select (e => new TimedInput (e.code, new Timestamp(LastInternalFrame + 1)));
			var r = new InputHistory ();
			var ig = new InputGroup (LastInternalFrame + 1, inputs.ToArray ());
			//UnityEngine.Debug.Log ("inputs " + inputs.Count() + " after " + ig.inputs.Length);
			r.SetInputs (ig);
			queuedInputs.Clear ();
			return r;
		}
	}

	public class AIInputSource : InputSource
	{
		protected override InputHistory GetNewInputs ()
		{
			//TODO
			return new InputHistory ();
		}
	}
		
	//represents inputs from a single frame
	[Serializable]
	public class NetworkInputPacket
	{
		//by conventien, negative values for number inputs mean appending onto previous packet, where the value is used for ordering
		//but I'm going to assume everything can be sent in a single packet :).
		int numberInputs; 
		int frame;
		List<TimedInput> inputs;
	}

	public class NetworkInputSource : InputSource
	{
		//this can have more than one entry if networked player is several frames into the future
		//or UDP packets arrive out of order
		//TODO switch this to use InputHistory, since it does everything you are trying to do here.
		SortedDictionary<int,InputGroup > networkInputBuffer = new SortedDictionary<int, InputGroup>();
		HashSet<int> receivedFrames = new HashSet<int>();

		public Guid Id{ get; private set;}
		public NetworkInputSource(Guid _id){
			Id = _id;
		}

		//this will return null if inputs are not ready yet
		protected override InputHistory GetNewInputs ()
		{
			InputHistory r = new InputHistory ();

			if (networkInputBuffer.Count == 0)
				return r;

			//first frame that has not been returned
			int tempInternalFrame = LastInternalFrame;

			var first = networkInputBuffer.First();
			if (LastInternalFrame + 1 != first.Key) { //this means our inputs are too far in the future
				DebugConsole.Log("FUTURE INPUTS " + LastInternalFrame + " " + first.Key);
				return r;
			}
				
			while (tempInternalFrame + 1 == first.Key && LastInternalFrame <= LastProcessedFrame) {
				tempInternalFrame++;
				r.SetInputs (first.Value);
				networkInputBuffer.Remove (first.Key);
				first = networkInputBuffer.FirstOrDefault ();
			}

			return r;
		}

		public void AddInput(InputGroup inputs)
		{
			networkInputBuffer [inputs.frame] = new InputGroup (inputs);
			receivedFrames.Add (inputs.frame);
		}

		public bool HasReceivedInput(int _frame)
		{
			return receivedFrames.Contains (_frame);
		}
	}

	public class RecordedInputSource : InputSource
	{
		protected override InputHistory GetNewInputs ()
		{
			return new InputHistory ();
			//TODO
		}
	}
}