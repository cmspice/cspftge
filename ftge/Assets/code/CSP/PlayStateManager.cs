﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


namespace CSP{
	public interface IPlayState{
		IPlayState Copy(); 
		void Update (InputHistory inputs); //advance playstate given input since last frame
		float CurrentStateImportance (); //how important the current frame is. 0 - unimportant, 1 - SUPER IMPORTANT

		int CurrentFrame{ get; }
		int StartFrame{ get; }
	}

	public class PlayStateManager{
		//Is there a way to pass a sort of abstract optimization object so that gets passed into PlayState in the Update routine?
		//TODO at the very least, this can try and keep track of input changes potentially allowing to skip UpdateCurrentState()
		//TODO or this can try and keep track game states at frames in between currentstate and lastconfirmedstate to reduce number of frames needed to be updated

		public static PlayStateManager CreatePlayStateManager(IPlayState initial)
		{
			PlayStateManager r = new PlayStateManager ();
			r.LastConfirmedFrame = 0;
			r.CurrentFrame = 0;
			r.LastConfirmedState = initial;
			r.CurrentState = initial.Copy ();
			return r;
		}

		//TODO add caching
		//honestly, this is pointless because we need to be able to function under worse case scenario :O
		//const int cacheInterval = 3;
		//List<IPlayState> cachedFrames; //first cached frame is last confirmed state

		/// <summary>
		/// game state stuff
		/// </summary>
		public IPlayState LastConfirmedState {get; private set;} //server side authoritative state
		public int LastConfirmedFrame {get; private set;}
		public IPlayState CurrentState {get; private set;} //client side prediction state, for rendering purposes only
		public int CurrentFrame {get; private set;}

		//TODO implement caching
		List<IPlayState> cachedStates = new List<IPlayState> ();
		int cacheInterval = -1;

		//List<Timestamp> frameTimestamps = new List<Timestamp>();
		public double UpdateFrame(IPlayState state, InputHistory inputs)
		{
			System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
			watch.Start ();
			state.Update (inputs);
			watch.Stop ();
			return watch.ElapsedTicks * System.Diagnostics.Stopwatch.Frequency; 
		}

		public double UpdateLastConfirmedState(int nextConfirmedFrame,InputHistory inputs)
		{
			double netTime = 0;
			while (LastConfirmedFrame < nextConfirmedFrame) {
			//if (LastConfirmedFrame != nextConfirmedFrame) {
				LastConfirmedFrame++;
				netTime += UpdateFrame (LastConfirmedState, inputs);
			}
			return netTime;
		}

		public void SetCurrentStateToLastConfirmedState()
		{
			CurrentState = LastConfirmedState.Copy ();
			CurrentFrame = LastConfirmedFrame;
		}

		public double UpdateCurrentState(int nextCurrentFrame, InputHistory inputs)
		{
			double netTime = 0;
			while (CurrentFrame < nextCurrentFrame) {
				CurrentFrame++;
				netTime += UpdateFrame (CurrentState, inputs);
			}
			return netTime;
		}
	}
}
