﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.Linq;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace CSP{
	public class ConnectionInfoData
	{
		IEnumerable<byte> tcpBuffer = new List<byte>();
		public int Length { get; private set; }
		public bool IsMessageBuffered { get { return tcpBuffer.Count () != 0; } }
		public Guid Guid { get; set; }
		public int ConnectionId{ get; private set; }

		public int syncStartTime = -1;
		public int syncTimeout = -1;
		public int syncWaitTime = -1;

		public ConnectionInfoData(int _connectionId)
		{
			ConnectionId = _connectionId;
			Length = -1;
		}

		//this stuff has never been tested lol
		//Turns out Unity LLAPI reliable is still built on UDP so this is basically pointless
		//I'm keeping this around in case we ever switch to true TCP
		//TODO test it
		public byte[] Store(byte[] buffer, int size, int length = -1)
		{
			if (!IsMessageBuffered)
				Length = length;
			tcpBuffer = tcpBuffer.Concat (buffer.Take(size));
			if(tcpBuffer.Count() == Length)
			{
				var r = tcpBuffer.ToArray ();
				Length = -1;
				tcpBuffer = new List<byte> ();
				return r;
			}
			return null;
		}
	}

	public enum NetworkMessageType : byte
	{
		CONFIRMATION,
		PING,
		IDENTIFY,
		SENDTEST,
		SYNCHRONIZE,
		SYNCACK,
		INPUT = 100 //100 and on are messages with no confirmation
	}
}