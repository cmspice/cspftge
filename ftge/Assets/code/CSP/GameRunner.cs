﻿using System.Collections.Generic;
using System;
using System.Linq;

namespace CSP{
	public class GameRunner
	{
		public NetworkGameController ngc;
		public PlayStateManager psm;
		public NetworkPlayerManager npm;



		//order matters!
		List<InputSource> sources = new List<InputSource>();
		public IEnumerable<InputSource> Sources { get{return sources;} }
		public bool PlayStateInitialialized { get; private set; }

		public string Frames { get { return psm.LastConfirmedFrame + " : " + psm.CurrentFrame; } }

		public T GetInputSource<T>() where T : InputSource
		{
			return (T)sources.FirstOrDefault (e => e is T);
		}

		public GameRunner(NetworkManager _nm)
		{
			ngc = new NetworkGameController ();
			npm = new NetworkPlayerManager (_nm);
		}

		//call this after connection + network player guid have been established
		public void AddNetworkPlayer(Guid _id, NetworkPlayerType _type)
		{
			var _source = npm.AddNetworkPlayer (_id, _type);
			sources.Add (_source);
		}

		public void AddLocalPlayer(InputSource _source)
		{
			sources.Add (_source);
		}

		public void SetPlayState(IPlayState _state)
		{
			psm = PlayStateManager.CreatePlayStateManager (_state);
			PlayStateInitialialized = true;
		}




		//this handles pausing (when one player is too far ahead in simulation)
		//and handles reconnect
		//TODO turn these into get private set properties
		public int lastDiff = 0;
		public int lastNumberUnconfirmedFrames = 0;
		public float timeUntilNextFrame = 0;
		public void Update(float deltaTime, float fps, float ping)
		{

			timeUntilNextFrame -= deltaTime;
			if (timeUntilNextFrame <= 0) {
				timeUntilNextFrame = timeUntilNextFrame + (1 / fps);
			
				//TODO do something if too many inputs are not confirmed (this means DC)
				if (npm.Opponent != null)
					lastNumberUnconfirmedFrames = npm.Opponent.UnconfirmedFrames (psm.CurrentFrame).Count;

				lastDiff = AdvanceOneFrame ();


				float timediff = timeUntilNextFrame+lastDiff * (1 / fps) - ping / 2f;
				if (timediff > (psm.CurrentState.CurrentStateImportance () + 1)*0.1f) {
					DebugConsole.Log ("SLOW");
					timeUntilNextFrame += timediff;
					return;
				}
			}
		}

		//TODO ideally this should fallback to input delay if there is too much time difference in input sources
		//so we don't simulate in advance too far
		//we queue up inputs in LocalInputSource so they don't all end up on the same frame if we are doing input delay
		//but maybe that's a bad idea because we have stuff like frame pauses and we want to recover to no input delay ideally.
		//we could in principle using real time to queue inputs in frame pauses properly though.
		//but that's super not-useful from a player's perspective

		public int AdvanceOneFrame()
		{
			int oldestFrame = int.MaxValue;
			int newestFrame = int.MinValue;

			string output = "";

			//process other input sources
			foreach (var e in sources) {
				var history = e.UpdateAndReturnInputs ();

				//if (history.inputs.Where (f => f.Value.inputs.Count () > 0).Count () > 0) 
					//DebugConsole.Log ("non zero inputs for " + e.GetType ().ToString () + " " + e.LastReturnedFrame + ":" + e.LastProcessedFrame);
				
				foreach (var f in history.inputs) {
					ngc.AddInputs (f.Value);
				}
				oldestFrame = Math.Min (oldestFrame, e.LastReturnedFrame);
				newestFrame = Math.Max (newestFrame, e.LastReturnedFrame);

				output += e.LastReturnedFrame + " ";
			}

			//send out new inputs immediately
			//this will also send out past inputs that have not been confirmed
			var localSource = sources.FirstOrDefault(e=>e.GetType() == typeof(LocalInputSource));
			npm.SendInputs (localSource,ngc.AllInputs);
				
				


			//TODO optimize
			//though, this is worst case scenario, and we want it to always work in this case!
			//or at the very least, it's good for testing or pause fallback XD

			//DebugConsole.Log (psm.CurrentFrame.ToString());

			psm.UpdateLastConfirmedState (oldestFrame, ngc.AllInputs);
			//DebugConsole.Log(psm.CurrentFrame + " " + psm.LastConfirmedFrame + " : " + oldestFrame + " " + newestFrame + " : " + output);
			psm.SetCurrentStateToLastConfirmedState ();
			//note, we never update to inputs "in the future" here as NetworkInputSource will hold onto future inputs until ready
			psm.UpdateCurrentState (newestFrame, ngc.AllInputs);

			

			return newestFrame-oldestFrame;
		}

		public void RollBack(){
			//TODO
		}
	}
}
