﻿using System.Collections.Generic;
using System.Linq;
using System;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

namespace CSP
{
	[Serializable]
	public struct Timestamp : IComparable<Timestamp>
	{

		public int frame; //better to use timestamp->frame map in PlayStateManager and delete this
		//public float timestamp; //should use NetworkTransport.GetNetworkTimestamp() for this, but keep it abstracted

		public Timestamp(int _frame){
			frame = _frame;
		}

		public int CompareTo(Timestamp other)
		{
			return frame.CompareTo (other.frame);
		}
		public static bool operator >  (Timestamp operand1, Timestamp operand2){
			return operand1.CompareTo(operand2) == 1;
		}
		public static bool operator <  (Timestamp operand1, Timestamp operand2){
			return operand1.CompareTo(operand2) == -1;
		}
		public static bool operator >=  (Timestamp operand1, Timestamp operand2){
			return operand1.CompareTo(operand2) >= 0;
		}
		public static bool operator <=  (Timestamp operand1, Timestamp operand2){
			return operand1.CompareTo(operand2) <= 0;
		}
	}


	[Serializable]
	public struct TimedInput
	{
		public Input input;
		public Timestamp timestamp;

		public TimedInput(InputCode _input, Timestamp _timestamp){
			input = new Input(_input);
			timestamp = _timestamp;
		}
	}

	//represents a bunch of inputs on the same frame
	[Serializable]
	public struct InputGroup
	{
		public TimedInput[] inputs;
		public int frame;
		public InputGroup(int _frame, TimedInput[] _inputs)
		{
			frame = _frame;
			inputs = _inputs;
		}
		public InputGroup(InputGroup ig)
		{
			frame = ig.frame;
			inputs = ig.inputs.ToArray ();
		}

		//TODO memory efficient serialization
		public static byte[] Serialize(InputGroup inputs)
		{
			using (var ms = new MemoryStream())
			{
				var formatter = new BinaryFormatter();
				formatter.Serialize(ms, inputs);
				return ms.ToArray ();
			}
		}

		public static InputGroup Deserialize(byte[] data)
		{
			using (var ms = new MemoryStream (data)) {
				var formatter = new BinaryFormatter ();
				return (InputGroup)formatter.Deserialize (ms);
			}
		}
	}

	public class InputHistory
	{
		public Dictionary<int,InputGroup > inputs = new Dictionary<int, InputGroup> ();
		public int LastFrame {  get; private set;}

		public InputHistory()
		{
			LastFrame = -1;
		}




		public TimedInput[] Cropped(int start, int count)
		{
			IEnumerable<TimedInput> r = new List<TimedInput> ();
			for(int i = start; i < start + count; i++){
				InputGroup ig;
				if (inputs.TryGetValue (i, out ig)) {
					r = r.Concat (ig.inputs);
				}
			}
			return r.ToArray ();
		}

		public bool HasInputsOnFrame(int frame)
		{
			return inputs.ContainsKey (frame);
		}
		public void SetInputs(InputGroup _inputs){
			_inputs = new InputGroup (_inputs); //make a copy, I don't really need to do this tbh.
			inputs [_inputs.frame] = _inputs;
			LastFrame = Math.Max (LastFrame, _inputs.frame);
		}

		public void AddInputsMerge(InputGroup _inputs){
			InputGroup merge;
			if (inputs.TryGetValue (_inputs.frame, out merge)) {
				_inputs.inputs = merge.inputs.Concat (_inputs.inputs).ToArray ();
			}
			inputs [_inputs.frame] = _inputs;
			LastFrame = Math.Max (LastFrame, _inputs.frame);
		}

		public InputGroup? GetInputsOnFrame(int frame){
			InputGroup r;
			if (inputs.TryGetValue (frame, out r))
				return r;
			return null;
		}

		//note this will override previous history
		//if you want it to actually merge, you need to do it manually
		public void CombineHistoryOverride(InputHistory history){
			foreach (var e in history.inputs) {

				//TODO CAN DELETE
				//but leave it here for now for some error checking
				if (inputs.ContainsKey (e.Key))
					throw new Exception ("InputHistory already contains frame " + e.Key);
				
				inputs [e.Key] = e.Value;
			}
			LastFrame = Math.Max (LastFrame, history.LastFrame);
		}



		//TODO serialize/deserialize routines here

	}
}