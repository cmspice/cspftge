﻿using System;
namespace CSP{

	//TODO check if indexing does what you think it does, it probably doesn't which means you need to pad this out
	public enum InputCode : byte
	{
		Start = 0, Select,
		u, d, l, r, f, b,
		A, B, C, D, E, F,
		lp, mp, hp, lk, mk, hk,
		c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, 
		c11, c12
	}

	[Serializable]
	public struct Input
	{
		public InputCode code;
		public float? value; //analog input value, kinda silly to include it with none analogy inputs but it's not a big deal

		public Input(InputCode _code)
		{
			code = _code;
			value = null;
		}

		public static readonly byte sReleaseOffset = 32;
		public static readonly byte sPlayerOffset = 64;

		public static InputCode Convert(InputCode code, int player, bool backward = false)
		{
			var r = code;
			r = r == InputCode.f ? (backward ? InputCode.l : InputCode.r) : r;
			r = r == InputCode.f + sReleaseOffset ? (backward ? InputCode.l : InputCode.r) + sReleaseOffset : r;
			r = r == InputCode.b ? (backward ? InputCode.r : InputCode.l) : r;
			r = r == InputCode.b + sReleaseOffset? (backward ? InputCode.r : InputCode.l) + sReleaseOffset : r;
			byte playerOffset = player == 0 ? (byte)0 : (byte)((player-1)*CSP.Input.sPlayerOffset);
			r = (InputCode)((byte)r + playerOffset);
			return r;
		}

		public static string GetHumanReadable(InputCode _code)
		{
			string output = "";
			byte code = (byte)_code;
			if(code >= 32){
				output += "~";
				code -= 32;
			}
			return output + ((InputCode)code).ToString ();
		}

		public float Keystate(out InputCode outCode)
		{
			if (IsRelease(code)) {
				outCode = code - sReleaseOffset;
				return 0;
			}
			outCode = code;
			return 1;
		}

		public static bool IsRelease(InputCode _code){
			return (byte)_code % sPlayerOffset >= sReleaseOffset;
		}
		public static InputCode NegativeCode(InputCode _code){
			if (IsRelease (_code))
				return _code - sReleaseOffset;
			else
				return _code + sReleaseOffset;		
		}
			

		//TODO custom serialization so it only stores value if InputCode is of a relevant type
	}

	public class Keystates : ICloneable
	{
		//this is super memory inefficient as it stores "release" keycodes which have no meaning here.
		//this stores 1 for down, 0 for not down, or value for any input with value (which is none of them right now)
		float[] states = new float[256];

		public void UpdateStates(InputGroup input)
		{
			foreach (var e in input.inputs) {
				InputCode key;
				var val = e.input.Keystate (out key);
				states [(int)key] = val;
			}
		}

		public float GetState(InputCode input)
		{
			return states [(int)input];
		}

		public bool GetButtonState(InputCode input)
		{
			if (Input.IsRelease (input))
				return states [(int)Input.NegativeCode(input)] == 1 ? false : true;
			return states [(int)input] == 0 ? false : true;
		}

		public object Clone()
		{
			Keystates r = new Keystates ();
			r.states = (float[])states.Clone ();
			return r;
		}
	}
}