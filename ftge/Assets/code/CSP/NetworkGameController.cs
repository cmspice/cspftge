﻿using System.Collections.Generic;

namespace CSP{
	
	//TODO what was this class meant to do? I think it's now replaced by game runner so either rename this to InputSourceManager or incorporate it into GameRunner..
	public class NetworkGameController{
		//DelayedGameController just keeps a timestamp of the last time PlayStateManager pulls the input
		public Timestamp LastDirtyInputTimestamp{ get; private set; }
		public void CleanInput(){
			LastDirtyInputTimestamp = new Timestamp(int.MaxValue);
		}

		InputHistory allInput = new InputHistory();
		public InputHistory AllInputs{ get { return allInput; } }

		public void AddInputs(InputGroup inputs){
			allInput.AddInputsMerge (inputs);
		}
	}
}