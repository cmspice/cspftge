﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.Linq;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace CSP{
	

	public class NetworkManager 
	{
		//whatever
		static int RECV_BUFFER_SIZE = 4098;
		static int MAX_CONNECTIONS = 10;

		//host info
		int reliableChId;
		int unreliableChId;
		int hostId = -1; //socketId

		//state info
		Guid guid;
		public Dictionary<int,ConnectionInfoData> connections = new Dictionary<int,ConnectionInfoData>();
		Int64 networkMessageCount = 0;

		public Action<int> OnConnectionReceived = (e)=>{};
		public Action<int> OnConnected = (e)=>{};
		public Action<Guid, int> OnReceiveConfirmation = (e,f)=>{}; //id, frame
		public Action<Guid, byte[]> OnReceiveData = (e,f)=>{};
		public Action<Guid, bool> OnSynchronize = (e,f)=>{}; //id, timeout, current timout is not implemented
		public Action<Guid> OnSyncReceived = e=>{};
		public Action<int, Guid> OnReceiveId = (e,f)=>{}; 
		public Action<int, byte[]> OnReceiveTest = (e,f)=>{};



		public NetworkManager()
		{
			guid = Guid.NewGuid ();
		}


		bool useSimulator = false;
		//bool useSimulator = true;
		public void CreateNetworking(int port)
		{
			if(hostId != -1)
				NetworkTransport.RemoveHost (hostId);

			GlobalConfig gConfig = new GlobalConfig();
			NetworkTransport.Init(gConfig);
			ConnectionConfig config = new ConnectionConfig();
			reliableChId  = config.AddChannel(QosType.Reliable);
			unreliableChId = config.AddChannel(QosType.Unreliable);
			HostTopology topology = new HostTopology(config, MAX_CONNECTIONS);

			if(useSimulator)
				hostId = NetworkTransport.AddHostWithSimulator(topology,300,500,port);
			else
				hostId = NetworkTransport.AddHost(topology, port);

			DebugConsole.Log ("initialized host on port " + port);
		}


		//------------------
		//debug nonsense
		//------------------

		//TODO turn this into a callback
		void TimestampReceived(int connectionId, int timestamp)
		{
			byte error;	
			int currentPeerTime = NetworkTransport.GetRemoteDelayTimeMS (hostId, connectionId, timestamp, out error);

			DebugConsole.Log (
				"received ping message from " + ConnectionInfo (connectionId) + 
				" remote time: " + timestamp + 
				" delay time time: " + currentPeerTime + 
				" network time: " + NetworkTransport.GetNetworkTimestamp());
		}

		//------------------
		//senders
		//------------------


		public void SendTimestamp(int connectionId)
		{
			int ts = NetworkTransport.GetNetworkTimestamp(); //this will get time in milliseconds
			Send(connectionId,BitConverter.GetBytes(ts),NetworkMessageType.PING);
		}

		public void Synchronize(Guid id, int wait, int timeout = -1) //set wait to something like 20x ping
		{
			//TODO finish!!
			if (timeout != -1)
				throw new Exception ("Synchronize timeout not implemented");

			int ts = NetworkTransport.GetNetworkTimestamp(); //this will get time in milliseconds
			Send (id, BitConverter.GetBytes (ts).Concat(BitConverter.GetBytes(wait)).ToArray(), NetworkMessageType.SYNCHRONIZE, false);
			var con = ConnectionByGuid (id);

			con.syncStartTime = ts;
			con.syncTimeout = timeout;
			con.syncWaitTime = wait;
			//TODO callback after wait (or does the caller handle this
			//TODO possible timout nonsense if no acknowledge is received
		}

		public void SendIdentification(int connectionId)
		{
			Send (connectionId, guid.ToByteArray (), NetworkMessageType.IDENTIFY, true);
		}



		//------------------
		//internal
		//------------------
		public byte Send(Guid id, byte[] message, NetworkMessageType type = NetworkMessageType.INPUT, bool reliable = false)
		{
			return Send (ConnectionByGuid(id).ConnectionId, message, type, reliable);
		}

		public byte Send(int connectionId, byte[] message, NetworkMessageType type = NetworkMessageType.INPUT, bool reliable = false)
		{
			
			byte[] sendmsg = new byte[1]{(byte)type};

			if (type != NetworkMessageType.INPUT) {
				sendmsg = sendmsg.Concat(BitConverter.GetBytes(networkMessageCount)).ToArray();
			}

			sendmsg = sendmsg.Concat (message).ToArray();
			if (reliable == true) { //include message length for TCP in case it gets split
				sendmsg = BitConverter.GetBytes (sendmsg.Length).Concat (sendmsg).ToArray ();
			} 

			byte error;
			NetworkTransport.Send(hostId, connectionId, reliable ? reliableChId : unreliableChId, sendmsg, sendmsg.Length,  out error);

			networkMessageCount++;

			return error;
		}



		//------------------
		//connect/disconnect
		//------------------
		public void Connect(string ip4,int port)
		{
			byte error;


			int connectionId;
			if (useSimulator) {
				ConnectionSimulatorConfig config = new ConnectionSimulatorConfig (300, 500, 300, 500, 0.05f);
				connectionId = NetworkTransport.ConnectWithSimulator (hostId, ip4, port, 0, out error, config);
			}
			else
				connectionId = NetworkTransport.Connect(hostId, ip4, port, 0, out error);

			connections [connectionId] = new ConnectionInfoData (connectionId);
			string log = "Connect to " + ip4 + ":" + port + " with status " + error;
			DebugConsole.Log (log);
		}



		public void Disconnect(int connectionId)
		{
			byte error;
			NetworkTransport.Disconnect (hostId, connectionId, out error);
			string log = "Disconnect from connection " + ConnectionInfo(connectionId) + " with status " + error;
			DebugConsole.Log (log);
		}

		//------------------
		//core
		//------------------
		void ReceiveUDP(int connectionId, byte[] buffer, int size)
		{
			NetworkMessageType mtype = (NetworkMessageType)buffer [0];

			Int64 mcnt = -1;
			int headerLn = sizeof(byte);

			//INPUT is a special message, we don't need a message count for this
			if (mtype != NetworkMessageType.INPUT){
				mcnt = BitConverter.ToInt64 (buffer, 1);
				headerLn += sizeof(Int64);
			}

			var con = connections [connectionId];

			switch (mtype) 
			{
			case NetworkMessageType.PING:
				TimestampReceived (connectionId, BitConverter.ToInt32 (buffer,headerLn));
				break;
			case NetworkMessageType.CONFIRMATION:
				int frame = BitConverter.ToInt32 (buffer, headerLn);
				OnReceiveConfirmation (con.Guid, frame);
				break;
			case NetworkMessageType.INPUT:
				OnReceiveData (con.Guid, buffer.Skip(headerLn).Take(size-headerLn).ToArray());
				break;
			case NetworkMessageType.IDENTIFY:
				var theirGuid = new Guid (buffer.Skip (headerLn).Take (16).ToArray ());
				con.Guid = theirGuid;
				OnReceiveId (connectionId, con.Guid);
				break;
			case NetworkMessageType.SENDTEST:
				OnReceiveTest (connectionId, buffer.Skip(headerLn).Take(size-headerLn).ToArray());
				break;
			case NetworkMessageType.SYNCHRONIZE:
				int timestamp = BitConverter.ToInt32 (buffer, headerLn);
				int waitTime = BitConverter.ToInt32 (buffer, headerLn+4);
				byte error;	
				int currentPeerTime = NetworkTransport.GetRemoteDelayTimeMS (hostId, connectionId, timestamp, out error);
				if (currentPeerTime > timestamp + waitTime) {
					//TODO message took too long to arrive, do something
					//probably just fail :(
				}
				if (con.syncStartTime == -1 || con.Guid.CompareTo(guid) == 1) { //if we haven't tried to sync or connection has priority
					con.syncStartTime = timestamp;
					con.syncWaitTime = waitTime;
					//TODO timeout???
				}
				OnSyncReceived (con.Guid);
				break;
			default:
				throw new Exception("Unknown message type");
			}
		}

		void ReceiveTCP(int connectionId, byte[] buffer, int size)
		{
			byte[] finalMsg = null;
			var c = connections [connectionId];
			if (!c.IsMessageBuffered) {
				int length = BitConverter.ToInt32 (buffer, 0);
				//if there is no buffer, then the message must be new and follow this format (fingers crossed)
				//could look at value of (NetworkMessageType)buffer [0]; for some basic error checking
				finalMsg = c.Store (buffer.Skip(4).ToArray(), size-4, length);
			} else {
				finalMsg = c.Store (buffer, size, -1);
			}
			if (finalMsg != null) {
				ReceiveUDP (connectionId, finalMsg, finalMsg.Length);
			}
		}

		public void UpdateNetwork()
		{
			if (hostId == -1)
				return;
			
			var networkTime = NetworkTransport.GetNetworkTimestamp ();
			foreach (var e in connections.Values) {
				if (e.syncStartTime != -1) {
					if (networkTime - e.syncStartTime > e.syncWaitTime) {
						e.syncStartTime = -1;
						e.syncTimeout = -1;
						e.syncWaitTime = -1;
						OnSynchronize (e.Guid, false);
					}
					//TODO handle timeout
				}
			}
			while (ReceiveOnce () != NetworkEventType.Nothing)
				;
				
		}

		public NetworkEventType ReceiveOnce()
		{
			int connectionId; 
			int channelId; 
			byte[] recBuffer = new byte[RECV_BUFFER_SIZE];
			int bufferSize = RECV_BUFFER_SIZE;
			int dataSize;
			byte error;

			//this receives from all
			NetworkEventType eventType = NetworkTransport.ReceiveFromHost(hostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
			switch (eventType)
			{
			case NetworkEventType.Nothing:
				break;
			case NetworkEventType.ConnectEvent:  
				//TODO figure out what happnes when both players connect to each other
				//i.e. does a new connection get created, or the old one used again?
				if (connections.ContainsKey (connectionId)) {
					DebugConsole.Log ("Connection successful from:" + ConnectionInfo (connectionId) + " id: " + connectionId);
					//connection succesful
					OnConnected(connectionId);
				} else {
					DebugConsole.Log("Connection received from:" + ConnectionInfo (connectionId) + " id: " + connectionId);
					//someone else connected to us	
					OnConnectionReceived(connectionId);
				}
				connections [connectionId] = new ConnectionInfoData (connectionId);
				break;
			case NetworkEventType.DataEvent:    
				//DebugConsole.Log ("receveived data on " + connectionId);
				if (channelId == reliableChId) { 
					ReceiveTCP (connectionId, recBuffer, dataSize);
				} else if (channelId == unreliableChId) {
					ReceiveUDP (connectionId, recBuffer, dataSize);
				}
				break;
			case NetworkEventType.DisconnectEvent: //4
				if(connections.ContainsKey(connectionId)){
					Debug.Log ("connection removed " + connectionId);
					connections.Remove(connectionId);
				} else {
					Debug.Log ("connection failed " + error.ToString ());
				}

				break;
			}
			return eventType;
		}



		//------------------
		//accessors
		//------------------
		ConnectionInfoData ConnectionByGuid(Guid id)
		{
			return connections.Values.FirstOrDefault (e => e.Guid == id);
		}



		//-----------------
		//utility
		//-----------------

		public string ConnectionInfo(int connectionId)
		{
			string address = "ERROR";
			int port;
			byte error;

			//relay stuff, can ignore
			UnityEngine.Networking.Types.NetworkID network;
			UnityEngine.Networking.Types.NodeID dstNode;

			NetworkTransport.GetConnectionInfo(hostId, connectionId, out address, out port, out network, out dstNode, out error);

			return address + ":" + port;
		}
			
		public int Ping(Guid guid)
		{
			if (connections.Where (e => e.Value.Guid == guid).Count () == 0) {
				throw new Exception ("connection does not exist!");
				return 0;
			}
			return Ping (connections.FirstOrDefault (e => e.Value.Guid == guid).Key);
		}

		public int Ping(int connectionId)
		{
			byte error;
			return NetworkTransport.GetCurrentRtt (hostId, connectionId, out error);
			//this return round time trip!
			//we could also do this manually by using GetNetworkTimestamp() and checking it with GetRemoteDelayTimeMS() on receiving end
			//I imagine this uses the Network Time Protocol and has errors ~10ms
		}
	}
}
