﻿using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;

namespace FGE
{
	//For convenience when writing callbacks, all frame state stuff is in this class rather than a derived clas
	public class Entity : ICloneable
	{
		protected AnimationEntityData data;
		public string Name { get; private set; }
		public EntityMetadata EntityMetadata { get { return data.EntityMetadata; } }

		//--------------
		//Properties
		//--------------
		//TODO replace this with TRS + add rotational velocity
		//public Point Position {get; set;}
		public EntityTransform2d Transform {get; set;}
		public Point Velocity {get; set;}
		public int Depth { get; set; } //for rendering

		int timeOnCurrentFrame = 0;
		public ImmutableValueDictionary UserData {get; private set;}
		//this is a special hard coded piece of data intended to be used for handling hit stun and block stun states
		public int? OverrideDuration {get; set;} //special override parameter



		//TODO this should be something like LastFrameSequence or something
		//so you can actually see what the last FrameSequence was...
		//public SequenceFramePair LastStartOfOnFrameState { get; private set; }
		public SequenceFramePair CurrentStartOfOnFrameState { get; private set; }
		public SequenceFramePair CurrentState { get; private set; }
		public SequenceFramePair? NextState { get; private set; }





		//--------------
		//Callbacks
		//--------------
		public FGEDelegateTypes.OnFrame OnFrame { get; protected set; }
		public FGEDelegateTypes.OnInput OnInput { get; protected set; } //TODO consider making this a part of AdvanceFrame
		public FGEDelegateTypes.OnHitEntity OnHitEntity { get; protected set; } 
		//entity, environment bounds, (x,y) overlap
		public FGEDelegateTypes.OnHitEnvironment OnHitEnvironment{get; protected set;}

		public FGEDelegateTypes.OnFrameSequenceBegin OnFrameSequenceBegin { get; protected set; }
		public FGEDelegateTypes.OnFrameSequenceEnd OnFrameSequenceEnd { get; protected set; }
		public FGEDelegateTypes.OnCreate OnCreate { get; protected set; }
		public FGEDelegateTypes.OnDestroy OnDestroy { get; protected set; }

		//TODO bonus data to send to renderer for fancy render stuff like prefabs etc..
		//I imagine it would be a string and an int for animations



		public Entity(AnimationEntityData _data){
			
			data = _data;
			Name = EntityMetadata.name;
			UserData = new ImmutableValueDictionary ();

			Transform = new EntityTransform2d ();


			if (GetSequenceReadOnly(EntityMetadata.startingSequence.sequence) == null)
				throw new Exception ("starting FrameSequence does not exist");

			CurrentState = EntityMetadata.startingSequence;
			CurrentStartOfOnFrameState = EntityMetadata.startingSequence;
			//LastStartOfOnFrameState = EntityMetadata.startingSequence;
		}

		public virtual HitboxGroup Hitboxes { 
			get { 
				//TODO scaling + rotating
				var cf = GetCurrentFrameReadonly ();
				var boxes = (HitboxGroup)cf.hitboxGroup.Clone();
				//return boxes;
				boxes.hitboxes = boxes.hitboxes.Select (
					e=>{
						e.box = (Transform.backwards ? e.box.ReflectHorizontal (0) : e.box) + Transform.pos;
						//e.box =  e.box + Transform.pos;
						return e;
					}
				).ToList ();
				return boxes;
			} 
		}


		//-------------------
		//FrameSequence related routines
		//-------------------
		//not actually readonly but don't try and change it!
		public FrameSequence GetCurrentSequenceReadOnly(){
			return data.GetFrameSequence (CurrentState.sequence);
		}

		//not actually read only but don't try and change it!
		public FrameSequence[] GetAllSequencesReadOnly(){
			return data.GetAllFrameSequences ();
		}

		//not actually read only but don't try and change it!
		public FrameSequence GetSequenceReadOnly(string seq){
			return data.GetAllFrameSequences ().FirstOrDefault (e => e.name == seq);
		}

		public Frame GetCurrentFrameReadonly(){
			return GetCurrentSequenceReadOnly().Frames [CurrentState.frame].Copy();
		}

		public bool TrySetSequenceByName(string name)
		{
			if (GetSequenceReadOnly (name) != null) {
				SetFrameSequence (name);
				return true;
			}
			return false;
		}
			
		//don't call this in callbacks, use SetNextFrameSequence instead
		public void SetFrameSequence(string name, int startFrame = 0){
			CurrentState = new SequenceFramePair (name, startFrame);
			timeOnCurrentFrame = 1;
		}

		public void SetNextFrameSequence(string fs, int startFrame = 0){
			NextState = new SequenceFramePair (fs, startFrame);
			OverrideDuration = null;
		}

		//returns startFrame if we transitioned, or -1 if we did not
		//TODO makes this return a FrameSequence/StartFrame pair
		public void AdvanceToNextFrameSequence(bool finalize = true){
			if (NextState != null) {
				SetFrameSequence (NextState.Value.sequence, NextState.Value.frame);
				if (finalize)
					NextState = null;
			}
		}

		//TODO ruse some of code from AdvanceFrame please
		public bool IsAtEndOfCurrentFrameSequence(){
			var _duration = GetCurrentFrameReadonly ().staticParameters.duration;
			int duration = _duration.HasValue ? _duration.Value : 1;
			if (duration == -1)
				duration = int.MaxValue;

			if (OverrideDuration.HasValue) {
				if (OverrideDuration.Value-1 == 0) {
					return true;
				}
			}
			if (timeOnCurrentFrame+1 < duration) {
				return false;
			}
			var seq = GetCurrentSequenceReadOnly ();
			if (seq.Frames.Length == CurrentState.frame + 1) {
				//TODO this needs to support transitioning into an arbitrary frame
				if (seq.defaultTransition != null && new SequenceFramePair(seq.defaultTransition).sequence != CurrentState.sequence)
					return true;
			}
			return false;
		}

		public void StartOfOnFrame(){
			//LastStartOfOnFrameState = CurrentStartOfOnFrameState;
			CurrentStartOfOnFrameState = CurrentState;
		}

		//returns startFrame if we transitioned, or -1 if we did not
		//TODO makes this return a FrameSequence/StartFrame pair
		public void AdvanceFrame(PlayState state){
			
			var seq = GetCurrentSequenceReadOnly ();
			var frame = GetCurrentFrameReadonly ();

			var cs = CurrentState;

			var _duration = frame.staticParameters.duration;
			int duration = _duration.HasValue ? _duration.Value : 1;
			if (duration == -1)
				duration = int.MaxValue;

			//TODO
			//if(duration == 0) this means use override duration???
			//note if override duration is 0, it will still stay on this frame for 1 frame
			//we should probably do a special case for duration = 0 to auto advance to the next frame or animation

			timeOnCurrentFrame++;

			//special override duration situation
			if (OverrideDuration.HasValue) {
				OverrideDuration = OverrideDuration.Value - 1;
				if (OverrideDuration.Value == 0) {
					if (seq.defaultTransition != null){
						if (SequenceFramePair.IsDestroySequence (seq.defaultTransition)) {
							state.DestroyEntity (this);
						}
						else
							cs = new SequenceFramePair (seq.defaultTransition);
					} else {
						cs.frame = 0;
					}
					OverrideDuration = null;
					timeOnCurrentFrame = 1;
					CurrentState = cs;
					return;
				}
			}

			//

			//stay on current frame
			if (timeOnCurrentFrame < duration) {
				return;
			}

			//advance
			timeOnCurrentFrame = 1;

			if (seq.Frames.Length == cs.frame + 1) {
				//TODO this needs to support transitioning into an arbitrary frame
				if (seq.defaultTransition != null && new SequenceFramePair(seq.defaultTransition).sequence != cs.sequence) {
					if (SequenceFramePair.IsDestroySequence (seq.defaultTransition)) {
						state.DestroyEntity (this);
					}
					else {
						cs = new SequenceFramePair (seq.defaultTransition);
					}
				} else {
					cs.frame = 0;
				}
			} else
				cs.frame++;
			

			CurrentState = cs;
		}

		public virtual void SetCallbacks()
		{
			var sp = GetCurrentFrameReadonly ().staticParameters;
			//TODO

			//TODO read this from entity metadat
			//default callbacks
			OnFrame = EntityMetadata._OnFrame == null ? null : EntityCallbackMap.sOnFrameMap[EntityMetadata._OnFrame];
			OnInput = EntityMetadata._OnInput == null ? null : EntityCallbackMap.sOnInputMap[EntityMetadata._OnInput];
			OnHitEntity = EntityMetadata._OnHitEntity == null ? null : EntityCallbackMap.sOnHitEntityMap[EntityMetadata._OnHitEntity];
			OnHitEnvironment = EntityMetadata._OnHitEnvironment == null ? null : EntityCallbackMap.sOnHitEnvironmentMap[EntityMetadata._OnHitEnvironment];
			OnFrameSequenceEnd = EntityMetadata._OnFrameSequenceEnd == null ? null : EntityCallbackMap.sOnFrameSequenceEndMap [EntityMetadata._OnFrameSequenceEnd];
			OnFrameSequenceBegin = EntityMetadata._OnFrameSequenceBegin == null ? null : EntityCallbackMap.sOnFrameSequenceBeginMap [EntityMetadata._OnFrameSequenceBegin];
			OnCreate = EntityMetadata._OnCreate == null ? null : EntityCallbackMap.sOnCreateMap [EntityMetadata._OnCreate];
			OnDestroy = EntityMetadata._OnDestroy == null ? null : EntityCallbackMap.sOnDestroyMap [EntityMetadata._OnDestroy];
					

		}

		protected void CopyToEntity(Entity copy)
		{
			//name and data already set by constructor so we don't need to do it again

			copy.CurrentState = CurrentState;
			copy.CurrentStartOfOnFrameState = CurrentStartOfOnFrameState;
			copy.NextState = NextState;
			//copy.LastStartOfOnFrameState = LastStartOfOnFrameState;

			copy.timeOnCurrentFrame = timeOnCurrentFrame;
			copy.OverrideDuration = OverrideDuration;


			copy.Depth = Depth;
			copy.Transform = (EntityTransform2d)Transform.Clone();
			copy.Velocity = Velocity;

			copy.UserData = (ImmutableValueDictionary)UserData.Clone ();

			copy.OnFrame = OnFrame;
			copy.OnHitEntity = OnHitEntity;
			copy.OnHitEnvironment = OnHitEnvironment;
			copy.OnInput = OnInput;
		}
			
		public virtual object Clone(){
			var r = new Entity (data);
			CopyToEntity (r);
			return r;
		}

	}

	[Serializable]
	public class Player : Entity
	{
		public Player(AnimationEntityData _data) : base(_data){
			//TODO
		}


		public int id; //1 for player 1, 2 for player 2
		//hitpoints, user data

		public override object Clone()
		{
			Player r = new Player (data);	
			CopyToEntity (r);
			r.id = id;
			return r;
		}
	}


}