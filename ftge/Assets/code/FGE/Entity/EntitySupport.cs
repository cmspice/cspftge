﻿using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;

namespace FGE
{
	public class FGEDelegateTypes
	{
		public delegate void OnFrame(PlayState state, Entity entity);	
		public delegate string OnInput(PlayState state, Entity entity, CSP.InputHistory inputs);
		public delegate void OnHitEntity(PlayState state, Entity A, Entity B, HitboxCollisions collision);
		//TODO redo types, we don't pass in exact collision information anymore, just location of hit
		public delegate void OnHitEnvironment(PlayState state, Entity entity, Rectangle environment, Point overlap);

		public delegate void OnFrameSequenceEnd(PlayState state, Entity entity);
		public delegate void OnFrameSequenceBegin(PlayState state, Entity entity);
		public delegate void OnCreate(PlayState state, Entity entity);
		public delegate void OnDestroy(PlayState state, Entity entity);
	}

	public struct EntityMetadata
	{

		[Newtonsoft.Json.JsonIgnore]
		public string assetPath;

		//TODO type information

		public string name;

		//initialization info
		public SequenceFramePair startingSequence;
		//TODO other information such as starting health, power meter, etc..

		//character
		public float totalHealth;

		public string entityType;
		public string tags;

		//movement
		public Point? gravity;
		public float? airFriction;
		public float? groundFriction;

		//callbacks
		public string _OnFrame,_OnInput,_OnHitEntity,_OnHitEnvironment,_OnFrameSequenceEnd,_OnFrameSequenceBegin,_OnCreate,_OnDestroy;

		//other stuff
		Dictionary<string,string> customConstData;

		//ensure immutability of EntityMetadata
		public string GetCustomData(string key)
		{
			return customConstData [key];
		}


		/*TODO DELETE don't store these here
		//default callbacks
		[Newtonsoft.Json.JsonIgnore]
		public FGEDelegateTypes.OnFrame OnFrame;
		[Newtonsoft.Json.JsonIgnore]
		public FGEDelegateTypes.OnInput OnInput;
		[Newtonsoft.Json.JsonIgnore]
		public FGEDelegateTypes.OnHitEntity OnHitEntity;
		[Newtonsoft.Json.JsonIgnore]
		public FGEDelegateTypes.OnHitEnvironment OnHitEnvironment;
		[Newtonsoft.Json.JsonIgnore]
		public FGEDelegateTypes.OnFrameSequenceBegin OnFrameSequenceBegin;
		[Newtonsoft.Json.JsonIgnore]
		public FGEDelegateTypes.OnFrameSequenceEnd OnFrameSequenceEnd;
		[Newtonsoft.Json.JsonIgnore]
		public FGEDelegateTypes.OnCreate OnCreate;
		[Newtonsoft.Json.JsonIgnore]
		public FGEDelegateTypes.OnDestroy OnDestroy;
		*/

	}

	public class ImmutableValueDictionary : ICloneable
	{
		Dictionary<string,object> data = new Dictionary<string, object>();

		public T Get<T>(string key)
		{
			return (T)data [key];
		}

		public void Set<T>(string key, T value)
		{
			data [key] = value;
		}

		public bool ContainsKey(string key)
		{
			return data.ContainsKey (key);
		}

		public void Remove(string key)
		{
			data.Remove (key);
		}

		public object Clone()
		{
			ImmutableValueDictionary r = new ImmutableValueDictionary ();
			foreach (var e in data) {
				if (e.Value is ValueType) {
					r.data.Add (e.Key, (ValueType)e.Value);
				} else if (e.Value is string) {
					r.data.Add (e.Key, (string)((string)e.Value).Clone ());
				} else {
					throw new Exception ("attempting to clone ImmutableValueDictionary with unsupported type");
				}
			}
			return r;
		}
	}

	public class EntityTransform2d : ICloneable
	{
		public Point pos = new Point(0,0);
		public float scale = 1;
		public float rot = 0; //RADIANS
		public bool backwards = false;

		public Point TransformPoint(Point pt)
		{
			Point r = pt;
			r.x = backwards ? -r.x : r.x;

			//scale
			r *= scale;

			//float angle = backwards ? -rot : rot;
			float angle = rot;

			//rotate
			float s = (float)Math.Sin(angle);
			float c = (float)Math.Cos (angle);
			r = new Point(r.x * c - r.y * s, r.x * s + r.y * c);

			//translate
			r.y += pos.y;
			r.x += pos.x;

			return r;
		}


		public object Clone()
		{
			var r = new EntityTransform2d ();
			r.pos = pos;
			r.scale = scale;
			r.rot = rot;
			r.backwards = backwards;
			return r;
		}
	}
}