﻿using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;

namespace FGE
{

	public struct ConditionalCancel
	{
		public string cancel; //this could be a tag, or name of a specific animation, make sure you don't have animations named the same as tags :D
		public string condition; //by default, no condition
		public int? overridePriority;
		public string overrideComboString;

		public ConditionalCancel(string _cancel, string _condition = "true", int? _overridePriority = null){
			overridePriority = _overridePriority;
			condition = _condition;
			cancel = _cancel;

			overrideComboString = null;
			OverrideCombo = null;
		}

		public bool DoesCancelInto(FrameSequence _seq)
		{
			string[] check = cancel.Split (new char[] {','}, StringSplitOptions.RemoveEmptyEntries);
			foreach (var e in check) {
				if (e [0] == '!') {
					if (_seq.name == e.Substring (1))
						return false;
				}
			}
			foreach (var e in check) {
				if (_seq.name == e)
					return true;
				if (_seq.tags.Contains (e))
					return true;
			}
			return false;
		}


		[Newtonsoft.Json.JsonIgnore]
		public NewInputCombo OverrideCombo{ get; private set; }

		[System.Runtime.Serialization.OnDeserialized]
		internal void OnDeserializedMethod(System.Runtime.Serialization.StreamingContext context)
		{
			if (overrideComboString != null) {
				OverrideCombo = new NewInputCombo (overrideComboString);
			}
		}
	}

	public struct SpawnEntityParameters
	{
		public string name;
		public string startingSequence;
		public Point? startingPositionRelativeToPlayer;
		public Point? startingPositionRelativeToStage;

		//TODO OnSpawned callback? (or do we just use OnCreate)

		//TODO starting custom entity data

	}

	public struct FrameParameters
	{
		//these parameters always have values but they are still nullable so we can mark them for override
		public int? duration; //if null, this is assumed to be 1
		public int? pause; //if null, this is assumed to be 0

		//TODO make this not mutable
		public Dictionary<string,string> customData;

		//all frames
		public float? dx, dy, dvx, dvy, resetvx, resetvy;
		public ConditionalCancel[] cancels;
		public string audio;
		public bool? allowFaceChange; 
		public SpawnEntityParameters[] spawnList;

		//attack frames
		public int? id; //if no id, then assumes it's a unique id
		public int? damage;
		public string hitType;
		public int? hitStun;
		public int? blockStun;
		public int? hitPause;
		public int? blockPause;
		public int? priority;
		public Point? knockback;
		public Point? knockbackVelocity;

		//callbacks
		public string OnFrame;
		public string OnInput;
		public string OnHitEntity;
		public string OnHitEnvironment;
		public string OnFrameSequenceEnd;
		public string OnFrameSequenceBegin;

	}
		
	public enum FrameType
	{
		NONE,
		SPRITE,
		SPINE2D,
		MECHANIM,
		PREFAB,
		PREFAB_ONESHOT //this one gets destroyed immediately
	}
	//treat this as a value type
	//it's only a reference type because I didn't want the hitbox editor code to be a nightmare.
	[Serializable]
	public class Frame
	{
		public string name;
		public HitboxGroup hitboxGroup = new HitboxGroup();

		//-----------
		//sprite mode
		//-----------
		public SpriteInfo? sprite;

		//-----------
		//spine2D mode
		//-----------
		//TODO

		//-----------
		//mechanim mode
		//-----------
		//TODO

		//-----------
		//Prefab mode
		//-----------
		public PrefabInfo? prefab;

		public FrameType GetFrameType(){
			if (sprite != null)
				return FrameType.SPRITE;
			if (prefab != null)
				return prefab.Value.oneShot ? FrameType.PREFAB_ONESHOT : FrameType.PREFAB;
			return FrameType.NONE;
		}

		[Newtonsoft.Json.JsonIgnore]
		public FrameParameters staticParameters;

		public Frame Copy(){
			Frame r = new Frame ();
			r.name = name;
			r.sprite = sprite;
			r.prefab = prefab;
			r.hitboxGroup = (HitboxGroup)hitboxGroup.Clone ();
			r.staticParameters = staticParameters;
			return r;
		}
	}

	public struct SpriteInfo
	{
		public string texture;
		public Point offset; //offset from bottomleft of crop
		public Rectangle crop;
		public string shader; //overrides default shader
	}

	public struct PrefabInfo
	{
		//one shot prefab are expected to clean up after themselves, they will not be destroyed by RenderManager
		//they are intended for non-gameplay affecting visual effects
		public string prefabName;
		public bool oneShot; 
		public string scriptName;
	}

	public class IndexedFrameParameters
	{
		public int startFrame = 0;
		public int count = 1;
		public FrameParameters parameters;
	}
		
	//NOTE do not change anything in this class or its members after it is instantiated
	//it's a reference type to make the code less crazy
	public class FrameSequence
	{
		public string name;
		public string defaultTransition;

		//special stuff for players
		[Newtonsoft.Json.JsonIgnore]
		public NewInputCombo combo;

		public int priority;
		public string inputComboString;
		public string[] tags;

		public bool ignorePause;

		//intermediary object used for serialization
		public List<IndexedFrameParameters> serializedFrameParameters = new List<IndexedFrameParameters> (); 

		[Newtonsoft.Json.JsonIgnore]
		public Frame[] Frames { get; private set; }

		//adds frames and sets their frame parameters
		public void InitializeAfterSerialization(Frame[] _frames)
		{
			FrameSequence.SetIndexedFrameParameters(ref _frames, serializedFrameParameters);
			Frames = _frames;
			if (!string.IsNullOrEmpty (inputComboString)) {
				combo = new NewInputCombo (inputComboString);
			}
			if (tags == null)
				tags = new string[0];
		}

		//sets frame parameters from indexed frame parameters
		public static void SetIndexedFrameParameters(ref Frame[] frames, List<IndexedFrameParameters> indexedParams)
		{
			foreach (var e in indexedParams) {
				foreach (FieldInfo field in typeof(FrameParameters).GetFields()) {
					object val = field.GetValue (e.parameters);
					if (val != null) {
						//UnityEngine.Debug.Log (val.ToString () + " " + field.Name);
						for (int i = e.startFrame; i < e.startFrame + e.count; i++) {
							if (i >= frames.Length)
								throw new Exception ("IndexedFrameParameters index out of bounds");
							//TODO make sure property name is what you think it is
							if (field.Name == "customData" && e.parameters.customData != null) { //if there is already customData, merge it
								foreach (var f in val as Dictionary<string,string>) {
									//TODO this is sloppy b/c customData is mutable. fix it
									frames[i].staticParameters.customData [f.Key] = f.Value;
								}
							} else { //otherwise, override it
								object boxed = frames [i].staticParameters;
								field.SetValue (boxed, val);
								frames [i].staticParameters = (FrameParameters)boxed;
							}
						}
					}
				}
			}
		}
	}

	public struct SequenceFramePair
	{
		public string sequence;
		public int frame;
		public SequenceFramePair (string _seq, int _frame)
		{
			sequence = _seq;
			frame = _frame;
		}

		public SequenceFramePair(string formattedInput)
		{
			frame = 0;
			System.Text.RegularExpressions.Regex options = new System.Text.RegularExpressions.Regex("\\[(.*?)\\]");
			var match = options.Match(formattedInput);
			while(match.Success){
				string[] value = match.Value.Substring (1, match.Length - 2).Split (new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);	
				if (value.Length == 0) {
					//TODO warning empty option
				} else if (System.Text.RegularExpressions.Regex.IsMatch (value [0], @"^\d+$")) {
					frame = Convert.ToInt32 (value [0]);
				} 
				match = match.NextMatch ();
			}
			sequence = options.Replace(formattedInput,"");
		}

		public static bool IsDestroySequence(string input)
		{
			System.Text.RegularExpressions.Regex options = new System.Text.RegularExpressions.Regex("\\[(.*?)\\]");
			var match = options.Match(input);
			if (match.Success) {
				if (match.Value.Substring (1, match.Length - 2) == "destroy"){
					return true;
				}
			}
			return false;
		}
	}

	public class AnimationEntityData 
	{
		Dictionary<string,FrameSequence> animations;
		public EntityMetadata EntityMetadata{get; private set;}

		//DO NOT MODIFY ITS RETURN VALUE
		public FrameSequence GetFrameSequence(string key){
			return animations [key];
		}

		//DO NOT MODIFY RETURN VALUE
		public FrameSequence[] GetAllFrameSequences(){
			return animations.Select (e => e.Value).ToArray ();
		}

		public static AnimationEntityData Create(AssetGroup assets, string assetId)
		{
			AnimationEntityData r = new AnimationEntityData ();

			EntityMetadata emd = Newtonsoft.Json.JsonConvert.DeserializeObject<EntityMetadata> (assets.GetEntityMetadata ().text);
			emd.assetPath = assetId;
			r.EntityMetadata = emd;

			r.animations = new Dictionary<string, FrameSequence> ();
			var files = assets.GetAllSequences ();
			foreach(var e in files){
				var sequence = Newtonsoft.Json.JsonConvert.DeserializeObject<FrameSequence> (e.sequence.text);
				var frames = Newtonsoft.Json.JsonConvert.DeserializeObject<Frame[]> (e.frames.text);
				sequence.InitializeAfterSerialization (frames);
				r.animations [sequence.name] = sequence;
			}

			return r;
		}
	}
}
