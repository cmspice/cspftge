﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
using CSP;
namespace FGE{

	public class PlayStateShallowData{

		//TODO why are these properties... just use variables
		public AudioManager Audio { get; set; } //audio follows different rollback rules, so we put it here instead and manually call rollback
		public Dictionary<string, AnimationEntityData> EntityData { get; set; }
		public Action<int> OnMatchOver {get; set;}
	}

	[Serializable]
	public class PlayStateDeepData : ICloneable
	{
		public Player p1;
		public Player p2;
		public List<Entity> other;

		public Keystates keystates;

		public int pauseFrames;
		public bool paused; //this is distinct from pauseFrames because these two may not have the same value during update.

		public Rectangle environment;

		public object Clone(){
			var r = new PlayStateDeepData ();
			r.keystates = (Keystates)keystates.Clone ();
			r.p1 = (Player)p1.Clone ();
			r.p2 = (Player)p2.Clone ();
			r.other = other.Select (e => (Entity)e.Clone ()).ToList ();
			r.pauseFrames = pauseFrames;
			r.paused = paused;
			r.environment = environment;
			return r;
		}
	}

	public class PlayState : IPlayState
	{

		public PlayStateShallowData shallowData; //should I even be storing this in PlayState?
		PlayStateDeepData data;



		//TODO should probably move these into PlayStateDeepData??
		//TODO think these through more carefully, this should maybe be a part of IPlayState??
		public int CurrentFrame{ get; protected set; }
		public int StartFrame{ get; protected set; }

		public int PauseFrames { get{ return data.pauseFrames; } set{ data.pauseFrames = value; } }
		public bool Paused{get{return data.paused;} set{data.paused = value;}}
		public IEnumerable<Entity> AllEntities { get { return (new Entity[]{ data.p1, data.p2 }).Concat (data.other); } }
		public Player Player1 { get { return data.p1; } }
		public Player Player2 { get { return data.p2; } }
		public Keystates Keystates { get { return data.keystates; } }
		public Rectangle Environment { get { return data.environment; } set { data.environment = value; } }



		public Rectangle Viewport { 
			get { 
				var avg = (Player1.Transform.pos + Player2.Transform.pos)*0.5f;
				//TODO figure out screen size
				//TODO crop based on Environment and screen size
				return new Rectangle(); 
			} 
		}

		//TODO move to deepdata


		public IPlayState Copy(){
			PlayState r = new PlayState ();
			r.shallowData = shallowData; 
			r.data = (PlayStateDeepData)data.Clone ();

			r.CurrentFrame = CurrentFrame;
			r.StartFrame = StartFrame;
			return r;
		}

		public void Initialize(List<AssetGroup> assets, string p1Char, string p2Char, int startFrame)
		{
			data = new PlayStateDeepData ();
			data.keystates = new Keystates ();

			shallowData = new PlayStateShallowData ();
			shallowData.Audio = new AudioManager ();
			shallowData.EntityData = new Dictionary<string, AnimationEntityData> ();
			foreach (var e in assets) {
				var anidata = AnimationEntityData.Create (e,e.FullFilePath);
				//Debug.Log (anidata.EntityMetadata.name);
				shallowData.EntityData [anidata.EntityMetadata.name] = anidata;
				if(anidata.EntityMetadata.name == p1Char)
					data.p1 = new Player (anidata);
				if(anidata.EntityMetadata.name == p2Char)
					data.p2 = new Player (anidata);
			}
			if (data.p1 == null || data.p2 == null)
				throw new Exception ("Character does not exists in assets");

			data.p2.Transform.backwards = true;

			data.p1.Transform.pos = new Point (-200, 0);
			data.p2.Transform.pos = new Point (200, 0);

			data.p1.id = 1;
			data.p2.id = 2;

			data.other = new List<Entity> ();

			data.pauseFrames = 0;
			data.paused = false;


			//it's up to you to make sure this is in sync with inputsource
			CurrentFrame = StartFrame = startFrame;
			Environment = new Rectangle (new Point (-500, 0), new Point (1000, 1000));
			//TODO load backgrounds

		}


		public void Update(InputHistory inputs)
		{
			//update keystates
			var cInputs = inputs.GetInputsOnFrame(CurrentFrame);
			if(cInputs.HasValue)
				data.keystates.UpdateStates(cInputs.Value);

			var all = AllEntities.ToList();
			Paused = PauseFrames > 0;
			var unpaused = PauseFrames == 0 ? all : all.Where (e => e.GetCurrentSequenceReadOnly ().ignorePause == true).ToList();
			PauseFrames = Math.Max (0, PauseFrames - 1);


			//handle input
			foreach (var e in unpaused) {
				if (e.OnInput != null) {
					var newSequence = e.OnInput (this, e, inputs);
					if (newSequence != null) {
						DebugConsole.Log ("TRANSITION TO " + newSequence);
						var sfp = new SequenceFramePair (newSequence);
						e.SetNextFrameSequence (sfp.sequence,sfp.frame);
					}
				}
			}

			foreach (var e in unpaused) {
				if (e.NextState != null || e.IsAtEndOfCurrentFrameSequence ()) {
					if(e.OnFrameSequenceEnd != null)
						e.OnFrameSequenceEnd (this, e);
				}
			}

			//advance to next sequence/frame
			foreach (var e in unpaused) {
				if (e.NextState != null) {
					e.AdvanceToNextFrameSequence ();
				} else if (e.CurrentState.sequence == e.CurrentStartOfOnFrameState.sequence) {
					e.AdvanceFrame (this);
				}
				e.SetCallbacks ();
			}

			foreach (var e in unpaused) {
				if (e.CurrentState.sequence != e.CurrentStartOfOnFrameState.sequence) {
					if(e.OnFrameSequenceBegin != null)
						e.OnFrameSequenceBegin (this, e);
				}
			}

			foreach (var e in unpaused) {
				e.StartOfOnFrame ();
			}

			foreach (var e in unpaused) {
				if (e.OnFrame != null)
					e.OnFrame (this, e);
			}


			//strictly speaking, we could still run collisions during pause frames
			if (!Paused) {
				//entity-entity collisions
				var entities = all.ToArray ();
				for (int i = 0; i < entities.Length; i++) {
					if (entities [i].Hitboxes == null)
						continue;
					for (int j = i + 1; j < entities.Length; j++) {
						if (entities [j].Hitboxes == null)
							continue;
						var EA = entities [i];
						var EB = entities [j];
						var collisions = HitboxGroup.CollideAll (
							                Hitbox.sDefaultCollisionMask, 
							                EA.Hitboxes,
							                EB.Hitboxes);
						ResolveEntityEntityCollision (this, EA, EB, collisions);
					}
				}
			}

			//special case of the abvoe, because we don't treat environment as an entity
			foreach (var e in all) {
				if (e.OnHitEnvironment == null)
					continue;
				float left, right, top, bottom;
				if (e.Hitboxes != null && HitboxGroup.CollideEnvironment (out left, out right, out bottom, out top, Environment, e.Hitboxes, HitboxMask.BODY)) {
					//we assume an entity can not collide with left+right walls or ceiling+floor at the same time
					e.OnHitEnvironment (this, e, Environment, new Point (-left + right, -bottom + top));
				}
			}

			//advance to next sequence for render purposes
			//this will render the most up to date version of the game state
			//this will also make inputs on the next frame be based on post-collision state
			foreach (var e in unpaused) {
				if (e.NextState != null) {
					if(e.OnFrameSequenceEnd != null)
						e.OnFrameSequenceEnd (this, e);
				}
			}
			foreach (var e in unpaused) {
				//TODO this needs to call OnFrameSequenceEnd
				e.AdvanceToNextFrameSequence (false);
				e.SetCallbacks ();
			}

			//resolve collisions at very end in case of funny hit box changes
			//note, this will not trigger OnHitEnvironment callbacks
			//this is a hard limit for the OnHitEntity/Environment->new collisions->OnHitEntityEnvironment->... infinite loop
			ResolvePlayerEnvironmentCollisions ();

			CurrentFrame++;
		}

		public void DestroyEntity(Entity _entity)
		{
			//TODO queue in on destroy callback
			if (_entity.OnDestroy != null)
				_entity.OnDestroy (this,_entity);
			data.other.Remove (_entity);
		}

		public void AddEntity(Entity _entity)
		{
			//TODO queue in on create callback
			if (_entity.OnCreate != null)
				_entity.OnCreate (this,_entity);
			data.other.Add (_entity);
		}
			


		//----------------------
		//collision helper stuff
		//----------------------
		public void ResolvePlayerEnvironmentCollisions()
		{
			//final player - player - environment collision resolution 
			var players = (data.p1.Transform.pos.x + data.p2.Transform.pos.x)/2f <= 0 ? new Entity[]{ data.p1, data.p2 } :  new Entity[]{ data.p2, data.p1 };
			foreach(var e in new Entity[]{ data.p1, data.p2 })
			{
				float left, right, top, bottom;
				if (e.Hitboxes != null && HitboxGroup.CollideEnvironment (out left, out right, out bottom, out top, Environment, e.Hitboxes, HitboxMask.BODY)) {
					//we assume an entity can not collide with left+right walls or ceiling+floor at the same time
					Point p = new Point (-left + right, -bottom + top);

					var newPos = e.Transform.pos;
					newPos.x -= p.x;
					newPos.y -= p.y;
					e.Transform.pos = newPos;

					Player other = e == Player1 ? Player2 : Player1;
					if (p.x != 0) {
						var collision = HitboxGroup.CollideAll (
							new List<HitboxMask[]> (){ new HitboxMask[]{ HitboxMask.BODY, HitboxMask.BODY } },
							e.Hitboxes,
							other.Hitboxes);
						if (collision.Count > 0) {
							var npos = other.Transform.pos;
							npos.x -= (float)Math.Sign (p.x) * collision [0].collision.Value.size.x;
							other.Transform.pos = npos;
						}
					}
				}
			}
		}

		class HitPriorityTuple{
			public int priority = 0;
			public Entity A;
			public Entity B;
			public HitboxCollisions collision;
		}

		public static void ResolveEntityEntityCollision(PlayState state, Entity a,Entity b, List<HitboxCollisions> collisions){
			//body body collisions
			foreach (var e in collisions) {
				if (e.maska == HitboxMask.BODY && e.maskb == HitboxMask.BODY) {
					var split = e.collision.Value.size.x;
					float aMove = a.Transform.pos.x <= b.Transform.pos.x ? -split / 2f : split / 2f;
					float bMove = -aMove;

					var newAPosition = a.Transform.pos;
					var newBPosition = b.Transform.pos;
					newAPosition.x += aMove;
					newBPosition.x += bMove;
					a.Transform.pos = newAPosition;
					b.Transform.pos = newBPosition;

					//TODO try and move a by split/2
					//try and move b by (split/2 + a collision amnt w/ environment)
					//move a by b collision amnt w/ environment
					//do we still call OnHitEntity here? Probably should for consistency
				}
			}

			//Hits
			List<HitPriorityTuple> hits = new List<HitPriorityTuple>();
			foreach (var e in collisions) { 
				HitPriorityTuple hit = new HitPriorityTuple ();
				hit.A = a;
				hit.B = b;
				hit.collision = e;

				//we're only hard coded to handle these cases
				if (e.maska == HitboxMask.PROJECTILE ||e.maska == HitboxMask.HIT || e.maska == HitboxMask.GRAB) {
					hit.priority = a.GetCurrentFrameReadonly ().staticParameters.priority.GetValueOrDefault();
					hits.Add (hit);
				} 
				if (e.maskb == HitboxMask.PROJECTILE || e.maskb == HitboxMask.HIT || e.maskb == HitboxMask.GRAB) {
					hit.priority = b.GetCurrentFrameReadonly ().staticParameters.priority.GetValueOrDefault();
					hits.Add (hit);
				}

				//TODO fireballs
			}

			int maxPriority = hits.Aggregate<HitPriorityTuple,int>(-1,(p,e)=>Mathf.Max(p,e.priority));
			foreach (var e in hits) {
				if (e.priority == maxPriority) {
					if(e.A.OnHitEntity != null)
						e.A.OnHitEntity (state, e.A, e.B, e.collision);
					if(e.B.OnHitEntity != null)
						e.B.OnHitEntity (state, e.B, e.A, e.collision.Flipped ());
				}
			}
		}


		public float CurrentStateImportance()
		{
			return 0;
		}
	}
}
