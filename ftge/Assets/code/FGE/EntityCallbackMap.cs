﻿using System.Collections.Generic;

namespace FGE
{

	public class EntityCallbackMap
	{
		public static Dictionary<string,FGEDelegateTypes.OnCreate> sOnCreateMap = new Dictionary<string,FGEDelegateTypes.OnCreate>()
		{
			//{"<player:default>",null}
		};

		public static Dictionary<string,FGEDelegateTypes.OnDestroy> sOnDestroyMap = new Dictionary<string,FGEDelegateTypes.OnDestroy>()
		{
			//{"<player:default>",null}
		};

		public static Dictionary<string,FGEDelegateTypes.OnFrame> sOnFrameMap = new Dictionary<string,FGEDelegateTypes.OnFrame>()
		{
			{"<player:default>",DefaultPlayerCallbacks.OnFrame},
			{"<projectile:default>",DefaultPlayerCallbacks.OnFrame} //TODO should make separate version for player/projectile
		};


		public static Dictionary<string,FGEDelegateTypes.OnFrameSequenceBegin> sOnFrameSequenceBeginMap = new Dictionary<string,FGEDelegateTypes.OnFrameSequenceBegin>()
		{
			//{"<projectile:default>",null},
			{"<player:default>",DefaultPlayerCallbacks.OnFrameSequenceBegin}

		};

		public static Dictionary<string,FGEDelegateTypes.OnFrameSequenceEnd> sOnFrameSequenceEndMap = new Dictionary<string,FGEDelegateTypes.OnFrameSequenceEnd>()
		{
			//{"<projectile:default>",null},
			{"<player:default>",DefaultPlayerCallbacks.OnFrameSequenceEnd}
		};

		public static Dictionary<string,FGEDelegateTypes.OnHitEntity> sOnHitEntityMap = new Dictionary<string,FGEDelegateTypes.OnHitEntity>()
		{
			{"<player:default>",DefaultPlayerCallbacks.OnHitEntity},
			{"<projectile:default>",DefaultProjectileCallbacks.OnHitEntity}
		};

		public static Dictionary<string,FGEDelegateTypes.OnHitEnvironment> sOnHitEnvironmentMap = new Dictionary<string,FGEDelegateTypes.OnHitEnvironment>()
		{
			{"<player:default>",DefaultPlayerCallbacks.OnHitEnvironment},
			{"<projectile:default>",DefaultProjectileCallbacks.OnHitEnvironment}
		};

		public static Dictionary<string,FGEDelegateTypes.OnInput> sOnInputMap = new Dictionary<string,FGEDelegateTypes.OnInput>()
		{
			//{"<projectile:default>",null},
			{"<player:default>",DefaultPlayerCallbacks.OnInput}
		};
	}
}