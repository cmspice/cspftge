﻿namespace FGE
{
	public class FGEConstants
	{
		public static readonly bool DEBUG_DRAW_HITBOXES = true;
		public static readonly float framesPerSecond = 30;
		public static readonly Point gravity = new Point(0,-8);
		public static readonly float airFriction = 0.05f;
		public static readonly float groundFriction = 0.2f;

	}
}