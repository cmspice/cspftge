﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using CSP;
using System.Text.RegularExpressions;	
namespace FGE
{
	public class NewInputCombo
	{
		public class Option
		{
			public List<Option> followups = new List<Option>();
			public List<Option> parents = new List<Option>();
			public List<CSP.Input> input = new List<CSP.Input>();

			//TODO options!


			public bool IsDummy(){
				return input.Count == 0;
			}

			public int Depth(){
				int selfDepth = input.Count == 0 ? 0 : 1;
				if (followups.Count == 0)
					return selfDepth;
				return followups.Max(e=>e.Depth()) + selfDepth;
			}

			public List<Option> Heads(){
				if(IsDummy())
					return followups;
				else 
					return new List<Option>(){this};
			}

			public List<Option> Tails(){
				List<Option> r = new List<Option> ();
				if (followups.Count == 0)
					r.Add (this);
				else {
					foreach (var e in followups)
						r.AddRange (e.Tails ());
				}
				return r;
			}
		}

		int lookback = -1;

		//TODO flatten this so it's linear in memory making it more efficient and easier to deep copy
		Option combo;
		InputCode? held;


		//TODO rewrite this so it searches backwards, which is both more efficient and easier to do stuff like "charge"
		public bool CheckCombo(int currentFrame, InputHistory history, Keystates keystates, int player, bool backward)
		{
			//we do held keys as a special case
			if (held != null) {
				if (keystates.GetButtonState (CSP.Input.Convert(held.Value,player,backward))) {
					if (combo == null) {
						return true;
					}
				} else {
					return false;
				}
			}
			if (combo == null)
				return false;

			//TODO this is inefficient, don't do it this way
			//TODO add reverse index accessor to InputHistory...
			List<InputCode> inputs = new List<InputCode> ();
			int pLook = lookback == -1 ? 100 : lookback;
			for (int i = Math.Max (0, currentFrame - pLook + 1); i <= currentFrame; i++) {
				if(history.inputs.ContainsKey(i))
					inputs.AddRange (history.inputs [i].inputs.Select (e => e.input.code));
			}

			Stack<int> indices = new Stack<int> ();
			Stack<Option> path = new Stack<Option> ();
			foreach (var e in combo.Tails()) {
				path.Push (e);
				indices.Push (inputs.Count -1);
			}

			while (path.Count > 0) {

				Option current = path.Peek ();

				int procIndex = indices.Peek ();
				bool sucess = false;

				//check current input
				if (!current.IsDummy ()) {
					List<InputCode> looking = current.input.Select(e=>CSP.Input.Convert(e.code,player,backward)).ToList ();

					sucess = true;
					while (looking.Count > 0) {
						
						//TODO handle ! cases
						//if !<code><#> look back until first frame that is less than frame of last input - #
						//fail if it's trying to look further pack than currentframe-lookback

						if (procIndex < 0) {
							sucess = false;
							break;
						}
						InputCode input = inputs [procIndex];
						procIndex--; 
						if (looking.Contains (input))
							looking.Remove (input);
					}
				} else {
					sucess = true;
				}

				if (sucess) { //move on to next one
					//we've reached the front, we're done!
					if (current.parents.Count == 0)
						return true;
					path.Push (current.parents [0]);
					indices.Push (procIndex);
				} else { //pop stuff off until we're cool
					while (true) {
						current = path.Pop (); //this one is no good
						var index = indices.Pop ();
						if (path.Count == 0)
							break;
						var child = path.Peek ();
						int newParentIndex = child.parents.IndexOf (current) + 1;
						if (newParentIndex == 0)
							break;
						if (newParentIndex < child.parents.Count) {
							path.Push (child.parents [newParentIndex]);
							indices.Push (index);
							break;
						}
					}
				}
			}
			return false;
		}

		int ReadTerm(string _combo, int start, out string term)
		{
			char[] operators = new char[]{',', '+', '|','(',')'};
			if (operators.Contains (_combo [start])) {
				term = _combo [start].ToString();
				return 1;
			}
			var temp = _combo.Skip (start).TakeWhile (e => !operators.Contains (e));
			term = new string (temp.Where (e => !char.IsWhiteSpace (e)).ToArray ());
			return temp.Count ();
		}
			
		InputCode ParseInputCode(string codeString)
		{
			InputCode r;
			//Debug.Log (codeString[0]);
			if (codeString[0] == '~') {
				r = (InputCode)Enum.Parse (typeof(InputCode), codeString.Substring (1));
				r = (InputCode)((byte)r + CSP.Input.sReleaseOffset); //offset
			} else {
				r = (InputCode)Enum.Parse (typeof(InputCode), codeString);
			}
			return r;
		}
		public NewInputCombo(string _combo)
		{
			Regex options = new Regex("\\[(.*?)\\]");
			var match = options.Match(_combo);
			while(match.Success){
				string[] value = match.Value.Substring (1, match.Length - 2).Split (new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);	
				if (value.Length == 0) {
					//TODO warning empty option
				} else if (Regex.IsMatch (value [0], @"^\d+$")) {
					lookback = Convert.ToInt32 (value [0]);
				} else if (value [0] == "held") {
					held = ParseInputCode (value [1]);
				}
				match = match.NextMatch ();
			}
			_combo = options.Replace(_combo,"");

			//on combo special case
			if (_combo.All (char.IsWhiteSpace)) {
				combo = null;
				return;
			}
				

			//TODO replace _360, QCF, QCB, HCF, HCB, DP
			//TODO add !<input> and wait#
			//TODO add timing options!

			string[] precedence = new string[]{ ",", "+", "|"};
			char[] charPrecedence = new char[]{ ',', '+', '|'};

			Stack<string> infixStack = new Stack<string> ();
			Queue<string> rpn = new Queue<string> ();
			int index = 0;

			//construct Reverse Polish Notation
			do {
				string nextTerm;
				index += ReadTerm (_combo, index, out nextTerm);
				if (precedence.Contains (nextTerm)) { //if operator
					int p = Array.IndexOf<string> (precedence, nextTerm);
					while (infixStack.Count > 0 && Array.IndexOf<string> (precedence, infixStack.Peek ()) >= p) {
						rpn.Enqueue (infixStack.Pop ());
					}
					infixStack.Push (nextTerm);
				} else if (nextTerm == "(") { 
					infixStack.Push(nextTerm);
				} else if (nextTerm == ")") { 
					//TODO better error handling here
					while (infixStack.Peek () != "(") {
						rpn.Enqueue (infixStack.Pop ());
					}
					infixStack.Pop (); //remove the brace
				} else { 
					rpn.Enqueue (nextTerm);
				}
			} while (index < _combo.Length);


			while (infixStack.Count > 0) {
				var push = infixStack.Pop ();
				if (push == "(")
					throw new Exception ("invalid combo string");
				rpn.Enqueue (push);
			}

			//construct graph out of rpn
			Stack<Option> rpnStack = new Stack<Option>();
			while (rpn.Count > 0) {
				string op = rpn.Dequeue ();
				Option snd;
				Option fst;
				switch (op) {
				case ",":
					snd = rpnStack.Pop ();
					fst = rpnStack.Pop ();
					foreach (var e in fst.Tails()) {
						foreach (var f in snd.Heads()) {
							f.parents.Add (e);
							e.followups.Add (f);
						}
					}
					rpnStack.Push (fst);
					break;
				case "+":
					snd = rpnStack.Pop ();
					fst = rpnStack.Pop ();
					if (snd.Depth() != 1)
						throw new Exception ("adding deep options on rhs not allowed");
					foreach (var e in fst.Tails()) {
						if (!snd.IsDummy ()) { //if not dummy, add inputs
							e.input.AddRange (snd.input);
						} else { //otherwise add inputs of its followups
							if (e.parents.Count == 0) { //create a dummy parent if we're a root node, 
								Option dummyParent = new Option ();
								dummyParent.followups.Add (e);
								e.parents.Add (dummyParent);
								fst = dummyParent;
							}
							foreach (var f in e.parents) //disconnect this option
								f.followups.Remove (e);
							foreach (var f in snd.followups) { //create new options for each followup
								Option addCombined = new Option ();
								addCombined.input.AddRange (e.input);
								addCombined.input.AddRange (f.input);
								foreach(var g in e.parents){
									addCombined.parents.Add(g);
									g.followups.Add(addCombined);
								}
							}
						}
					}
					rpnStack.Push (fst);
					break;
				case "|":
					snd = rpnStack.Pop ();
					fst = rpnStack.Pop ();
					Option combined = new Option ();
					foreach (var e in fst.Heads().Concat(snd.Heads())) {
						e.parents.Add (combined);
						combined.followups.Add (e);
					}
					rpnStack.Push (combined);
					break;
				default:
					Option push = new Option ();
					CSP.Input input = new CSP.Input ();
					input.code = ParseInputCode (op);
					push.input.Add(input);
					rpnStack.Push(push);
					break;
				};
			}
			if (rpnStack.Count != 1)
				throw new Exception ("combo string formatting exception");
			combo = rpnStack.Pop ();
		}
		
	}

}