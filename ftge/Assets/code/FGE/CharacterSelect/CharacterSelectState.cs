﻿using System;
using System.Collections.Generic;
using System.Linq;
using CSP;

namespace FGE{

	public class CharSelectState : IPlayState
	{
		//shallow data
		public Action<string,string> OnGameStart { get; set; }
		public Dictionary<string, AnimationEntityData> entityData = new Dictionary<string, AnimationEntityData>();

		//deep data
		public int CurrentFrame{ get; protected set; }
		public int StartFrame{ get; protected set; }
		public string[] characters;
		public int p1Char, p2Char;
		public bool p1Select = false;
		public bool p2Select = false;
		public List<Entity> entities = new List<Entity>();
		Entity p1Preview, p2Preview;
		public IEnumerable<Entity> AllEntities { get { return (new Entity[]{ p1Preview, p2Preview }).Where(e=>e!=null).Concat (entities); } }

		public IPlayState Copy(){
			CharSelectState r = new CharSelectState ();
			r.characters = characters;
			r.p1Char = p1Char;
			r.p2Char = p2Char;
			r.OnGameStart = OnGameStart;
			r.entities = entities.Select (e => e.Clone () as Entity).ToList ();
			r.p1Preview = p1Preview;
			r.p2Preview = p2Preview;

			r.entityData = entityData;
			return r;
		}

		public void Initialize(List<AssetGroup> characterBundles, CharacterResourceInfo[] previewInfo)
		{
			foreach (var e in characterBundles) {
				UnityEngine.Debug.Log ("loaded character bundle " + e.BundleName);
				var data = AnimationEntityData.Create (e,e.BundleName);
				entityData [e.BundleName] = data;
				Entity ent = new Entity (data);
				var fs = ent.GetSequenceReadOnly ("PORTRAIT");
				if (fs == null)
					throw new Exception (e.BundleName + " bundle has no portrait");
				ent.SetFrameSequence ("PORTRAIT");
				entities.Add (ent);

				//TODO queque in preview info for preview...
			}

			float centerY = 0;
			float centerX = 0;
			float width = 150;
			int count = entities.Count;
			for (int i = 0; i < entities.Count; i++) {
				var npos = new Point(centerX + width * (i - count / 2f),centerY);
				entities [i].Transform.pos = npos;
			}
		}

		public void CreatePreviews()
		{
			//TODO	
		}

		public void Update(InputHistory inputs)
		{
			//TODO this will miss inputs from opponent because InputHistory always has inputs from the future...
			var currentInputs = inputs.GetInputsOnFrame (CurrentFrame).Value.inputs;
			bool p1Change = false;
			bool p2Change = false;
			foreach (var e in currentInputs) {
				if (e.input.code == InputCode.r) {
					p1Char += 1;
					p1Select = false; 
					p1Change = true;
				} else if (e.input.code == InputCode.l) {
					p1Char -= 1;
					p1Select = false;
					p1Change = true;
				} else if (e.input.code == CSP.Input.Convert(InputCode.r,2)) {
					p2Char += 1;
					p2Select = false;
					p2Change = true;
				} else if (e.input.code == CSP.Input.Convert(InputCode.l,2)) {
					p2Char -= 1;
					p2Change = true;
				}
			}
			p1Char = (p1Char+entities.Count)%entities.Count;
			p2Char = (p2Char + entities.Count) % entities.Count;

			//TODO highlight selected character somehow...

			//TODO move this to create previews
			if(p1Change){
				p1Preview = (Entity)entities [p1Char].Clone ();
				var fs = p1Preview.GetSequenceReadOnly ("PREVIEW");
				if (fs == null)
					throw new Exception ("boop");
				p1Preview.SetFrameSequence("PREVIEW");
			}

			if(p2Change){
				p2Preview = (Entity)entities [p2Char].Clone ();
				var fs = p2Preview.GetSequenceReadOnly ("PREVIEW");
				if (fs == null)
					throw new Exception ("boop");
				p2Preview.SetFrameSequence("PREVIEW");
			}

			foreach (var e in currentInputs) {
				if (e.input.code == InputCode.A) {
					p1Select = true;
				} else if (e.input.code == CSP.Input.Convert(InputCode.A,2)) {
					p2Select = true;
				}
			}

			if (p1Select && p2Select) {
				//TODO maybe this should be character name instead of index...
				//and by character name, I mean converted into the characters, not bundle preview names
				OnGameStart (entities [p1Char].Name,entities [p2Char].Name);
				//TODO outro animation...
			}

			CurrentFrame++;
		}

		public float CurrentStateImportance()
		{
			return 0;
		}

		public static Entity CreateSingleImageEntity()
		{
			//TODO
			return null;
		}

	}
}