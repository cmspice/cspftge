﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FGE
{
	[Serializable]
	public struct Point
	{
		public static readonly Point zero = new Point(0,0);
		public static readonly Point one = new Point(1,1);
		[Newtonsoft.Json.JsonIgnore]
		public float Max{ get { return x > y ? x : y; } }
		public float x, y;
		public Point(float aX, float aY)
		{
			x = aX;
			y = aY;
		}
		public float this[int index]{
			get{
				if(index ==0) return x; else if(index == 1) return y; else throw new IndexOutOfRangeException();
			}
			set{
				if(index ==0) x = value; else if(index == 1) y = value; else throw new IndexOutOfRangeException();
			}
		}
		public override string ToString(){return "(" + x + ", " + y + ")";}

		public static Point Parse(string aString)
		{
			var vals = aString.Replace("(", "").Replace(")", "").Split(',');
			if (vals.Length == 2)
			{
				Point r = new Point();
				r.x = Convert.ToSingle(vals[0]);
				r.y = Convert.ToSingle(vals[1]);
				return r;
			} else
				throw new Exception("Bad point format exception");
		}

		public Point ComponentWiseMultiply(Point p){return new Point(p.x*x,p.y*y);}
		public static Point operator +(Point p1, Point p2) { return new Point(p1.x + p2.x, p1.y + p2.y); }
		public static Point operator -(Point p1) { return new Point(-p1.x, -p1.y); }
		public static Point operator -(Point p1, Point p2) { return p1 + -p2; }
		public static Point operator *(Point p, float s){return new Point(p.x * s, p.y * s);}
		public static bool operator ==(Point o1, Point o2) { return Object.Equals(o1, o2); }
		public static bool operator !=(Point o1, Point o2) { return !(o1 == o2); }
		public override bool Equals(object comparand) { return this.ToString().Equals(comparand.ToString()); }

		//this is bad but whateever
		public override int GetHashCode ()
		{
			int prime = 31;
			int result = 1;
			result = prime * result + x.GetHashCode();
			result = prime * result + y.GetHashCode();
			return (int)result;
		}
	}

	[Serializable]
	public struct Rectangle{
		[Newtonsoft.Json.JsonIgnore]
		public Point LDCorner{ get { return corner; } }
		[Newtonsoft.Json.JsonIgnore]
		public Point RUCorner{ get { return corner+size; } }
		[Newtonsoft.Json.JsonIgnore]
		public Point Center{ get { return corner + size*0.5f; } }

		public Point corner,size;

		public Rectangle(Point aCorner, Point aSize)
		{
			corner = aCorner;
			size = aSize;
		}

	
		public bool ContainsBox(Rectangle aBox)
		{
			return ContainsPoint(aBox.LDCorner) && ContainsPoint(aBox.RUCorner);
		}

		public bool ContainsPoint(Point aPosition)
		{
			return 
				(aPosition.x >= corner.x) &&
				(aPosition.y >= corner.y) &&
				(aPosition.x < corner.x + size.x) &&
				(aPosition.y < corner.y + size.y);
		}

		public Rectangle ReflectHorizontal(float x){
			Rectangle r = this;
			r.corner.x = x - (corner.x + size.x);
			return r;
		}

		//TODO test
		public static Rectangle Union(Rectangle a, Rectangle b)
		{
			Rectangle r = new Rectangle ();
			float x1 = Math.Min(a.LDCorner.x, b.LDCorner.x);
			float x2 = Math.Max(a.RUCorner.x, b.RUCorner.x); 
			float y1 = Math.Min(a.LDCorner.y,b.LDCorner.y);
			float y2 = Math.Max(a.RUCorner.y,b.RUCorner.y); 

			return new Rectangle(new Point(x1, y1), new Point(x2 - x1, y2 - y1));
		}

		//TODO check if this function acutally works
		public static Rectangle? Intersect(Rectangle a, Rectangle b) {
			float x1 = Math.Max(a.LDCorner.x, b.LDCorner.x);
			float x2 = Math.Min(a.RUCorner.x, b.RUCorner.x); 
			float y1 = Math.Max(a.LDCorner.y,b.LDCorner.y);
			float y2 = Math.Min(a.RUCorner.y,b.RUCorner.y); 

			if (x2 >= x1
				&& y2 >= y1) { 

				return new Rectangle(new Point(x1, y1), new Point(x2 - x1, y2 - y1));
			}
			return null;
		}
			

		public override string ToString(){return corner.ToString() + " " + size.ToString();}
		public static bool operator ==(Rectangle o1, Rectangle o2) { return Object.Equals(o1, o2); }
		public static bool operator !=(Rectangle o1, Rectangle o2) { return !(o1 == o2); }
		public static Rectangle operator +(Rectangle r, Point p) { return new Rectangle(r.corner + p,r.size); }
		public static Rectangle operator -(Rectangle r, Point p) { return new Rectangle(r.corner - p,r.size); }
		public override bool Equals(object comparand) { return this.ToString().Equals(comparand.ToString()); }

		public override int GetHashCode ()
		{
			//TODO
			return base.GetHashCode ();
		}

	}


}