﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;


namespace FGE
{
	public static class DefaultProjectileCallbacks
	{
		public static void OnHitEntity(PlayState state, Entity A, Entity B, HitboxCollisions hit)
		{
			//TODO this should really just queue into the "hit" sequence which should destroy itself at end of sequence
			//Debug.Log ("destroying " + A.Name);

			//do not destroy if hitting with the player who spawned the projectile
			if (B is Player)
				if (A.UserData.Get<int> ("spawn_player") == (B as Player).id)
					return;


			state.DestroyEntity (A);
		}

		public static void OnHitEnvironment(PlayState state, Entity entity, Rectangle environment, Point p)
		{
			//TODO this should really just queue into the "hit" sequence which should destroy itself at end of sequence
			//Debug.Log ("destroying " + entity.Name);
			state.DestroyEntity (entity);
		}
	}


	public static class DefaultPlayerCallbacks
	{


		class OverridePair
		{
			public int priority;
			public NewInputCombo combo;
		}
		//reads input on this frame, and decides if we should transition or not
		//returns new animation or null if no change
		public static string OnInput(PlayState state, Entity entity, CSP.InputHistory inputs)
		{

			Dictionary<FrameSequence,OverridePair> cancels = new Dictionary<FrameSequence, OverridePair> ();
			var sequences = entity.GetAllSequencesReadOnly ();
			var cancelStrings = entity.GetCurrentFrameReadonly ().staticParameters.cancels;
			if(cancelStrings == null)
				return null;
			foreach (var e in cancelStrings) {
				foreach (var f in sequences.Where(g=>e.DoesCancelInto(g))) {
					OverridePair addMe = new OverridePair();
					addMe.priority = e.overridePriority == null ? f.priority : e.overridePriority.Value;
					addMe.combo = e.OverrideCombo == null ? f.combo : e.OverrideCombo;
					cancels.Add (f,addMe);
				}
			}
			int player = (entity is Player) ? ((Player)entity).id : 0;
			foreach (var f in cancels.OrderBy(e=>-e.Value.priority)) {
				//Debug.Log (f.Key.name);
				if (f.Value.combo.CheckCombo (state.CurrentFrame, inputs,state.Keystates,player,entity.Transform.backwards)) {
					return f.Key.name;
				}
			}
			return null;
		}

		//this is called after a new frame has been set
		public static void OnFrame(PlayState state, Entity entity)
		{
			var seq = entity.GetCurrentSequenceReadOnly ();
			var frame = entity.GetCurrentFrameReadonly ();
			float dirModifier = entity.Transform.backwards ? -1 : 1;

			Point gravity = entity.EntityMetadata.gravity.HasValue ? entity.EntityMetadata.gravity.Value : FGEConstants.gravity;
			float airFriction = entity.EntityMetadata.airFriction.HasValue ? entity.EntityMetadata.airFriction.Value : FGEConstants.airFriction;
			float groundFriction = entity.EntityMetadata.groundFriction.HasValue ? entity.EntityMetadata.groundFriction.Value : FGEConstants.groundFriction;


			//velocity
			var velocity = entity.Velocity;

			//TODO decide if on air or ground and do different frictions / gravity
			if (seq.tags.Contains ("AIR")) {
				velocity = velocity * (1 - airFriction);
			} else {
				velocity = velocity * (1 - groundFriction);
			}
			velocity += gravity;
				

			if (frame.staticParameters.dvx != null) {
				velocity.x += frame.staticParameters.dvx.Value * dirModifier;
			}
			if (frame.staticParameters.dvy != null) {
				velocity.y += frame.staticParameters.dvy.Value;
			}
			if (frame.staticParameters.resetvx != null) {
				velocity.x = frame.staticParameters.resetvx.Value * dirModifier;
			}
			if (frame.staticParameters.resetvy != null) {
				velocity.y = frame.staticParameters.resetvy.Value;
			}
			entity.Velocity = velocity;
		
			//position
			var position = entity.Transform.pos;
			position.x += velocity.x;
			position.y += velocity.y;
			if (frame.staticParameters.dx != null) {
				position.x += frame.staticParameters.dx.Value * dirModifier;
			}
			if (frame.staticParameters.dy != null) {
				position.y += frame.staticParameters.dy.Value;
			}
			entity.Transform.pos = position;

			if(entity is Player){
				//var sequence = entity.GetCurrentSequenceReadOnly ();
				//if (sequence.name == "IDLE" || sequence.name == "CROUCHING") {
				if(frame.staticParameters.allowFaceChange.HasValue && frame.staticParameters.allowFaceChange.Value){
					Player thisPlayer = (entity as Player);
					Player otherPlayer = thisPlayer.id == state.Player1.id ? state.Player2 : state.Player1;
					if (thisPlayer.Transform.pos.x < otherPlayer.Transform.pos.x) {
						thisPlayer.Transform.backwards = false;
					} else{
						thisPlayer.Transform.backwards = true;
					}
				}
			}

			//spawn stuff
			if (frame.staticParameters.spawnList != null) {
				foreach (var e in frame.staticParameters.spawnList) {
					var start = new SequenceFramePair (e.startingSequence);
					Entity spawn = new Entity (state.shallowData.EntityData [e.name]);
					EntityTransform2d t = spawn.Transform;
					if (e.startingPositionRelativeToPlayer.HasValue) {
						t.pos = entity.Transform.TransformPoint (e.startingPositionRelativeToPlayer.Value);
						t.backwards = entity.Transform.backwards;
					} else if (e.startingPositionRelativeToStage.HasValue) {
						t.pos = e.startingPositionRelativeToStage.Value; //stage is always at 0,0 lets say...
					}
					if (entity is Player)
						spawn.UserData.Set<int> ("spawn_player", (entity as Player).id);
					else if (entity.UserData.ContainsKey("spawn_player")) //spawn spawning a spawn, proporgate owner
						spawn.UserData.Set<int> ("spawn_player",entity.UserData.Get<int>("spawn_player"));
					state.AddEntity (spawn);
				}
			}
		}




		//this is called when two entities collide
		public static void OnHitEntity(PlayState state, Entity A, Entity B, HitboxCollisions hit){
			//TODO check hitbox types and decide what situation we are in
			//set to ishit or isgrab type animiation (hardcoded I guess)
			//set duration for block/hit stun
			//set knockback velocity for hit/stun
			//
			//set pause frames???

			if (B is Player) {
				OnHitPlayer (state, A, B as Player, hit);
			}
			if (A is Player) {
				OnHitPlayer (state, B, A as Player, hit.Flipped());
			} 
		}

		public static void OnHitPlayer(PlayState state, Entity A, Player B, HitboxCollisions hit){
			var Aprop = A.GetCurrentFrameReadonly ().staticParameters;
			var Bprop = B.GetCurrentFrameReadonly ().staticParameters;
			//A hits B
			bool Aattack = ((hit.maska & HitboxMask.HIT) != 0) || ((hit.maska & HitboxMask.PROJECTILE) != 0);
			bool bwasattack = (hit.maskb & HitboxMask.CANHIT) != 0;
			if (Aattack && bwasattack) {
				if (Aprop.hitType != null) {
					//var fs = A.GetCurrentSequenceReadOnly();
					var frame = A.GetCurrentFrameReadonly ();
					int attackId = frame.staticParameters.id == null ? 0 : frame.staticParameters.id.Value;
					if(!A.UserData.ContainsKey("last attack id") || A.UserData.Get<int>("last attack id") != attackId){
						A.UserData.Set<int> ("last attack id", attackId);


						int? lastOD = B.OverrideDuration;

						//TODO BLOCKING
						//hit
						//TODO decide which hit animation to play based on state of B
						//TODO check tags if(fs.tags.Contains("GROUND"))
						B.SetNextFrameSequence ("hit_stand");
						//B.SetNextFrameSequence ("hit_hard");
						int addOD = frame.staticParameters.hitStun.HasValue ? frame.staticParameters.hitStun.Value : 20;
						//TODO if lastOD was from block, do not add 
						B.OverrideDuration = (lastOD.HasValue ? lastOD.Value : 0) + addOD;

						//TODO damage

						state.PauseFrames = frame.staticParameters.hitPause.HasValue ? frame.staticParameters.hitPause.Value : 0;
					
					}
				}

			}
		}
			
		//this is called when a player hits ground or wall
		//TODO make different versions for Player and for fireball or whatever.
		public static void OnHitEnvironment(PlayState state, Entity entity, Rectangle environment, Point p)
		{
			//CAN DELETE
			/*
			var newPos = entity.Position;
			newPos.x -= p.x;
			newPos.y -= p.y;
			entity.Position = newPos;
			*/

			//TODO check for bounce property :D
			//reset velocity
			var newVel = entity.Velocity;
			newVel.x = p.x == 0 ? newVel.x : 0;
			newVel.y = p.y == 0 ? newVel.y : 0;
			entity.Velocity = newVel;

			if (entity is Player) {
				var player = entity as Player;
				var seq = player.GetCurrentSequenceReadOnly ();
				//if we hit the ground
				if (p.y < 0) { 
					if(seq.name == "hit_air" || seq.name == "knockdown" || seq.name == "hard_hit"){
						player.TrySetSequenceByName ("down");
					} else if (seq.tags.Contains ("AIR")) {
						player.TrySetSequenceByName ("standing");
					}
				}
				//if we hit the wall
				if (p.x != 0) {

				}
			} 

			//TODO destroy fireballs
			//state.DestroyEntity(entity);

		}

		public static void OnFrameSequenceEnd(PlayState state, Entity entity)
		{
			//DebugConsole.Log ("END " + entity.CurrentStartOfOnFrameState.sequence);

			entity.UserData.Remove ("last attack id");
		}

		public static void OnFrameSequenceBegin(PlayState state, Entity entity)
		{
			

			//entity.CurrentState - FrameSequence we are beginning
			//entity.CurrentStartOfOnFrameState - the FrameSequence we just finished
			//entity.LastStartOfOnFrameState - the state before entity.CurrentStartOfOnFrameState

			//DebugConsole.Log ("BEGIN " + entity.CurrentState.sequence);
		}
	}
}