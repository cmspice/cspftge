﻿using System.Collections.Generic;
using System;
using System.Linq;

namespace FGE
{
	public enum HitboxMask : int{
		ENVIRONMENT = 	1 << 0,	//this is kinda weird since we also hard code environment
		BODY = 			1 << 1,
		CANHIT = 		1 << 2,
		HIT = 			1 << 3,
		PROJECTILE = 	1 << 4,
		//specific stuff
		CANGRAB =		1 << 5,
		GRAB = 			1 << 6,
	}

	public struct Hitbox{
		public string name; //this is used in the sprite editor and no where else
		public HitboxMask mask;
		public Rectangle box;

		//this is symmetrical meaning {0,1} is also {1,0}
		//NOTE, this also determines order in which collisions are resolved
		public static List<HitboxMask[]> sDefaultCollisionMask = new List<HitboxMask[]>() {
			new HitboxMask[] {HitboxMask.ENVIRONMENT,HitboxMask.BODY},
			new HitboxMask[] {HitboxMask.BODY,HitboxMask.BODY},
			new HitboxMask[] {HitboxMask.HIT,HitboxMask.CANHIT},
			new HitboxMask[] {HitboxMask.PROJECTILE,HitboxMask.CANHIT},
			new HitboxMask[] {HitboxMask.GRAB,HitboxMask.CANGRAB},
		};
			
	}

	public struct HitboxCollisions
	{
		public HitboxMask maska;
		public HitboxMask maskb;
		public Rectangle? collision;

		public HitboxCollisions Flipped(){
			HitboxCollisions r = new HitboxCollisions ();
			r.maska = maskb;
			r.maskb = maska;
			r.collision = collision;
			return r;
		}
	}

	//TODO convert to struct
	public class HitboxGroup : ICloneable
	{
		public List<Hitbox> hitboxes = new List<Hitbox>();

		public object Clone()
		{
			HitboxGroup r = new HitboxGroup ();
			r.hitboxes = hitboxes.ToList ();
			return r;
		}

		public static bool CollideEnvironment(out float left, out float right, out float bottom, out float top, Rectangle environment, HitboxGroup A, HitboxMask mask)
		{
			left = right = bottom = top = 0;
			foreach (var e in A.hitboxes) {
				var box = e.box;
				//var box = reflectA ? e.box.ReflectHorizontal (0) : e.box;
				//box = box + offsetA;
				if ((e.mask & mask) != 0) {
					if (box.corner.x < environment.corner.x)
						left = Math.Max (left, environment.corner.x - box.corner.x);
					if (box.corner.x + box.size.x > environment.corner.x + environment.size.x)
						right = Math.Max (right, box.corner.x + box.size.x - (environment.corner.x + environment.size.x));
					if (box.corner.y < environment.corner.y)
						bottom = Math.Max (bottom, environment.corner.y - box.corner.y);
					if (box.corner.y + box.size.y > environment.corner.y + environment.size.y)
						top = Math.Max (top, box.corner.y + box.size.y - (environment.corner.y + environment.size.y));
				}
			}
			return !(left == 0 && right == 0 && bottom == 0 && top == 0);
		}

		//TODO move these functions elsewhere
		public static List<HitboxCollisions> CollideAll(List<HitboxMask[]> masks, HitboxGroup A, HitboxGroup B)
		{
			List<HitboxCollisions> r = new List<HitboxCollisions> ();
			var sym = masks.Concat (masks.Where(e=>e[0] != e[1]).Select(e => new HitboxMask[] {e [1],e [0]}));
			foreach (var e in sym) {
				//var resultab = Collide (e [0], e [1], A, B, offsetA, offsetB,reflectA,reflectB);
				var resultab = Collide (e [0], e [1], A, B);
				if (resultab == null)
					continue;
				//var resultba = e [0] != e [1] ? Collide (e [1], e [0], A, B, offsetA, offsetB,reflectA,reflectB) : resultab;
				var resultba = e [0] != e [1] ? Collide (e [1], e [0], A, B) : resultab;
				HitboxCollisions add = new HitboxCollisions ();
				add.collision = resultab;
				add.maska = e [0];
				add.maskb = e [1];
				r.Add (add);
			}
			return r;
		}

		//TODO test this, I feel like I probably messed this function up
		static Rectangle? Collide(HitboxMask maska, HitboxMask maskb, HitboxGroup A, HitboxGroup B)
		{
			Rectangle? r = null;
			foreach(var a in A.hitboxes){
				foreach (var b in B.hitboxes){
					if ((a.mask & maska) != 0 && (b.mask & maskb) != 0) { //if masks align
						//var abox = reflectA ? a.box.ReflectHorizontal (0) : a.box;
						//abox = abox + offsetA;
						//var bbox = reflectB ? b.box.ReflectHorizontal (0) : b.box;
						//bbox = bbox + offsetB;
						var intersect = Rectangle.Intersect (a.box, b.box);
						if (intersect != null) {
							if (r == null)
								r = intersect;
							else
								r = Rectangle.Union (r.Value, intersect.Value);
						}
					}
				}
			}
			return r;
		}
	}

}