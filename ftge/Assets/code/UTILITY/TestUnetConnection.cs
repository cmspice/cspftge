﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Linq;


public class TestUnetConnection : MonoBehaviour {
	CSP.NetworkManager nm;
	string ip = "127.0.0.1";
	public void Start()
	{
		nm = new CSP.NetworkManager ();

		nm.OnReceiveTest += OnReceiveTest;

		/* just making sure structs do what I think they do
		CSP.TimedInput ti1 = new CSP.TimedInput (CSP.InputCode.SPACE, new CSP.Timestamp (100));
		CSP.TimedInput ti2 = new CSP.TimedInput (CSP.InputCode.SPACE, new CSP.Timestamp (200));
		Debug.Log (ti1.timestamp.frame);
		Debug.Log (ti2.timestamp.frame);
		ti2 = ti1;
		ti1.timestamp.frame = 300;
		Debug.Log (ti1.timestamp.frame);
		Debug.Log (ti2.timestamp.frame);*/
	}

	public void Update()
	{
		nm.ReceiveOnce ();	
	}

	public void OnReceiveTest(int cid, byte[] msg)
	{
		DebugConsole.Log (System.BitConverter.ToInt32 (msg,0).ToString ());
	}


	string id = "0";
	public void OnGUI()
	{
		


		int buttonHeight = 50;
		int buttonWidth = 200;
		int buttonX = Screen.height - buttonWidth / 2;
		int buttonY = Screen.height / 2 - 200;

		ip = GUI.TextArea (new Rect (buttonX, buttonY, buttonWidth, buttonHeight), ip);
		buttonY += buttonHeight + 10;

		id = GUI.TextArea (new Rect (buttonX, buttonY, buttonWidth, buttonHeight), id);
		buttonY += buttonHeight + 10;

		if(GUI.Button(new Rect(buttonX, buttonY, buttonWidth, buttonHeight),"connect")){
			nm.Connect (ip,8888);
		}
		buttonY += buttonHeight + 10;

		int connection;
		try{
			connection = System.Convert.ToInt32 (id);
		} catch {
			return;
		}

		if (GUI.Button (new Rect (buttonX, buttonY, buttonWidth, buttonHeight), "test network timestamp")) {
			DebugConsole.Log ("GetNetworkTimestamp() = " +NetworkTransport.GetNetworkTimestamp ().ToString());
			nm.SendTimestamp (connection);
		}
		buttonY += buttonHeight + 10;

		if (GUI.Button (new Rect (buttonX, buttonY, buttonWidth, buttonHeight), "test RTT")) {
			DebugConsole.Log ("Ping self = " + nm.Ping(connection));
		}
		buttonY += buttonHeight + 10;

		if (GUI.Button (new Rect (buttonX, buttonY, buttonWidth, buttonHeight), "send garbage")) {
			nm.Send (connection, System.BitConverter.GetBytes (27), CSP.NetworkMessageType.SENDTEST, true);
		}

	}
}
