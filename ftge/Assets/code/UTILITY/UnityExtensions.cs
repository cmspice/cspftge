﻿using UnityEngine;

public static class RectExtensions
{
	public static Rect FromCorners(Vector2 a, Vector2 b)
	{
		Rect r = new Rect ();
		r.x = Mathf.Min (a.x, b.x);
		r.y = Mathf.Min (a.y, b.y);
		r.width = Mathf.Abs (a.x - b.x);
		r.height = Mathf.Abs (a.y - b.y);
		return r;
	}


}
