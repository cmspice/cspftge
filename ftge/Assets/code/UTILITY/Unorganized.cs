﻿
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;

public static class Unorganized{
	public static bool IsDefault<T>(this T value) where T : struct
	{
		bool isDefault = value.Equals(default(T));

		return isDefault;
	}
	public static T DeepClone<T>(T obj)
	{
		using (var ms = new MemoryStream())
		{
			var formatter = new BinaryFormatter();
			formatter.Serialize(ms, obj);
			ms.Position = 0;

			return (T) formatter.Deserialize(ms);
		}
	}

	//IDictionary extensions for type safe dictionaries reference types
	public static T Get<T>(this IDictionary<string, object> dictionary, string key)
	{
		return (T)dictionary[key];
	}

	public static bool TryGet<T>(this IDictionary<string, object> dictionary,
		string key, out T value)
	{
		object result;
		if (dictionary.TryGetValue(key, out result) && result is T) {
			value = (T)result;
			return true;
		}
		value = default(T);
		return false;
	}

	public static void Set(this IDictionary<string, object> dictionary,
		string key, object value)
	{
		dictionary[key] = value;
	}


	//naughty functions
	//these do no error checking
	public static T GetPrivateFieldValue<T,S>(this S owner, string field)
	{ 
		FieldInfo fi = typeof(S).GetField(field, BindingFlags.NonPublic | BindingFlags.Instance);
		return (T)fi.GetValue (owner);
	}
	public static void SetPrivateFieldValue<T,S>(this S owner, string field, T value)
	{ 
		FieldInfo fi = typeof(S).GetField(field, BindingFlags.NonPublic | BindingFlags.Instance);
		fi.SetValue (owner, value);
	}

	//Array extensions
	public static T[] SubArray<T>(this T[] data, int index, int length)
	{
		T[] result = new T[length];
		Array.Copy(data, index, result, 0, length);
		return result;
	}

	public static T[] CloneSubArray<T>(this T[] data, int index, int length)
		where T : ICloneable
	{
		T[] result = new T[length];
		for (int i = 0; i < length; i++) { 
			var original = data [index + i];
			if (original != null)
				result [i] = (T)original.Clone ();            
		}
		return result;
	}
}

public static class ListExt
{
	public static void AddSorted<T>(this List<T> @this, T item) where T: IComparable<T>
	{
		if (@this.Count == 0)
		{
			@this.Add(item);
			return;
		}
		if (@this[@this.Count-1].CompareTo(item) <= 0)
		{
			@this.Add(item);
			return;
		}
		if (@this[0].CompareTo(item) >= 0)
		{
			@this.Insert(0, item);
			return;
		}
		int index = @this.BinarySearch(item);
		if (index < 0) 
			index = ~index;
		@this.Insert(index, item);
	}
}