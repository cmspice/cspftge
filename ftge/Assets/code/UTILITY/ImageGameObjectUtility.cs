﻿using UnityEngine;
using System.Collections;

public class ImageGameObjectUtility  
{

	public static GameObject create(Texture aTex, Shader shader, float aPpu)
	{
		return (new ImageGameObjectUtility(aTex,aPpu,shader)).ParentObject;
	}

	public GameObject ParentObject { get; private set; }
	public GameObject PlaneObject { get; private set; }
	public Material PlaneMaterial { 
		get { 
			return PlaneObject.GetComponent<Renderer> ().material;
		} set { 
			var renderer = PlaneObject.GetComponent<Renderer> ();
			var oldTex = renderer.material.mainTexture;
			renderer.material = value;
			renderer.material.mainTexture = oldTex;
		} 
	}


	//doesn't it just make more sense to store this in pixel dimensions???
	//public Rect relCrop = new Rect(0,0,1,1);
	public Rect pixelCrop;
	//relative to BaseDimension
	public Rect Crop {
		get{
			//var d = BaseDimension;
			//return new Rect (relCrop.x * d.x, relCrop.y * d.y, relCrop.width * d.x, relCrop.y * d.y);
			return pixelCrop;
		}
		set{
			pixelCrop = value;
			var d = BaseDimension;
			var relCrop = new Rect (pixelCrop.x / d.x, pixelCrop.y / d.y, pixelCrop.width / d.x, pixelCrop.height / d.y);
			TextureOffset = relCrop.position;
			TextureScale = relCrop.size;
		}
	}

	public Vector2 TextureOffset {
		get{
			return PlaneObject.GetComponent<Renderer>().material.mainTextureOffset;
		}
		set{
			PlaneObject.GetComponent<Renderer>().material.mainTextureOffset = value;
		}
	}
	public Vector2 TextureScale {
		get{
			return PlaneObject.GetComponent<Renderer>().material.mainTextureScale;
		}
		set{
			PlaneObject.GetComponent<Renderer>().material.mainTextureScale = value;
		}
	}

	//this just stores the original dimensions
	//this would be the pixel dimensions of the texture

	public Vector2 BaseDimension { get; private set; }

	//set to a multiple of BaseDimension/Crop if you want want image not to stretch
	public Vector2 PixelDimension
	{
		get 
		{ 
			Vector3 planeScale = PlaneObject.transform.localScale;
			return new Vector2(planeScale.x, planeScale.y)*ppu; 
		}
		set
		{
			PlaneObject.transform.localScale = new Vector3 ((value.x), (value.y), 0) * (1.0f/ppu);
		}
	}

	float ppu = 1;
	public float PixelsPerUnit { 
		get {
			return ppu;
		} 
		set {
			var pixels = PixelDimension;
			ppu = value;
			PixelDimension = pixels;
		}
	}


	public ImageGameObjectUtility(Texture aTex, float aPpu, Shader shader)
	{
		ppu = aPpu;
		//TODO use a primitive from resource directory that has no MeshCollider
		ParentObject = new GameObject("genImageObjectParent");

		//PlaneObject = new GameObject ("genPlane");
		//PlaneObject.AddComponent<MeshFilter> ().mesh = CreatePlaneMesh ();
		PlaneObject = GameObject.CreatePrimitive (PrimitiveType.Quad);

		GameObject.Destroy (PlaneObject.GetComponent<Collider> ());

		//var renderer = PlaneObject.AddComponent<MeshRenderer> ();
		PlaneObject.GetComponent<Renderer>().material = new Material(shader);
		set_new_texture(aTex);

		//PlaneObject.transform.rotation = Quaternion.AngleAxis(90, Vector3.right) * PlaneObject.transform.rotation;
		PlaneObject.transform.parent = ParentObject.transform;
	}

	public void set_new_texture(Texture aTex)
	{
		if (aTex != null)
			BaseDimension = new Vector2(aTex.width, aTex.height);
		else
			BaseDimension = new Vector2(1, 1);
		PlaneObject.GetComponent<Renderer>().material.mainTexture = aTex;	
		pixelCrop = new Rect(Vector2.zero,BaseDimension);
		PixelDimension = BaseDimension;
	}

	public void destroy()
	{
		GameObject.Destroy(ParentObject);
		GameObject.Destroy(PlaneObject);
	}

	Mesh CreatePlaneMesh()
	{
		Mesh mesh = new Mesh();

		Vector3[] vertices = new Vector3[]
		{
			new Vector3( 0.5f, 0,  0.5f),
			new Vector3( 0.5f, 0, -0.5f),
			new Vector3(-0.5f, 0,  0.5f),
			new Vector3(-0.5f, 0, -0.5f),
		};

		Vector3[] normals = new Vector3[] {
			new Vector3(0,1,0),
			new Vector3(0,1,0),
			new Vector3(0,1,0),
			new Vector3(0,1,0),
		};

		Vector2[] uv = new Vector2[]
		{
			new Vector2(1, 1),
			new Vector2(1, 0),
			new Vector2(0, 1),
			new Vector2(0, 0),
		};

		int[] triangles = new int[]
		{
			0, 1, 2,
			2, 1, 3,
		};

		mesh.vertices = vertices;
		mesh.uv = uv;
		mesh.triangles = triangles;
		mesh.normals = normals;

		return mesh;
	}

}