﻿using System.Collections.Generic;

public class Logger 
{
	static Logger sInst = null;
	public static Logger Inst {
		get {
			if (sInst == null)
				sInst = new Logger ();
			return sInst;
		} 
	}

	public static void Log(string msg)
	{
		Logger.Inst.logs.Add (msg);
	}

	List<string> logs = new List<string> ();

}
