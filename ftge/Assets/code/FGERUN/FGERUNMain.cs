﻿using UnityEngine;
using System.Collections.Generic;

public class FGERUNMain : MonoBehaviour {
	public Shader defaultSpriteShader;
	public Shader debugShader;

	FGERUNMenu menu;
	FGERUNGame game = null;

	public CSP.NetworkManager network;
	public FGE.ResourceManager resourceManager;
	public FGE.EntityRenderManager renderManager;


	void Start () {
		Application.targetFrameRate = 60;
		menu = gameObject.AddComponent<FGERUNMenu> ();
		menu.IsMenu = false;

		resourceManager = new FGE.ResourceManager ();
		renderManager = new FGE.EntityRenderManager (defaultSpriteShader, resourceManager);
		renderManager.debugShader = debugShader;

		StartSingle ();
	}

	public void StartSingle()
	{
		game = new FGERUNGame (this);
		game.InitializeInputSources ();
		game.InitializePlay ();
		//game.InitializeCharacterSelect();
	}

	public void StartMultiplayer()
	{
		//TODO
	}

	void Update () {
		if (game != null) {
			game.Update ();
		}
	}

	void OnGUI()
	{
		if (game != null) {
			game.OnGUI_keyevents ();
		}

		//statistics here
		if (true) {
			if (game != null) {
				
				GUI.skin.label.fontSize = 20;

				int width = 200;
				int height = Screen.height;
				GUILayout.BeginArea (new Rect (Screen.width - width, 0, width, height));
				GUILayout.Label (game.gamerunner.Frames);
				GUILayout.EndArea ();
			}
		}
	}



	//for now just go straight into single or multiplayer
	//menu
		//settings
		//single player
		//multiplayer (+spectate)
		//replay

	//multiplayer lobby
		//TODO (integrate with unity lobby??)
		//would be pretty cool to write an HTTP lobby server

	//single player
		//create character select play state and hook up to CSP.DelayedGameController
		//set mode to char select (update controller, listen for game start events)
		//OnGameStart()
			//create main play state and hook up to CSP.DelayedGameController
			//set mode to play (updated controller, listen to game end events)	



	//SYNC
		//send sync event to opponent (handle case when both players send request simultaneously)
		//decide on starting network time (something like request received time + network delay * 2)
		

	//multiplayer
		//SYNC
			//create character select play state and hook up game controller
			//set mode to char select, (update controller, listen for game start events)
		//OnGameStart()
			//SYNC
				//create main play state and hook up game controller
				//sync network input, resimulate if necessary
				//call OnGameFinished() only when triggerd by lastConfirmedState
		//OnGameFinished()
			//no longer sync input
			//ending animation
				//end game screen (part of multiplayer lobby)

		


	//spectate
		//TODO attempt to spectate through proxy (to reduce server load on competing players)
		//request game state from client (either entire input list or last confirmed state + input since then)
			//mark time of response, start playback at this time or -5 seconds, whichever is older
			//if reconnecting, can request game state given last confirmed state (may send input, or may just try and recreate game), decide based on how long its been
		//this works exactly like mulitplayer except taking inputs from two networkinputsources

		


		
}
