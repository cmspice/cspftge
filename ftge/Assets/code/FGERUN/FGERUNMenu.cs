﻿using UnityEngine;
using System.Collections.Generic;

public class FGERUNMenu : MonoBehaviour {

	public bool IsMenu{ get; set; }

	public bool IsOption{get; private set;}

	public void OnGUI()
	{
		if (!IsMenu)
			return;


		int buttonHeight = 50;
		int buttonWidth = 200;
		int buttonX = Screen.height - buttonWidth / 2;
		int buttonY = Screen.height / 2 - 200;

		if (!IsOption) {
			if (GUI.Button (new Rect (buttonX, buttonY, buttonWidth, buttonHeight), "SINGLEPLAYER")) {
			}
			buttonY += buttonHeight + 10;
			if (GUI.Button (new Rect (buttonX, buttonY, buttonWidth, buttonHeight), "MULTIPLAYER")) {
			}
			buttonY += buttonHeight + 10;
			if (GUI.Button (new Rect (buttonX, buttonY, buttonWidth, buttonHeight), "OPTIONS")) {
				IsOption = true;
			}
			buttonY += buttonHeight + 10;
			if (GUI.Button (new Rect (buttonX, buttonY, buttonWidth, buttonHeight), "QUIT")) {
				Application.Quit ();
			}
			buttonY += buttonHeight + 10;
		} else {
			if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Escape) {
				IsOption = false;
			}
		}
	}
}
