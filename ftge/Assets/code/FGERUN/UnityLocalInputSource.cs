﻿using System.Collections.Generic;
using CSP;
using UnityEngine;

public class UnityLocalInputSource
{
	//TODO finish this if this is how you decide to interface with Unity Inputs
	public static Dictionary<UnityEngine.KeyCode,CSP.InputCode> sDefaultSinglePlayerMapping = new Dictionary<UnityEngine.KeyCode, InputCode>()
	{
		{KeyCode.UpArrow, InputCode.u},
		{KeyCode.DownArrow, InputCode.d},
		{KeyCode.LeftArrow, InputCode.l},
		{KeyCode.RightArrow, InputCode.r},

		{KeyCode.A, InputCode.D},
		{KeyCode.S, InputCode.E},
		{KeyCode.D, InputCode.F},
		{KeyCode.Z, InputCode.A},
		{KeyCode.X, InputCode.B},
		{KeyCode.C, InputCode.C},

		{KeyCode.Return, InputCode.Start},
		{KeyCode.Space, InputCode.Select},

		{KeyCode.JoystickButton5,InputCode.u},
		{KeyCode.JoystickButton6,InputCode.d},
		{KeyCode.JoystickButton7,InputCode.l},
		{KeyCode.JoystickButton8,InputCode.r},

		{KeyCode.JoystickButton9,InputCode.Start},
		{KeyCode.JoystickButton10,InputCode.Select},

		{KeyCode.JoystickButton16,InputCode.A},
		{KeyCode.JoystickButton17,InputCode.B},
		{KeyCode.JoystickButton18,InputCode.C},
		{KeyCode.JoystickButton19,InputCode.D},

		{KeyCode.JoystickButton13,InputCode.E},
		{KeyCode.JoystickButton14,InputCode.F},

		{KeyCode.I, (InputCode)((byte)InputCode.u+CSP.Input.sPlayerOffset)},
		{KeyCode.K, (InputCode)((byte)InputCode.d+CSP.Input.sPlayerOffset)},
		{KeyCode.J, (InputCode)((byte)InputCode.l+CSP.Input.sPlayerOffset)},
		{KeyCode.L, (InputCode)((byte)InputCode.r+CSP.Input.sPlayerOffset)},

		{KeyCode.RightShift, (InputCode)((byte)InputCode.A+CSP.Input.sPlayerOffset)},
	};

	public LocalInputSource source;
	Dictionary<UnityEngine.KeyCode,CSP.InputCode> mappings;

	HashSet<UnityEngine.KeyCode> downs = new HashSet<KeyCode>();

	public int player { get; private set; } //player 0 means multiple players on one machine

	public void Initialize(int _player, Dictionary<UnityEngine.KeyCode,CSP.InputCode> _mappings){
		source = new LocalInputSource ();
		player = _player;

		mappings = _mappings;

		/*mappings = new Dictionary<int, InputCode> ();
		var keycodes = System.Enum.GetValues(typeof(UnityEngine.KeyCode));
		foreach(UnityEngine.KeyCode val in keycodes) {
			mappings [(int)val] = InputCode.None;
		}*/
	}

	public void OnKeyChanged(KeyCode key, bool press)
	{
		var isKeyDown = downs.Contains (key);
		if (press && isKeyDown) //this is a repeat press, ignore it
			return;
		else if (press)
			downs.Add (key);
		else if (!press && !isKeyDown) //this is a repeat release, ignore it
			return;
		else if (!press)
			downs.Remove (key);
		
		byte shift = player == 0 ? (byte)0 : (byte)((player-1)*CSP.Input.sPlayerOffset);
		shift = (byte)(shift + (press ? (byte)0 : (byte)CSP.Input.sReleaseOffset));
		if (mappings.ContainsKey (key)) {
			var inputcode = (CSP.InputCode)((byte)mappings [key] + shift); 
			source.AddInput (new CSP.Input (inputcode));
			//DebugConsole.Log (CSP.Input.GetHumanReadable(inputcode));
		}
	}

	//TODO do I need special axis input methods??

	//TODO configuration to overrid emappings/mapping serialization
}
