﻿using UnityEngine;
using System.Collections;
using FGE;
using CSP;

public enum EGMode
{
	MAIN,
	SINGLE,
	MULTI,
}

public class EGMainMenu : MonoBehaviour {

	public EGGameData data;

	public bool autoStartSingle = false;

	EGMode mode = EGMode.MAIN;

	public bool IsOption{get; private set;}

	public void Awake()
	{
		data = gameObject.GetComponent<EGGameData> ();

	}

	public void Start()
	{
		if (autoStartSingle) {
			OnClickSinglePlayer ();
		}

		#if !UNITY_EDITOR
		OnClickMultiPlayer("");
		#endif

	}

	public void OnClickSinglePlayer()
	{
		mode = EGMode.SINGLE;
		var play = gameObject.AddComponent<EGPlayLocal> ();
		play.Initialize (data);
	}


	public void OnClickMultiPlayer(string ip)
	{
		mode = EGMode.MULTI;
		DebugConsole.Log ("starting multiplayer");
		data.network.CreateNetworking (System.Convert.ToInt32( hostPort));
		var play = gameObject.AddComponent<EGPlayMultiNetworkTest> ();
		play.Initialize (data);
		if (ip != "")
			play.Connect (ip, System.Convert.ToInt32(dstPort));

	}

	public void OnClickTraining()
	{
	}

	public void OnClickReplay()
	{
	}

	public void OnClickOptions()
	{
		//TODO
	}

	#if UNITY_EDITOR
	string ip = "127.0.0.1";
	//string ip = "192.168.1.100";
	string dstPort = "8888";
	string hostPort = "5000";
	#else
	string ip= "";
	string dstPort = "8888";
	string hostPort = "8888";
	#endif

	public void OnGUI()
	{
		if (mode == EGMode.MAIN) {
			int width = 600;
			GUILayout.BeginArea (new Rect ((Screen.width - width) / 2f, 0, width, Screen.height));
			if(GUILayout.Button("LOCAL VS")){
				OnClickSinglePlayer ();
			}
			GUILayout.BeginHorizontal ();
			ip = GUILayout.TextField (ip);
			dstPort = GUILayout.TextField (dstPort);
			if(GUILayout.Button("MULTIPLAYER")){
				OnClickMultiPlayer (ip);
			}
			hostPort = GUILayout.TextField (hostPort);
			GUILayout.EndHorizontal ();
			if(GUILayout.Button("OPTIONS")){
				IsOption = true;
			}
			if(GUILayout.Button("QUIT")){
				Application.Quit ();
			}
			GUILayout.EndArea ();
		}  


		//if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Escape) {
			//IsOption = false;
		//}
	}

}
