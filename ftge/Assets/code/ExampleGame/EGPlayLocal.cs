﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CSP;
using FGE;


public class EGPlayLocal : MonoBehaviour {
	
	public GameRunner gamerunner;
	IPlayState playState;
	UnityLocalInputSource local;

	EGGameData data;

	CharacterResourceInfo[] charInfo;


	public void Initialize(EGGameData _data)
	{
		data = _data;
		gamerunner = new GameRunner (data.network);
		local = new UnityLocalInputSource ();
		local.Initialize (0, UnityLocalInputSource.sDefaultSinglePlayerMapping);
		gamerunner.AddLocalPlayer (local.source);

		//TODO if character info file does not exist, try and generate frome folders
		//charInfo = CharacterResourceInfo.LoadInfo ("TESTCHARACTERS.json");

		InitializePlay ();
	}

	public void InitializeCharacterSelect()
	{
		
		List<AssetGroup> icons = new List<AssetGroup> ();
		foreach (var e in charInfo) {
			icons.Add (data.resourceManager.LoadAsset ("StreamingAssets/" + e.previewBundleName));
		}
		CharSelectState state = new CharSelectState ();
		state.Initialize (icons,charInfo);
		state.OnGameStart += OnGameStart;
		gamerunner.SetPlayState (state);
		playState = state;
	}


	//TODO pass in player 1, player 2, stage, and input sources (in order p1, p2)	
	public void InitializePlay()
	{
		//TODO load based on which characters we want
		List<AssetGroup> assets = new List<AssetGroup>();
		var robat = data.resourceManager.LoadAsset ("EditorOutput/Resources/ROBAT/robat.bundle");
		assets.Add(robat);
		assets.Add(data.resourceManager.LoadAsset("EditorOutput/Resources/PlaceholderProjectile/placeholderprojectile.bundle"));
		assets.Add(data.resourceManager.LoadAsset("EditorOutput/Resources/prefabprojectile/prefabprojectile.bundle"));

		StartCoroutine (LoadPlayCoroutine (assets));
	}

	System.Collections.IEnumerator LoadPlayCoroutine(List<AssetGroup> assets){
		if (assets.Where (e => !e.IsDone).Count() > 0)
			yield return null;

		Debug.Log ("asset bundles done loading");
		PlayState state = new PlayState ();

		state.Initialize (assets,"robat", "robat",0);
		gamerunner.SetPlayState (state);		
		playState = state;
	}

	public void Update(){
		if (gamerunner.PlayStateInitialialized) {
			gamerunner.Update (Time.deltaTime,FGEConstants.framesPerSecond,0);
			if(playState is PlayState)
				data.renderManager.Render (((PlayState)playState).AllEntities);
			if(playState is CharSelectState)
				data.renderManager.Render (((CharSelectState)playState).AllEntities);
		}
	}

	//-----------
	//listeners
	//-----------
	void OnGameStart(string p1, string p2){
		Debug.Log ("GAME START");
		//TODO destroy characterselectstate and go into playsate
		//reset localinput and network input source
		//alternatively, you could add a "start at frame" parameter to playstate which is better now that I think about it
	}

	public LocalInputSource CreateLocalInputSource()
	{
		LocalInputSource r = new LocalInputSource ();
		return r;
	}

	public NetworkInputSource CreateNetworkInputSource(int id)
	{
		//TODO
		return null;
	}


	public void OnGUI()
	{
		if (Event.current.type == EventType.KeyDown) {
			local.OnKeyChanged (Event.current.keyCode, true);
		} else if (Event.current.type == EventType.KeyUp) {
			local.OnKeyChanged (Event.current.keyCode, false);
		}
	}
}
