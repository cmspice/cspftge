﻿using UnityEngine;
using System.Collections;

using CSP;
using FGE;
public class EGGameData : MonoBehaviour {

	public Shader defaultSpriteShader;
	public Shader debugShader;

	public NetworkManager network;
	public ResourceManager resourceManager;
	public EntityRenderManager renderManager;

	void Awake () {
		Application.targetFrameRate = 60;
		resourceManager = new FGE.ResourceManager ();
		renderManager = new FGE.EntityRenderManager (defaultSpriteShader, resourceManager);
		renderManager.debugShader = debugShader;

		network = new NetworkManager ();
	}


	void Update () {
		network.UpdateNetwork ();
	}
}
