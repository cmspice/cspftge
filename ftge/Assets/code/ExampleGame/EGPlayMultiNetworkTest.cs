﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using CSP;
using FGE;

public class EGPlayMultiNetworkTest : MonoBehaviour {

	public GameRunner gamerunner;
	UnityLocalInputSource local;

	EGGameData data;

	bool connecting = false;
	bool waiting = false;
	bool connected = false;

	//TODO do timeouts!!


	public void Initialize(EGGameData _data)
	{
		data = _data;
		gamerunner = new GameRunner (data.network);
		local = new UnityLocalInputSource ();
		local.Initialize (0, UnityLocalInputSource.sDefaultSinglePlayerMapping);

		data.network.OnConnectionReceived += OnConnectionReceived;
		data.network.OnReceiveId += OnReceiveId;
		data.network.OnSynchronize += OnSynchronize;

		waiting = true;
		DebugConsole.Log ("initialized as host, accepting connections");
	}

	public void OnDestroy()
	{
		data.network.OnConnectionReceived -= OnConnectionReceived;
		data.network.OnReceiveId -= OnReceiveId;
		data.network.OnSynchronize -= OnSynchronize;
	}

	public void Connect(string _ip, int _port)
	{
		
		//1 - peer connects to host
		if (connecting) {
			throw new Exception ("already trying to connect, don't call twice :O");
		}

		DebugConsole.Log ("Attempting to connect to host " + _ip);
		waiting = false;
		connecting = true;
		data.network.Connect (_ip,_port);
	}

	public void OnConnectionReceived(int connId)
	{
		if (connecting) {
			throw new Exception ("Received connection when trying to connect");
		}

		//2 - host receives connection
		DebugConsole.Log ("HOST received connection from " + connId );
		data.network.SendIdentification (connId);
	}

	public void OnReceiveId(int connId, System.Guid guid)
	{
		if (connecting) {
			//3 - peer receives id from host
			DebugConsole.Log ("PEER received id from " + connId + " guid: " + guid.ToString ());
			data.network.SendIdentification (connId);
		}

		if (waiting) {
			//4 - host receives id from peer
			var wait = data.network.Ping(connId)*20;
			DebugConsole.Log ("HOST received id from " + connId + " guid: " + guid.ToString ());
			DebugConsole.Log ("synchronizing with wait " + data.network.Ping (connId) * 20);
			data.network.Synchronize(guid,wait);
		}

		waiting = false;
		connecting = false;
		connected = true;

		gamerunner.AddLocalPlayer (local.source);
		gamerunner.AddNetworkPlayer (guid, NetworkPlayerType.PLAYER);
	}

	public void OnSynchronize(System.Guid guid, bool timeout)
	{
		//timeout not implemented

		InitializePlay ();
	}

	//TODO pass in player 1, player 2, stage, and input sources (in order p1, p2)	
	public void InitializePlay()
	{
		//TODO load based on which characters we want
		List<AssetGroup> assets = new List<AssetGroup>();
		var robat = data.resourceManager.LoadAsset ("EditorOutput/Resources/ROBAT/robat.bundle");
		assets.Add(robat);
		assets.Add(data.resourceManager.LoadAsset("EditorOutput/Resources/PlaceholderProjectile/placeholderprojectile.bundle"));
		assets.Add(data.resourceManager.LoadAsset("EditorOutput/Resources/prefabprojectile/prefabprojectile.bundle"));

		StartCoroutine (LoadPlayCoroutine (assets));
	}


	System.Collections.IEnumerator LoadPlayCoroutine(List<AssetGroup> assets){
		if (assets.Where (e => !e.IsDone).Count() > 0)
			yield return null;

		Debug.Log ("asset bundles done loading");
		PlayState state = new PlayState ();

		state.Initialize (assets,"robat", "robat",0);
		gamerunner.SetPlayState (state);		

	}
	public void Update(){
		if (gamerunner.PlayStateInitialialized) {
			gamerunner.Update (Time.deltaTime,FGEConstants.framesPerSecond,data.network.Ping(gamerunner.npm.Opponent.Id)/1000f);
			//var playState = gamerunner.psm.LastConfirmedState;
			var playState = gamerunner.psm.CurrentState;
			if (playState is PlayState)
				data.renderManager.Render (((PlayState)playState).AllEntities);
			if (playState is CharSelectState)
				data.renderManager.Render (((CharSelectState)playState).AllEntities);
		}
	}

	public void OnGUI()
	{
		if (Event.current.type == EventType.KeyDown) {
			local.OnKeyChanged (Event.current.keyCode, true);
		} else if (Event.current.type == EventType.KeyUp) {
			local.OnKeyChanged (Event.current.keyCode, false);
		}

		if (true) {
			if (gamerunner.PlayStateInitialialized) {

				GUI.skin.label.fontSize = 20;

				int width = 200;
				int height = Screen.height;
				GUILayout.BeginArea (new Rect (Screen.width - width, 0, width, height));
				//GUILayout.Label (gamerunner.Frames);
				GUILayout.Label (gamerunner.lastDiff.ToString());
				GUILayout.Label (gamerunner.psm.LastConfirmedFrame + " " + gamerunner.ngc.AllInputs.LastFrame);
				GUILayout.Label (gamerunner.lastNumberUnconfirmedFrames.ToString());
				GUILayout.Label ("ping " +  data.network.Ping (gamerunner.npm.Opponent.Id));



				List<InputCode> inputs = new List<InputCode> ();
				int pLook = 20;
				var history = gamerunner.ngc.AllInputs;
				for (int i = Math.Max (0, history.LastFrame - pLook + 1); i <= history.LastFrame; i++) {
					if(history.inputs.ContainsKey(i))
						inputs.AddRange (history.inputs [i].inputs.Select (e => e.input.code));
				}
				foreach (var e in inputs) {
					GUILayout.Label (CSP.Input.GetHumanReadable (e));
				}
					


				GUILayout.EndArea ();
			}
		}
	}
}
