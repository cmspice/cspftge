﻿using UnityEngine;
using System.Collections;

public class PulsatingColorBehaviour : MonoBehaviour {
	public float FadeDuration = 0.1f;
	public Color Color1 = Color.black;
	public Color Color2 = Color.white;

	private Color startColor;
	private Color endColor;
	private float lastColorChangeTime;

	private Material material;

	public void Start()
	{
		Debug.Log ("start");
		material = GetComponentInChildren<Renderer>().material;
		startColor = Color1;
		endColor = Color2;
	}

	public void Update()
	{
		Debug.Log ("updating in fact");
		var ratio = (Time.time - lastColorChangeTime) / FadeDuration;
		ratio = Mathf.Clamp01(ratio);
		material.color = Color.Lerp(startColor, endColor, ratio);
		//material.color = Color.Lerp(startColor, endColor, Mathf.Sqrt(ratio)); // A cool effect
		//material.color = Color.Lerp(startColor, endColor, ratio * ratio); // Another cool effect

		if (ratio == 1f)
		{
			lastColorChangeTime = Time.time;

			// Switch colors
			var temp = startColor;
			startColor = endColor;
			endColor = temp;
		}
	}
}